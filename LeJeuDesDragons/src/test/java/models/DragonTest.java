package models;

import models.card.dragon.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DragonTest {
    public static Metallic gold;
    public static Colored red;
    public static None draco;

    @BeforeAll
    public static void createDragon(){
        gold = new Metallic(0, "Dragon d'or", 13, Metal.GOLD);
        red = new Colored(7, "Dragon rouge", 12, Color.RED);
        draco = new None(10, "Dracoliche", 10, "Dragon mort-vivant");
    }

    @Test
    public void DragonMetallicCreateTest(){
        assertEquals(0, gold.getIdCard());
        assertEquals("Dragon d'or", gold.getName());
        assertEquals(13, gold.getStrength());
        assertEquals(Metal.GOLD, gold.getMetal());
        assertEquals(true, gold.isGood());
    }

    @Test
    public void DragonColoredCreateTest(){
        assertEquals(7, red.getIdCard());
        assertEquals("Dragon rouge", red.getName());
        assertEquals(12, red.getStrength());
        assertEquals(Color.RED, red.getColor());
        assertEquals(false, red.isGood());
    }

    @Test
    public void DragonNoneCreateTest(){
        assertEquals(10, draco.getIdCard());
        assertEquals("Dracoliche", draco.getName());
        assertEquals(10, draco.getStrength());
        assertEquals("Dragon mort-vivant", draco.getType());
    }

}
