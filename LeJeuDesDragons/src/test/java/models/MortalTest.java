package models;

import models.card.mortal.Mortal;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MortalTest {
    public static Mortal mortal;

    @BeforeAll
    public static void CreateMortal(){
        mortal = new Mortal(2, "Druidesse", 6);
    }

    @Test
    public void MortalCreateTest(){
        assertEquals(2, mortal.getIdCard());
        assertEquals("Druidesse", mortal.getName());
        assertEquals(6, mortal.getStrength());
    }
}
