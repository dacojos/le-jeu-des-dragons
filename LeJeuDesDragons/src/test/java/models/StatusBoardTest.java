package models;

import models.board.Board;
import models.statusBoard.*;
import org.junit.jupiter.api.BeforeEach;

public class StatusBoardTest {
    static Ante ante;
    static Choice choice;
    static EndGame endGame;
    static WaitingPlay waitingPlay;
    static WaitingPlayer waitingPlayer;
    static Board board;

    @BeforeEach
    public void createBoardStatus(){
        board = new Board();
        ante = new Ante(board);
        choice = new Choice(board);
        endGame = new EndGame(board);
        waitingPlay = new WaitingPlay(board);
        waitingPlayer = new WaitingPlayer(board);
    }

    //todo utilité ? les fonctionnalités sont testés avec les processEvent de Board
}
