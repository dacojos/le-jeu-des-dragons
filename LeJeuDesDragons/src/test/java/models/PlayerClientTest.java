package models;

import game.Game;
import models.player.PlayerClient;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PlayerClientTest {
    public static PlayerClient player1;
    public static PlayerClient player2;

    @BeforeAll
    public static void CreatePlayer(){
        player1 = new PlayerClient(50, "nameHere", 0);
        player2 = new PlayerClient(43, "frank", 1, 50);
    }

    @Test
    public void PlayerClientCreateTest(){
        assertEquals(50, player1.getCoin());
        assertEquals("nameHere", player1.getName());
        assertEquals(0, player1.getId());
        assertEquals(0, player1.getHandSize());

        assertEquals(43, player2.getCoin());
        assertEquals("frank", player2.getName());
        assertEquals(1, player2.getId());
        assertEquals(50, player2.getHandSize());
    }

    @Test
    public void flightStrengthShouldBeRight(){
        player1.getFlight().add(Game.cardGameList.get(10));
        player1.getFlight().add(Game.cardGameList.get(12));
        assertEquals(11, player1.getFlightStrength());
    }

}
