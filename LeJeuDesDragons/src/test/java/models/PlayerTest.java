package models;

import game.Game;
import models.player.Player;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PlayerTest {
    public static Player player1;

    @BeforeAll
    public static void CreatePlayer(){
        player1 = new Player(50, "Bob", 0);
    }

    @Test
    public void PlayerCreateTest(){
        assertEquals(50, player1.getCoin());
        assertEquals("Bob", player1.getName());
        assertEquals(0, player1.getId());
    }

    @Test
    public void flightStrengthShouldBeRight(){
        player1.addCardFlight(Game.cardGameList.get(10));
        player1.addCardFlight(Game.cardGameList.get(12));
        assertEquals(11, player1.getFlightStrength());
    }
}
