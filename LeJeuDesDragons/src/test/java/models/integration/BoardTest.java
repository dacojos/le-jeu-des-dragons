package models.integration;

import communication.server.DDServer;
import models.event.*;
import models.board.Board;
import models.board.BoardClient;
import models.card.Card;
import models.card.dragon.*;
import models.card.mortal.Mortal;
import game.Game;
import models.event.choice.ChoiceMade;
import models.player.Player;
import models.power.dragon.color.White;
import models.power.mortal.Archmage;
import models.statusBoard.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

public class BoardTest {

    public models.board.Board board;
    public  BoardClient boardClient;
    public  ArrayList<Event> historique = new ArrayList<>();
    public  ArrayList<Card> deckTest = new ArrayList<Card>(70);

    @BeforeEach
    public void createBoard(){
        board = new models.board.Board();
        board.setServer(new DDServer(board));

        boardClient = new BoardClient();
    }

    public void initBoard() {
        createDeck();
        board.setDeck(deckTest);
        board.addPlayerBoard("Alice");
        board.addPlayerBoard("Bob");
        board.addPlayerBoard("Camille");
        board.addPlayerBoard("Daniel");
        resetBoard();
    }

    public void resetBoard(){
        board.setCardPlayed( new ArrayList<>(Collections.nCopies(board.getPlayers().size(), null)));
        board.setAnte( new ArrayList<>(Collections.nCopies(board.getPlayers().size(), null)));
        board.setHasBrelan( new ArrayList<>(Collections.nCopies(board.getPlayers().size(), false)));
        board.setHasColor( new ArrayList<>(Collections.nCopies(board.getPlayers().size(), false)));
        board.setDiscard(new ArrayList<>());

        for(Player p : board.getPlayers()){
            p.getHand().clear();
            p.getFlight().clear();
            p.setCoin(50);
            p.addCardHand(new Card(100,"non vide",1));
            p.addCardHand(new Card(100,"non vide",1));
        }

        board.setTotalBet(20);
    }

    public void createDeck() {
        deckTest.add(new Colored(40, "Dragon Noir", 2, Color.BLACK));
        deckTest.add(new Metallic(29, "Dragon De Bronze", 6, Metal.BRONZE));
        deckTest.add(new Metallic(49, "Dragon D'Or", 11, Metal.GOLD));
        deckTest.add(new Metallic(8, "Dragon d'Arain", 9, Metal.BRASS));
        deckTest.add(new Colored(51, "Dragon Rouge", 2, Color.RED));
        deckTest.add(new Colored(52, "Dragon Rouge", 3, Color.RED));
        deckTest.add(new Metallic(35, "Dragon De Cuivre", 5, Metal.COPPER));
        deckTest.add(new Metallic(33, "Dragon De Cuivre", 1, Metal.COPPER));
        deckTest.add(new Colored(15, "Dragon Blanc", 1, Color.WHITE));
        deckTest.add(new Colored(26, "Dragon Bleu", 11, Color.BLUE));
        deckTest.add(new Colored(44, "Dragon Noir", 9, Color.BLACK));
        deckTest.add(new Metallic(36, "Dragon De Cuivre", 7, Metal.COPPER));
        deckTest.add(new Colored(39, "Dragon Noir", 1, Color.BLACK));
        deckTest.add(new Metallic(11, "Dragon d'Argent", 6, Metal.SILVER));
        deckTest.add(new Colored(25, "Dragon Bleu", 9, Color.BLUE));
        deckTest.add(new Metallic(30, "Dragon De Bronze", 7, Metal.BRONZE));
        deckTest.add(new Colored(56, "Dragon Rouge", 12, Color.RED));
        deckTest.add(new Mortal(69, "La Voleuse", 7));
        deckTest.add(new Colored(18, "Dragon Blanc", 4, Color.WHITE));
        deckTest.add(new Mortal(65, "Le Prêtre", 5));
        deckTest.add(new Mortal(63, "La Druidesse", 6));
        deckTest.add(new None(2, "Dracolich", 10, "Dragon Mort-Vivant"));
        deckTest.add(new Colored(43, "Dragon Noir", 7, Color.BLACK));
        deckTest.add(new Metallic(12, "Dragon d'Argent", 8, Metal.SILVER));
        deckTest.add(new Colored(53, "Dragon Rouge", 5, Color.RED));
        deckTest.add(new Colored(54, "Dragon Rouge", 8, Color.RED));
        deckTest.add(new Mortal(64, "L'Idiot", 3));
        deckTest.add(new Metallic(32, "Dragon De Bronze", 11, Metal.BRONZE));
        deckTest.add(new Colored(23, "Dragon Bleu", 4, Color.BLUE));
        deckTest.add(new Colored(16, "Dragon Blanc", 2, Color.WHITE));
        deckTest.add(new Colored(22, "Dragon Bleu", 2, Color.BLUE));
        deckTest.add(new Colored(67, "Tiamat", 13, Color.DIVIN));
        deckTest.add(new Metallic(28, "Dragon De Bronze", 3, Metal.BRONZE));
        deckTest.add(new Mortal(68, "Le Tueur De Dragon", 8));
        deckTest.add(new Metallic(10, "Dragon d'Argent", 3, Metal.SILVER));
        deckTest.add(new Metallic(47, "Dragon D'Or", 6, Metal.GOLD));
        deckTest.add(new Metallic(38, "Dragon De Cuivre", 10, Metal.COPPER));
        deckTest.add(new Colored(42, "Dragon Noir", 5, Color.BLACK));
        deckTest.add(new Metallic(7, "Dragon d'Arain", 7, Metal.BRASS));
        deckTest.add(new Colored(21, "Dragon Bleu", 1, Color.BLUE));
        deckTest.add(new Colored(59, "Dragon Vert", 4, Color.GREEN));
        deckTest.add(new Colored(57, "Dragon Vert", 1, Color.GREEN));
        deckTest.add(new Colored(58, "Dragon Vert", 2, Color.GREEN));
        deckTest.add(new Colored(24, "Dragon Bleu", 7, Color.BLUE));
        deckTest.add(new Metallic(14, "Dragon d'Argent", 12, Metal.SILVER));
        deckTest.add(new Metallic(13, "Dragon d'Argent", 10, Metal.SILVER));
        deckTest.add(new Metallic(37, "Dragon De Cuivre", 8, Metal.COPPER));
        deckTest.add(new Metallic(48, "Dragon D'Or", 9, Metal.GOLD));
        deckTest.add(new Metallic(1, "Bahamut", 13, Metal.DIVIN));
        deckTest.add(new Colored(55, "Dragon Rouge", 10, Color.RED));
        deckTest.add(new Metallic(3, "Dragon d'Arain", 1, Metal.BRASS));
        deckTest.add(new Mortal(66, "La Princesse", 4));
        deckTest.add(new Colored(19, "Dragon Blanc", 6, Color.WHITE));
        deckTest.add(new Colored(61, "Dragon Vert", 8, Color.GREEN));
        deckTest.add(new Metallic(6, "Dragon d'Arain", 5, Metal.BRASS));
        deckTest.add(new Colored(20, "Dragon Blanc", 8, Color.WHITE));
        deckTest.add(new Colored(17, "Dragon Blanc", 3, Color.WHITE));
        deckTest.add(new Metallic(46, "Dragon D'Or", 4, Metal.GOLD));
        deckTest.add(new Colored(62, "Dragon Vert", 10, Color.GREEN));
        deckTest.add(new Metallic(5, "Dragon d'Arain", 4, Metal.BRASS));
        deckTest.add(new Mortal(0, "L'Archimage", 9));
        deckTest.add(new Colored(60, "Dragon Vert", 6, Color.GREEN));
        deckTest.add(new Metallic(45, "Dragon D'Or", 2, Metal.GOLD));
        deckTest.add(new Metallic(4, "Dragon d'Arain", 2, Metal.BRASS));
        deckTest.add(new Metallic(31, "Dragon De Bronze", 9, Metal.BRONZE));
        deckTest.add(new Metallic(9, "Dragon d'Argent", 2, Metal.SILVER));
        deckTest.add(new Metallic(50, "Dragon D'Or", 13, Metal.GOLD));
        deckTest.add(new Metallic(27, "Dragon De Bronze", 1, Metal.BRONZE));
        deckTest.add(new Colored(41, "Dragon Noir", 3, Color.BLACK));
        deckTest.add(new Metallic(34, "Dragon De Cuivre", 3, Metal.COPPER));
    }

    public void createHistorique(){
        historique.add(new JoinGame("Bob"));
        historique.add(new JoinGame("Marcel"));
        historique.add(new JoinGame("Henris"));
        historique.add(new JoinGame("Jaque"));
        historique.add(new StartGame(0));

        //Ante
        historique.add(new PlayerPlayCard(0, 0));
        historique.add(new PlayerPlayCard(1, 0));
        historique.add(new PlayerPlayCard(2, 0));
        historique.add(new PlayerPlayCard(3, 0));

        //T1
        historique.add(new PlayerPlayCard(1, 0));
        historique.add(new PlayerPlayCard(2, 0));
        historique.add(new PlayerPlayCard(3, 0));
        historique.add(new PlayerPlayCard(0, 0));

        //T2
        historique.add(new PlayerPlayCard(3, 0));
        historique.add(new PlayerPlayCard(0, 0));
        historique.add(new PlayerPlayCard(1, 0));
        historique.add(new PlayerPlayCard(2, 0));

        //T3
        historique.add(new PlayerPlayCard(0, 0));
        historique.add(new PlayerPlayCard(1, 0));
        historique.add(new PlayerPlayCard(2, 0));
        historique.add(new PlayerPlayCard(3, 0));
    }

    /*
    @Test
    public void TestHistoricProcessEvent(){
        createHistorique();
        createDeck();

        assertEquals(70, board.getDeck().size());

        assertEquals(true, board.getStatusBoard().canJoin());
        board.processEvent((JoinGame) historique.remove(0));
        board.processEvent((JoinGame) historique.remove(0));
        assertEquals(2, board.getNumberPlayer());
        board.processEvent((JoinGame) historique.remove(0));
        board.processEvent((JoinGame) historique.remove(0));
        assertEquals(4, board.getNumberPlayer());
        assertEquals(false, board.getStatusBoard().canPlay());

        board.processEvent((StartGame) historique.remove(0));

        board.setDeck(deckTest);
        for(Player p : board.getPlayers()){
            p.getHand().clear();
            board.draw(p, 6);
        }
        board.setCardPlayed( new ArrayList<>(Collections.nCopies(board.getPlayers().size(), null)));
        board.setAnte( new ArrayList<>(Collections.nCopies(board.getPlayers().size(), null)));

        assertEquals(false, board.getStatusBoard().canJoin());
        assertEquals(true, board.getStatusBoard().antePhase());
        assertEquals(46, board.getDeck().size());

        board.processEvent((PlayerPlayCard) historique.remove(0));
        board.processEvent((PlayerPlayCard) historique.remove(0));
        board.processEvent((PlayerPlayCard) historique.remove(0));
        board.processEvent((PlayerPlayCard) historique.remove(0));

        assertEquals(true, board.getStatusBoard().canPlay());
        assertEquals(false, board.getStatusBoard().antePhase());

        board.processEvent((PlayerPlayCard) historique.remove(0));
        board.processEvent((PlayerPlayCard) historique.remove(0));
        board.processEvent((PlayerPlayCard) historique.remove(0));
        board.processEvent((PlayerPlayCard) historique.remove(0));

        board.processEvent((PlayerPlayCard) historique.remove(0));
        board.processEvent((PlayerPlayCard) historique.remove(0));
        board.processEvent((PlayerPlayCard) historique.remove(0));
        board.processEvent((PlayerPlayCard) historique.remove(0));

        board.processEvent((PlayerPlayCard) historique.remove(0));
        board.processEvent((PlayerPlayCard) historique.remove(0));
        board.processEvent((PlayerPlayCard) historique.remove(0));
        board.processEvent((PlayerPlayCard) historique.remove(0));

        assertEquals(true, board.getStatusBoard().antePhase());

    }

    */

    /**
     *
     */
    @Test
    public void roundEndWhenEachPlayerHadPlay(){
        //todo utilité? si les processEvent fonctionne
    }

    @Test
    public void turnEndWhenPlayerHadPlay(){
        //todo utilité? si les processEvent fonctionne
    }

    private void initBrelanAndColor(){
        initBoard();
        board.setStatusBoard(new WaitingPlay(board));
        board.setAnte(new ArrayList<>());

        board.getPlayers().get(0).addCardFlight(new Colored(0, "Dragon Rouge", 4, null, Color.RED));
        board.getPlayers().get(0).addCardFlight(new Colored(0, "Dragon Rouge", 10, null, Color.RED));
        board.getPlayers().get(0).addCardFlight(new Colored(0, "Dragon Noir", 4, null, Color.BLACK));
        board.getPlayers().get(0).addCardFlight(new Colored(0, "Dragon Noir", 8, null, Color.BLACK));
        board.getPlayers().get(0).addCardFlight(new Metallic(0, "Dragon D'or", 3, null, Metal.GOLD));
        board.getPlayers().get(0).addCardFlight(new Metallic(0, "Dragon D'or", 13, null, Metal.GOLD));

        board.getPlayers().get(0).addCardHand(new Colored(0, "Dragon Noir", 9, null, Color.BLACK));
        board.getPlayers().get(0).addCardHand(new Colored(0, "Tiamat", 12, null, Color.DIVIN));
        board.getPlayers().get(0).addCardHand(new Metallic(0, "Dragon D'or", 12, null, Metal.GOLD));
        board.getPlayers().get(0).addCardHand(new Metallic(0, "Dragon D'or", 4, null, Metal.GOLD));
        board.getPlayers().get(0).addCardHand(new Colored(0, "Dragon Noir", 8, null, Color.BLACK));
        board.getPlayers().get(0).addCardHand(new Colored(0, "Dragon Vert", 4, null, Color.GREEN));
    }

    @Test
    public void familyOf3OrMoreSameColorStealCoinEqualToSecondStrongest(){
        initBrelanAndColor();
        board.processEvent(new PlayerPlayCard(0,2));

        assertEquals(true, board.getHasColor().get(0));
        assertEquals(50+8*(board.getNumberPlayer()-1),board.getPlayers().get(0).getCoin());
    }

    @Test
    public void familyOf3OrMoreSameMetalStealCoinEqualToSecondStrongest(){
        initBrelanAndColor();
        board.processEvent(new PlayerPlayCard(0,4));

        assertEquals(true, board.getHasColor().get(0));
        assertEquals(50+12*(board.getNumberPlayer()-1), board.getPlayers().get(0).getCoin());
    }

    @Test
    public void ifTiamatMake2FamilyOnlyTheStrongestIsApply(){
        initBrelanAndColor();
        board.processEvent(new PlayerPlayCard(0,3));

        assertEquals(true, board.getHasColor().get(0));
        assertEquals(50+10*(board.getNumberPlayer()-1), board.getPlayers().get(0).getCoin());
    }

    @Test
    public void ifSecondStrongestEqualStrongestNotUseThirdStrongest(){
        //TODO impossible
        initBrelanAndColor();
        board.processEvent(new PlayerPlayCard(0,6));

        assertEquals(true, board.getHasColor().get(0));
        assertEquals(50+8*(board.getNumberPlayer()-1), board.getPlayers().get(0).getCoin());
    }

    @Test
    public void tiamatCountAsAnyColorButNotAMetal(){
        for( int i = 0; i< Color.values().length - 2; i++){
            initBoard();
            board.setStatusBoard(new WaitingPlay(board));

            board.getPlayers().get(0).addCardFlight(new Colored(0, "Dragon Coloré", 4, null, Color.values()[i]));
            board.getPlayers().get(0).addCardFlight(new Colored(0, "Dragon Coloré", 10, null, Color.values()[i]));

            board.getPlayers().get(0).addCardHand(new Colored(0, "Tiamat", 12, null, Color.DIVIN));
            board.getPlayers().get(0).addCardHand(new Colored(0, "Tiamat", 12, null, Color.DIVIN));

            board.processEvent(new PlayerPlayCard(0,2));

            assertEquals(true, board.getHasColor().get(0));
            assertEquals(50+10*(board.getNumberPlayer()-1), board.getPlayers().get(0).getCoin());
        }

    }

    @Test
    public void playerCanNotApply2FamilyIn1Bet(){
        initBrelanAndColor();
        board.getStatusBoard().setNbrRound(3);

        board.processEvent(new PlayerPlayCard(0,4));
        board.processEvent(new PlayerPlayCard(0,2));

        assertEquals(true, board.getHasColor().get(0));
        assertEquals(50+12*(board.getNumberPlayer()-1), board.getPlayers().get(0).getCoin());

    }

    @Test
    public void familyAndBrelanCanBeApplyInSameRound(){
        initBrelanAndColor();
        board.getAnte().add(new Colored(0, "Dragon Rouge", 4, null, Color.RED));
        board.getAnte().add(new Colored(0, "Dragon Rouge", 4, null, Color.RED));
        int nbCard = board.getSizeAnte() - 1 + board.getPlayers().get(0).getSizeHand();

        board.processEvent(new PlayerPlayCard(0,5));

        assertEquals(true, board.getHasColor().get(0));
        assertEquals(true, board.getHasBrelan().get(0));
        assertEquals(54+4*(board.getNumberPlayer()-1), board.getPlayers().get(0).getCoin());
        assertEquals(16, board.getTotalBet());
        assertEquals(nbCard, board.getPlayers().get(0).getSizeHand());
        assertEquals(0, board.getSizeAnte());
    }

    @Test
    public void mortalNotCountInfamilyAndBrelan(){
        initBoard();
        board.setStatusBoard(new WaitingPlay(board));
        board.setAnte(new ArrayList<>());

        board.getPlayers().get(0).addCardFlight(new Mortal(0,"mortel 1",4,null));
        board.getPlayers().get(0).addCardFlight(new Mortal(0,"mortel 2",4,null));

        board.getPlayers().get(0).addCardHand(new Mortal(0,"mortel 3",4,null));
        board.getPlayers().get(0).addCardHand(new Mortal(0,"mortel 3",4,null));

        board.processEvent(new PlayerPlayCard(0,0));

        assertEquals(false, board.getHasColor().get(0));
        assertEquals(false, board.getHasBrelan().get(0));
    }

    @Test
    public void ifCardMake2BrelanOnlyTheStrongestIsApply(){
        //TODO impossible une carte n'a qu'une force
    }

    @Test
    public void brelanOf3OrMoreCardStealCoinEqualToBrelanStrength(){
        initBrelanAndColor();
        board.getAnte().add(new Colored(0, "Dragon Rouge", 4, null, Color.RED));
        board.getAnte().add(new Colored(0, "Dragon Rouge", 4, null, Color.RED));
        int nbCard = board.getSizeAnte() - 1 + board.getPlayers().get(0).getSizeHand();

        board.processEvent(new PlayerPlayCard(0,7));

        assertEquals(true, board.getHasBrelan().get(0));
        assertEquals(54, board.getPlayers().get(0).getCoin());
        assertEquals(16, board.getTotalBet());
        assertEquals(nbCard, board.getPlayers().get(0).getSizeHand());
        assertEquals(0, board.getSizeAnte());
    }

    @Test
    public void brelanOf3OrMoreCardProposeChoosee1AnteUntilHandFull(){
        initBrelanAndColor();
        for(int j = 0; j < board.NB_MAX_CARD_HAND; j++) {
            board.getAnte().add(new Colored(0, "Dragon Rouge", 4, null, Color.RED));
        }

        board.processEvent(new PlayerPlayCard(0,7));

        assertEquals(true, board.getHasBrelan().get(0));
        assertEquals(54, board.getPlayers().get(0).getCoin());
        assertEquals(16, board.getTotalBet());
        assertEquals( StatusId.CHOICE, board.getStatusBoard().getStateId());

        for(int i = 1; i <= 3 ; i++){
            board.processEvent(new ChoiceMade(0,0));
            assertEquals(board.NB_MAX_CARD_HAND - i, board.getSizeAnte());
        }
        assertEquals( StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
    }

    @Test
    public void brelanOf3OrMoreCardTakeAllAnteIfNotOverFullHand(){
        int nbCard;
        initBrelanAndColor();
        for(int i = 0; i < board.getNumberPlayer(); i++){
            board = new Board();
            board.setServer(new DDServer(board));
            initBrelanAndColor();
            for(int j = 0; j < i; j++) {
                board.getAnte().add(new Colored(0, "Dragon Rouge", 4, null, Color.RED));
            }

            nbCard = board.getSizeAnte() - 1 + board.getPlayers().get(0).getSizeHand();

            board.processEvent(new PlayerPlayCard(0,7));
            assertEquals(true, board.getHasBrelan().get(0));
            assertEquals(54, board.getPlayers().get(0).getCoin());
            assertEquals(16, board.getTotalBet());
            assertEquals( nbCard, board.getPlayers().get(0).getSizeHand());
            assertEquals(0, board.getSizeAnte());
        }
    }

    @Test
    public void playerCanNotApply2BrelanIn1Bet(){
        initBrelanAndColor();
        board.getAnte().add(new Colored(0, "Dragon Rouge", 4, null, Color.RED));
        board.getAnte().add(new Colored(0, "Dragon Rouge", 4, null, Color.RED));
        int nbCard = board.getSizeAnte() - 1 + board.getPlayers().get(0).getSizeHand();

        board.processEvent(new PlayerPlayCard(0,7));
        board.processEvent(new PlayerPlayCard(0,5));

        assertEquals(true, board.getHasBrelan().get(0));
        assertEquals(54, board.getPlayers().get(0).getCoin());
        assertEquals(16, board.getTotalBet());
        assertEquals(nbCard, board.getPlayers().get(0).getSizeHand());
        assertEquals(0, board.getSizeAnte());
    }

    @Test
    public void inWaitingPlayLeaderIsStrongestWithoutEquality(){
        initBoard();
        int[] idLeader = new int[]{3,-1,0,1,-1};
        ArrayList<int[]> listStrength = new ArrayList<>();
        listStrength.add(new int[]{4,4,1,3});
        listStrength.add(new int[]{4,4,1,1});
        listStrength.add(new int[]{7,4,4,1});
        listStrength.add(new int[]{3,7,4,1});
        listStrength.add(new int[]{1,1,1,1});

        for(int i = 0; i< listStrength.size(); i++) {
            resetBoard();
            board.setStatusBoard(new WaitingPlay(board));

            assertEquals(0,board.getStatusBoard().getCurrentPlayerId());

            for(int j = 0; j < board.getNumberPlayer(); j++) {
                board.getCardPlayed().set(j, new Card("carte " + j, listStrength.get(i)[j]));
            }

            assertEquals(idLeader[i], board.getStatusBoard().searchLeader(board.getCardPlayed()));
        }
    }

    @Test
    public void inAnteLeaderIsStrongestWithoutEquality(){
        initBoard();
        int[] idLeader = new int[]{3,-1,0,1,-1};
        ArrayList<int[]> listStrength = new ArrayList<>();
        listStrength.add(new int[]{4,4,1,3});
        listStrength.add(new int[]{4,4,1,1});
        listStrength.add(new int[]{7,4,4,1});
        listStrength.add(new int[]{3,7,4,1});
        listStrength.add(new int[]{1,1,1,1});

        for(int i = 0; i< listStrength.size(); i++) {
            resetBoard();
            board.setStatusBoard(new Ante(board));

            assertEquals(-1,board.getStatusBoard().getCurrentPlayerId());

            for(int j = 0; j < board.getNumberPlayer(); j++) {
                board.getAnte().set(j, new Card("carte " + j, listStrength.get(i)[j]));
            }

            board.getStatusBoard().tryStartPlay();

            assertEquals((idLeader[i]  == -1 ? StatusId.ANTE : StatusId.WAITING_PLAY), board.getStatusBoard().getStateId());
        }
    }

    @Test
    public void playerCanNotHaveMoreThan10CardAfterDraw(){
        JoinGame event = new JoinGame("Bob");
        board.processEvent(event);
        board.draw(board.getPlayers().get(0),12);
        assertNotEquals(board.getPlayers().get(0),10);

        board.processEvent(event);
        board.draw(board.getPlayers().get(0),3);
        assertNotEquals(board.getPlayers().get(1),3);
        board.draw(board.getPlayers().get(0),8);
        assertNotEquals(board.getPlayers().get(1),10);
    }

    /**
     *
     */
    @Test
    public void inWaitingPlayerCanReturnStartPage(){
        //todo
    }

    /**
     * processEvent doit retourner false si le nombre de joueur max est déjà atteint
     * et vrai s'il a pu ajouter le joueur, le nombre de joueurs doit être cohérent
     */
    @Test
    public void inWaitingPlayerMax6PlayerCanJoin(){
        assertEquals(board.getStatusBoard().getClass().getSimpleName(),"WaitingPlayer");
        for (int i = 0; i < board.NBR_PLAYER_MAX; i++) {
            assertTrue(board.processEvent(new JoinGame("player" + i)));
            assertEquals(board.getPlayers().size(),i+1);
        }
        assertFalse(board.processEvent(new JoinGame("player6")));
        assertEquals(board.getPlayers().size(),board.NBR_PLAYER_MAX);
        assertEquals(board.getStatusBoard().getClass().getSimpleName(),"WaitingPlayer");
    }

    /**
     * un joueur seul ne peut pas lancer la partie, l'état reste à WaitingPlayer
     */
    @Test
    public void inWaitingPlayerRightPlayerCanNotStartAlone(){
        assertEquals(board.getStatusBoard().getClass().getSimpleName(),"WaitingPlayer");
        board.processEvent(new JoinGame("player"));
        assertFalse(board.processEvent(new StartGame(0)));
        assertEquals(board.getStatusBoard().getClass().getSimpleName(),"WaitingPlayer");
    }

    /**
     * le mauvais joueur ne peut pas lancer la partie, l'état reste à WaitingPlayer
     */
    @Test
    public void inWaitingPlayerWrongPlayerCanNotStart(){
        assertEquals(board.getStatusBoard().getClass().getSimpleName(),"WaitingPlayer");
        board.processEvent(new JoinGame("player0"));
        board.processEvent(new JoinGame("player1"));
        assertFalse(board.processEvent(new StartGame(1)));
        assertEquals(board.getStatusBoard().getClass().getSimpleName(),"WaitingPlayer");
    }

    /**
     * le bon joueur peut lancer la partie, l'état passe à WaitingPlay
     */
    @Test
    public void inWaitingPlayerRightPlayerCanStartNotAlone(){
        assertEquals(board.getStatusBoard().getClass().getSimpleName(),"WaitingPlayer");
        board.processEvent(new JoinGame("player0"));
        board.processEvent(new JoinGame("player1"));
        assertTrue(board.processEvent(new StartGame(0)));
        assertEquals(board.getStatusBoard().getClass().getSimpleName(),"Ante");
    }

    /**
     *
     */
    @Test
    public void inWaitingPlayManageTimeOut(){
        //todo
    }

    /**
     * le bon joueur ne peut pas jouer 2 fois de suite
     */
    @Test
    public void inWaitingPlayTheRightPlayerCanNotPlayTwiceInRow(){
        initBoard();
        board.setStatusBoard(new WaitingPlay(board));
        board.getPlayers().get(0).getHand().add(Game.cardGameList.get(10));
        board.getPlayers().get(0).getHand().add(Game.cardGameList.get(11));

        assertTrue(board.processEvent(new PlayerPlayCard(0,0)));
        assertFalse(board.processEvent(new PlayerPlayCard(0,0)));
    }

    /**
     * le bon joueur peut jouer
     */
    @Test
    public void inWaitingPlayTheRightPlayerCanPlay(){
        initBoard();
        board.setStatusBoard(new WaitingPlay(board));
        board.getPlayers().get(0).getHand().add(Game.cardGameList.get(10));
        assertTrue(board.processEvent(new PlayerPlayCard(0,0)));
    }

    /**
     * le mauvais joueur ne peut pas jouer
     */
    @Test
    public void inWaitingPlayTheWrongPlayerCanNotPlay(){
        board.setStatusBoard(new WaitingPlay(board));
        assertFalse(board.processEvent(new PlayerPlayCard(1,0)));
    }

    @Test
    public void processEventChooseEndGameReturnFalseIfNotInEndGame(){
        initBoard();
        assertFalse(board.processEvent(new ChooseEndGame(0,false)));
        assertFalse(board.processEvent(new ChooseEndGame(0,true)));
    }

    @Test
    public void processEventChooseEndGameReturnTrueInEndGame(){
        initBoard();
        board.setStatusBoard(new EndGame(board));
        assertTrue(board.processEvent(new ChooseEndGame(0,false)));
        assertTrue(board.processEvent(new ChooseEndGame(1,true)));

    }

    @Test
    public void processEventChooseEndGameRestartBoardIfAllVoteAreTrue(){
        initBoard();
        board.setStatusBoard(new EndGame(board));
        for(int i = 0; i < board.getNumberPlayer(); i++){
            board.processEvent(new ChooseEndGame(i,true));
        }
        assertEquals( StatusId.ANTE,board.getStatusBoard().getStateId());
        assertEquals(70-6*board.getNumberPlayer(), board.getSizeDeck());
    }

    @Test
    public void processEventChooseEndGameQuitIfAtLeast1VoteAreFalse(){
        initBoard();
        board.setStatusBoard(new EndGame(board));
        for(int i = 0; i < board.getNumberPlayer(); i++){
            for(int j = 0; j < board.getNumberPlayer(); j++){
                board.processEvent(new ChooseEndGame(i,i!=j));
            }
            assertNotEquals( StatusId.ANTE,board.getStatusBoard().getStateId());
            assertNotEquals(70-6*board.getNumberPlayer(), board.getSizeDeck());
        }

    }

    @Test
    public void processEventChoiceMadeApplyChoice(){
        //TODO
        initBoard();
        board.setStatusBoard(new WaitingPlay(board));
        board.setAnte(new ArrayList<>());
        board.getAnte().add(new Colored(1, "Dragon Blanc", 7, new White(), Color.WHITE));

        board.getPlayers().get(0).getHand().add(new Mortal(0, "L'Archimage", 9, new Archmage()));
        board.getPlayers().get(0).getHand().add(new Mortal(0, "L'Archimage", 9, new Archmage()));

        board.processEvent(new PlayerPlayCard(0,2));
        assertEquals( StatusId.CHOICE, board.getStatusBoard().getStateId());

        board.processEvent(new ChoiceMade(0,0));
        assertEquals( StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
        assertEquals( 18, board.getTotalBet());
    }

    @Test void if1PlayerHandIsEmptyRebuy(){
        initBoard();

        board.setStatusBoard( new WaitingPlay(board.getStatusBoard()));
        board.getPlayers().get(0).getHand().add(new Card(0,"truc",1));
        board.getPlayers().get(3).getHand().clear();

        assertEquals(0, board.getPlayers().get(3).getSizeHand());
        board.processEvent( new PlayerPlayCard(0,2));

        assertEquals(StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
        assertEquals(4, board.getPlayers().get(3).getSizeHand());
    }

    @Test void if1PlayerHas1CardAndNotHisTurnRebuy(){
        initBoard();

        board.setStatusBoard( new WaitingPlay(board.getStatusBoard()));
        board.getPlayers().get(0).getHand().add(new Card(0,"truc",1));
        board.getPlayers().get(3).getHand().remove(0);

        assertEquals(1, board.getPlayers().get(3).getSizeHand());
        board.processEvent( new PlayerPlayCard(0,2));

        assertEquals(StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
        assertEquals(1, board.getPlayers().get(3).getSizeHand());
    }

    @Test void if1PlayerHas1CardAndHisTurnRebuy(){
        initBoard();

        board.setStatusBoard( new WaitingPlay(board.getStatusBoard()));
        board.getPlayers().get(0).getHand().add(new Card(0,"truc",1));
        board.getPlayers().get(1).getHand().remove(0);

        assertEquals(1, board.getPlayers().get(1).getSizeHand());
        board.processEvent( new PlayerPlayCard(0,2));

        assertEquals(StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
        assertEquals(4, board.getPlayers().get(1).getSizeHand());
    }



}
