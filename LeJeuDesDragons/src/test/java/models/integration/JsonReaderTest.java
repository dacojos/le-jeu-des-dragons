package models.integration;

import models.event.ChooseEndGame;
import models.event.PlayerPlayCard;
import models.event.QuitGame;
import models.event.UpdateBoardClient;

import models.event.choice.ChoiceMade;
import models.event.choice.ChoiceOption;
import models.jsonHandler.JsonReader;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JsonReaderTest {
    static JsonReader jsonReader;

    @BeforeEach
    public void prepareJsonHandler(){
        jsonReader = new JsonReader();
    }


    /**
     * @brief:  on vérifie que l'Event parsé contiennent les bonnes valeurs
     */
    @Test
    public void parseJoinGameReturnWellFormedEvent(){
        assertEquals("Bob", jsonReader.parseJoinGame("{\"name\":\"Bob\"}").getName());
    }

    /**
     * @brief:  on vérifie que l'Event parsé contiennent les bonnes valeurs
     */
    @Test
    public void parseStartGameReturnWellFormedEvent(){
        assertEquals(0, jsonReader.parseStartGame("{\"id\":0}").getIdPlayer());
    }

    /**
     * @brief:  on vérifie que l'Event parsé contiennent les bonnes valeurs
     */
    @Test
    public void parseAssignIdReturnWellFormedEvent(){
        assertEquals(0, jsonReader.parseAssignId("{\"personalId\":0}").getId());
    }

    /**
     * @brief:  on vérifie que l'Event parsé contiennent les bonnes valeurs
     */
    @Test
    public void parsePlayerPlayCardReturnWellFormedEvent(){
        PlayerPlayCard jsonParsed = jsonReader.parsePlayerPlayCard("{\"idPlayer\":0,\"posCard\":0}");
        assertEquals(0, jsonParsed.getIdPlayer());
        assertEquals(0, jsonParsed.getPosCard());
    }

    /**
     * @brief:  on vérifie que l'Event parsé contiennent les bonnes valeurs
     */
    @Test
    public void parseUpdateBoardClientWellFormedEvent(){
        UpdateBoardClient jsonParsed = jsonReader.parseUpdateBoardClient("{" +
                "\"msg\":\"msg\"," +
                "\"currentPlayerId\":0," +
                "\"discard\":[0,1,2]," +
                "\"ante\":[3,4]," +
                "\"players\":[" +
                    "{\"flight\":[5,6]," +
                    "\"handSize\":4," +
                    "\"name\":\"player0\"," +
                    "\"id\":0," +
                    "\"coin\":50}," +

                    "{\"flight\":[11,12]," +
                    "\"handSize\":4," +
                    "\"name\":\"player1\"," +
                    "\"id\":1," +
                    "\"coin\":50}]," +

                "\"nbrCardDeck\":53," +
                "\"totalBet\":0," +
                "\"clientHand\":[7,8,9,10]," +
                "\"flagState\":0}"
        );

        for( int i = 0; i < 3; i++) {
            assertEquals( i, jsonParsed.getDiscard().get(i).getIdCard());
        }
        for( int i = 0; i < 2; i++) {
            assertEquals(i + 3, jsonParsed.getAnte().get(i).getIdCard());
        }
        for( int j = 0; j < 2; j++) {
            for (int i = 0; i < 2; i++) {
                assertEquals(i + 5 + j*6, jsonParsed.getPlayer(j).getFlight().get(i).getIdCard());
                assertEquals(4, jsonParsed.getPlayer(j).getHandSize());
                assertEquals("player" + j, jsonParsed.getPlayer(j).getName());
                assertEquals( j, jsonParsed.getPlayer(j).getId());
                assertEquals(50, jsonParsed.getPlayer(j).getCoin());
            }
        }
        assertEquals(53, jsonParsed.getNbrCardDeck());
        assertEquals(0, jsonParsed.getTotalBet());
        for( int i = 0; i < 4; i++) {
            assertEquals(i + 7, jsonParsed.getClientHand().get(i).getIdCard());
        }
    }

    /**
     * @brief:  on vérifie que l'Event parsé contiennent les bonnes valeurs
     */
    @Test
    public void parseChooseEndGameReturnWellFormedEvent(){
        ChooseEndGame jsonParsedFalse = jsonReader.parseChooseEndGame("{\"restart\":false,\"playerId\":0}");
        ChooseEndGame jsonParsedTrue = jsonReader.parseChooseEndGame("{\"restart\":true,\"playerId\":0}");
        assertEquals(false, jsonParsedFalse.isRestart());
        assertEquals(true, jsonParsedTrue.isRestart());
    }

    /**
     * @brief:  on vérifie que l'Event parsé contiennent les bonnes valeurs
     */
    @Test
    public void parseChoiceMadeReturnWellFormedEvent(){
        ChoiceMade jsonParsed = jsonReader.parseChoiceMade("{\"idChoose\":0,\"playerId\":0}");

        assertEquals(0, jsonParsed.getIndexChoose());
        assertEquals(0, jsonParsed.getPlayerId());
    }

    /**
     * @brief:  on vérifie que l'Event parsé contiennent les bonnes valeurs
     */
    @Test
    public void parseChoiceOptionReturnWellFormedEvent(){
        ArrayList<String> choix = new ArrayList<>();
        choix.add("la pilule rouge");
        choix.add("la pilule bleu");

        ChoiceOption jsonParsed = jsonReader.parseChoiceOption("{\"choiceOption\":[\"la pilule rouge\",\"la pilule bleu\"],\"playerId\":0}");

        assertEquals(choix, jsonParsed.getChoiceOption());
        assertEquals(0, jsonParsed.getTargetPlayerId());
    }
}
