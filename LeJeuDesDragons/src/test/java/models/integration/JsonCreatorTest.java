package models.integration;

import models.board.Board;
import models.jsonHandler.JsonCreator;
import models.statusBoard.Ante;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JsonCreatorTest {
    static JsonCreator jsonCreator;

    @BeforeEach
    public void prepareJsonHandler(){
        jsonCreator = new JsonCreator();
    }

    /**
     * @brief:  on vérifie que la string json retourné soit correcte
     */
    @Test
    public void joinGameReturnWellFormedEvent(){
        assertEquals("{\"name\":\"Bob\"}",jsonCreator.joinGame("Bob"));
    }

    /**
     * @brief:  on vérifie que la string json retourné soit correcte
     */
    @Test
    public void startGameReturnWellFormedEvent(){
        assertEquals("{\"id\":0}",jsonCreator.startGame(0));
    }

    /**
     * @brief:  on vérifie que la string json retourné soit correcte
     */
    @Test
    public void assignIdReturnWellFormedEvent(){
        assertEquals("{\"personalId\":0}",jsonCreator.assignId(0));
    }

    /**
     * @brief:  on vérifie que la string json retourné soit correcte
     */
    @Test
    public void playerPlayCardReturnWellFormedEvent(){
        assertEquals("{\"idPlayer\":0,\"posCard\":0}",jsonCreator.playerPlayCard(0,0));
    }

    /**
     * @brief:  on vérifie que la string json retourné soit correcte
     */
    @Test
    public void updateBoardClientReturnWellFormedEventIfNotAnte(){
        Board board = new Board();
        for (int i = 0; i < 3; i++) {
            board.moveCard(board.getDeck(), board.getDiscard(), 0);
        }
        for (int i = 0; i < 2; i++) {
            board.moveCard(board.getDeck(), board.getAnte(), 0);
        }
        for (int i = 0; i < 2; i++) {
            board.addPlayerBoard("player" + i);
            board.draw(board.getPlayers().get(i),6);
            for (int j = 0; j < 2; j++) {
                board.moveCard(board.getPlayers().get(i).getHand(), board.getPlayers().get(i).getFlight(), 0);
            }
        }

        ArrayList<String> jsonString = new ArrayList<String>();
        jsonString.add("{" +
                "\"msg\":\"msg\"," +
                "\"currentPlayerId\":0," +
                "\"discard\":[0,1,2]," +
                "\"ante\":[3,4]," +
                "\"players\":[" +
                "{\"flight\":[5,6]," +
                "\"handSize\":4," +
                "\"name\":\"player0\"," +
                "\"id\":0," +
                "\"coin\":50}," +

                "{\"flight\":[11,12]," +
                "\"handSize\":4," +
                "\"name\":\"player1\"," +
                "\"id\":1," +
                "\"coin\":50}]," +

                "\"nbrCardDeck\":53," +
                "\"totalBet\":0," +
                "\"clientHand\":[7,8,9,10]," +
                "\"flagState\":0}"
        );
        jsonString.add("{" +
                "\"msg\":\"msg\"," +
                "\"currentPlayerId\":0," +
                "\"discard\":[0,1,2]," +
                "\"ante\":[3,4]," +
                "\"players\":[" +
                "{\"flight\":[5,6]," +
                "\"handSize\":4," +
                "\"name\":\"player0\"," +
                "\"id\":0," +
                "\"coin\":50}," +

                "{\"flight\":[11,12]," +
                "\"handSize\":4," +
                "\"name\":\"player1\"," +
                "\"id\":1," +
                "\"coin\":50}]," +

                "\"nbrCardDeck\":53," +
                "\"totalBet\":0," +
                "\"clientHand\":[13,14,15,16]," +
                "\"flagState\":0}"
        );

        for (int i = 0; i < 2; i++) {
            assertEquals( jsonString.get(i), jsonCreator.updateBoardClient("msg", board, i));
        }
    }

    /**
     * @brief:  on vérifie que la string json retourné soit correcte
     */
    @Test
    public void updateBoardClientReturnWellFormedEventIfAnte(){
        Board board = new Board();
        board.setStatusBoard(new Ante(board));
        for (int i = 0; i < 3; i++) {
            board.moveCard(board.getDeck(), board.getDiscard(), 0);
        }
        for (int i = 0; i < 2; i++) {
            board.moveCard(board.getDeck(), board.getAnte(), 0);
        }
        for (int i = 0; i < 2; i++) {
            board.addPlayerBoard("player" + i);
            board.draw(board.getPlayers().get(i),6);
            for (int j = 0; j < 2; j++) {
                board.moveCard(board.getPlayers().get(i).getHand(), board.getPlayers().get(i).getFlight(), 0);
            }
        }

        ArrayList<String> jsonString = new ArrayList<>();
        jsonString.add("{" +
                "\"msg\":\"msg\"," +
                "\"currentPlayerId\":-1," +
                "\"discard\":[0,1,2]," +
                "\"ante\":[]," +
                "\"players\":[" +
                "{\"flight\":[5,6]," +
                "\"handSize\":4," +
                "\"name\":\"player0\"," +
                "\"id\":0," +
                "\"coin\":50}," +

                "{\"flight\":[11,12]," +
                "\"handSize\":4," +
                "\"name\":\"player1\"," +
                "\"id\":1," +
                "\"coin\":50}]," +

                "\"nbrCardDeck\":53," +
                "\"totalBet\":0," +
                "\"clientHand\":[7,8,9,10]," +
                "\"flagState\":1}"
        );
        jsonString.add("{" +
                "\"msg\":\"msg\"," +
                "\"currentPlayerId\":-1," +
                "\"discard\":[0,1,2]," +
                "\"ante\":[]," +
                "\"players\":[" +
                "{\"flight\":[5,6]," +
                "\"handSize\":4," +
                "\"name\":\"player0\"," +
                "\"id\":0," +
                "\"coin\":50}," +

                "{\"flight\":[11,12]," +
                "\"handSize\":4," +
                "\"name\":\"player1\"," +
                "\"id\":1," +
                "\"coin\":50}]," +

                "\"nbrCardDeck\":53," +
                "\"totalBet\":0," +
                "\"clientHand\":[13,14,15,16]," +
                "\"flagState\":1}"
        );

        for (int i = 0; i < 2; i++) {
            assertEquals( jsonString.get(i), jsonCreator.updateBoardClient("msg", board, i));
        }
    }

    /**
     * @brief:  on vérifie que la string json retourné soit correcte
     */
    @Test
    public void chooseEndGameReturnWellFormedEvent(){
        assertEquals( "{\"restart\":false,\"playerId\":0}", jsonCreator.chooseEndGame(0,false));
        assertEquals( "{\"restart\":true,\"playerId\":0}", jsonCreator.chooseEndGame(0,true));
    }

    /**
     * @brief:  on vérifie que la string json retourné soit correcte
     */
    @Test
    public void quitGameReturnWellFormedEvent(){
        //assertEquals( "{\"quit\":true}", jsonCreator.quitGame(true));
    }

    /**
     * @brief:  on vérifie que la string json retourné soit correcte
     */
    @Test
    public void choiceMadeReturnWellFormedEvent(){
        assertEquals( "{\"idChoose\":0,\"playerId\":0}", jsonCreator.choiceMade(0,0));
    }

    /**
     * @brief:  on vérifie que la string json retourné soit correcte
     */
    @Test
    public void choiceOptionReturnWellFormedEvent(){
        ArrayList<String> choix = new ArrayList<>();
        choix.add("la pilule rouge");
        choix.add("la pilule bleu");
        assertEquals( "{\"choiceOption\":[\"la pilule rouge\",\"la pilule bleu\"],\"playerId\":0}", jsonCreator.choiceOption(0, choix));
    }

}
