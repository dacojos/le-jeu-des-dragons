package models.integration;

import communication.server.DDServer;
import models.board.Board;
import models.card.Card;
import models.card.dragon.*;
import models.event.PlayerPlayCard;
import models.event.choice.ChoiceMade;
import models.player.Player;

import models.card.mortal.Mortal;

import models.power.dragon.color.*;
import models.power.dragon.metal.*;
import models.power.dragon.none.Dracolich;
import models.power.mortal.*;

import models.statusBoard.StatusId;
import models.statusBoard.WaitingPlay;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class PowerTest {
    static Board board;

    @BeforeEach
    public void createBoard(){
        board = new Board();
        board.setServer(new DDServer(board));

        board.addPlayerBoard("Alice");
        board.addPlayerBoard("Bob");
        board.addPlayerBoard("Camille");
        board.addPlayerBoard("Daniel");

        resetBoard();
    }

    public void resetBoard(){
        board.setCardPlayed( new ArrayList<>(Collections.nCopies(board.getPlayers().size(), null)));
        board.setAnte( new ArrayList<>());
        board.setHasBrelan( new ArrayList<>(Collections.nCopies(board.getPlayers().size(), false)));
        board.setHasColor( new ArrayList<>(Collections.nCopies(board.getPlayers().size(), false)));
        board.setDiscard(new ArrayList<>());

        for(Player p : board.getPlayers()){
            p.getHand().clear();
            p.getFlight().clear();
        }

        board.setStatusBoard(new WaitingPlay(board));
        board.setTotalBet(10);
        setHand2Card();
    }

    public void setHand2Card(){
        for(Player p : board.getPlayers()){
            p.addCardHand(new Card(100,"non vide",1));
            p.addCardHand(new Card(100,"non vide",1));
        }
    }

    @Test
    public void PowerAppliedIfPreviousAreStrongerOrNotExist(){
        board.setTotalBet(10);
        board.getPlayers().get(0).getHand().add(new Colored(0, "Dragon Noir", 9, new Black(), Color.BLACK));
        board.getPlayers().get(0).getHand().add(new Colored(0, "Dragon Noir", 9, new Black(), Color.BLACK));
        board.getPlayers().get(1).getHand().add(new Colored(1, "Dragon Noir", 7, new Black(), Color.BLACK));
        board.getPlayers().get(1).getHand().add(new Colored(1, "Dragon Noir", 7, new Black(), Color.BLACK));

        board.processEvent(new PlayerPlayCard(0,2));
        assertEquals(8, board.getTotalBet());
        board.processEvent(new PlayerPlayCard(1,2));
        assertEquals(6, board.getTotalBet());
    }

    @Test
    public void PowerNotAppliedIfPreviousAreWeaker(){
        board.setTotalBet(10);
        board.getPlayers().get(0).getHand().add(new Colored(0, "Dragon Noir", 7, new Black(), Color.BLACK));
        board.getPlayers().get(0).getHand().add(new Colored(0, "Dragon Noir", 7, new Black(), Color.BLACK));
        board.getPlayers().get(1).getHand().add(new Colored(1, "Dragon Noir", 9, new Black(), Color.BLACK));
        board.getPlayers().get(1).getHand().add(new Colored(1, "Dragon Noir", 9, new Black(), Color.BLACK));

        board.processEvent(new PlayerPlayCard(0,2));
        board.processEvent(new PlayerPlayCard(1,2));
        assertEquals(8, board.getTotalBet());
    }

    @Test
    public void BlackPowerSteal1CoinAndSetAnteIfTotalBetHas1(){
        board.setTotalBet(1);
        board.getPlayers().get(0).getHand().add(new Colored(0, "Dragon Noir", 9, new Black(), Color.BLACK));

        board.processEvent(new PlayerPlayCard(0,2));
        assertEquals(0, board.getTotalBet());
        assertEquals( StatusId.ANTE, board.getStatusBoard().getStateId());
    }

    @Test
    public void BlackPowerSteal2CoinAndSetAnteIfTotalBetHas2(){
        board.setTotalBet(2);
        board.getPlayers().get(0).getHand().add(new Colored(0, "Dragon Noir", 9, new Black(), Color.BLACK));

        board.processEvent(new PlayerPlayCard(0,2));
        assertEquals(0, board.getTotalBet());
        assertEquals( StatusId.ANTE, board.getStatusBoard().getStateId());
    }

    @Test
    public void BlackPowerSteal2CoinIfTotalBetHasMoreThan2(){
        board.setTotalBet(10);
        board.getPlayers().get(0).getHand().add(new Colored(0, "Dragon Noir", 9, new Black(), Color.BLACK));
        board.getPlayers().get(0).getHand().add(new Colored(0, "Dragon Noir", 9, new Black(), Color.BLACK));
        board.getPlayers().get(1).getHand().add(new Colored(1, "Dragon Noir", 7, new Black(), Color.BLACK));
        board.getPlayers().get(1).getHand().add(new Colored(1, "Dragon Noir", 7, new Black(), Color.BLACK));

        board.processEvent(new PlayerPlayCard(0,2));
        board.processEvent(new PlayerPlayCard(1,2));
        assertEquals(6, board.getTotalBet());
    }

    @Test
    public void WhitePowerSteal0CoinsIfMortalNotPresent(){
        board.setTotalBet(10);
        board.getPlayers().get(0).getHand().add(new Colored(0, "Dragon Blanc", 9, new White(), Color.WHITE));
        board.getPlayers().get(0).getHand().add(new Colored(0, "Dragon Blanc", 9, new White(), Color.WHITE));

        board.processEvent(new PlayerPlayCard(0,2));
        assertEquals(10, board.getTotalBet());
        assertEquals( StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
    }

    @Test
    public void WhitePowerSteal1CoinAndSetAnteIfTotalBetHas1AndMortalIsPresent(){
        board.setTotalBet(1);
        board.getPlayers().get(2).getFlight().add(new Mortal(0, "L'Archimage", 9, new Archmage()));
        board.getPlayers().get(0).getHand().add(new Colored(0, "Dragon Blanc", 9, new White(), Color.WHITE));

        board.processEvent(new PlayerPlayCard(0,2));
        assertEquals(0, board.getTotalBet());
        assertEquals( StatusId.ANTE, board.getStatusBoard().getStateId());
    }

    @Test
    public void WhitePowerSteal3CoinAndSetAnteIfTotalBetHas3AndMortalIsPresent(){
        board.setTotalBet(3);
        board.getPlayers().get(2).getFlight().add(new Mortal(0, "L'Archimage", 9, new Archmage()));
        board.getPlayers().get(0).getHand().add(new Colored(0, "Dragon Blanc", 7, new White(), Color.WHITE));

        board.processEvent(new PlayerPlayCard(0,2));
        assertEquals(0, board.getTotalBet());
        assertEquals( StatusId.ANTE, board.getStatusBoard().getStateId());
    }

    @Test
    public void WhitePowerSteal3CoinIfTotalBetHasMoreThan3AndMortalIsPresent(){
        board.setTotalBet(10);
        board.getPlayers().get(2).getFlight().add(new Mortal(0, "L'Archimage", 9, new Archmage()));
        board.getPlayers().get(0).getHand().add(new Colored(0, "Dragon Blanc", 9, new White(), Color.WHITE));
        board.getPlayers().get(0).getHand().add(new Colored(1, "Dragon Blanc", 7, new White(), Color.WHITE));

        board.processEvent(new PlayerPlayCard(0,2));
        assertEquals(7, board.getTotalBet());
    }

    @Test
    public void GoldPowerDraw1CardPerGoodInFlight(){
        for (int i = 0; i < 3; i++){
            resetBoard();

            board.getPlayers().get(0).getFlight().add(new Colored(1, "Dragon Blanc", 7, new White(), Color.WHITE));
            for( int j = 0; j < i; j++) {
                board.getPlayers().get(0).getFlight().add(new Metallic(50, "Dragon D'Or", 13, new Gold(), Metal.GOLD));
            }

            board.getPlayers().get(0).getHand().add(new Metallic(50, "Dragon D'Or", 3, new Gold(), Metal.GOLD));
            board.getPlayers().get(0).getHand().add(new Metallic(50, "Dragon D'Or", 3, new Gold(), Metal.GOLD));

            board.processEvent(new PlayerPlayCard(0,2));

            assertEquals(4 + i, board.getPlayers().get(0).getSizeHand());
        }
    }

    @Test
    public void SilverPowerDraw1CardIfGoodInFlight(){
        board.getPlayers().get(1).getFlight().add(new Metallic(50, "Dragon D'Or", 13, new Gold(), Metal.GOLD));
        board.getPlayers().get(2).getFlight().add(new Metallic(50, "Dragon D'Or", 13, new Gold(), Metal.GOLD));
        for(int i = 0; i < board.getNumberPlayer(); i++) {
            board.getPlayers().get(i).getFlight().add(new Colored(1, "Dragon Blanc", 7, new White(), Color.WHITE));

        }

        board.getPlayers().get(0).getHand().add(new Metallic(50, "Dragon D'Argent", 3, new Silver(), Metal.SILVER));
        board.getPlayers().get(0).getHand().add(new Metallic(50, "Dragon D'Argent", 3, new Silver(), Metal.SILVER));
        board.getPlayers().get(1).getHand().add(new Metallic(50, "Dragon D'Argent", 3, new Silver(), Metal.SILVER));

        board.processEvent(new PlayerPlayCard(0,2));

        assertEquals(4, board.getPlayers().get(0).getSizeHand());
        assertEquals(4, board.getPlayers().get(1).getSizeHand());
        assertEquals(3, board.getPlayers().get(2).getSizeHand());
        assertEquals(2, board.getPlayers().get(3).getSizeHand());
    }

    @Test
    public void FoolPowerPay1AndDraw1PerFlightStronger(){
        for(int i = 0; i < board.getNumberPlayer(); i++) {
            resetBoard();
            for (int j = 1; j <= i; j++) {
                board.getPlayers().get(j).getFlight().add(new Metallic(50, "Dragon D'Or", 13, new Gold(), Metal.GOLD));
            }

            board.getPlayers().get(0).getHand().add(new Mortal(64, "L'Idiot",3, new Fool()));
            board.getPlayers().get(0).getHand().add(new Mortal(64, "L'Idiot",3, new Fool()));

            board.processEvent(new PlayerPlayCard(0,2));
            assertEquals(3 + i, board.getPlayers().get(0).getSizeHand());
            assertEquals(11,board.getTotalBet());
        }
    }

    @Test
    public void DruidPowerPay1AndWeakestWin(){
        for (int j = 1; j < board.getNumberPlayer(); j++) {
            board.getPlayers().get(j).getFlight().add(new Metallic(50, "Dragon D'Or", 6+j, new Gold(), Metal.GOLD));
        }

        board.getPlayers().get(0).getHand().add(new Mortal(63, "La Druidesse", 6, new Druid()));
        board.getPlayers().get(0).getHand().add(new Mortal(63, "La Druidesse", 6, new Druid()));

        board.processEvent(new PlayerPlayCard(0,2));

        assertEquals(11,board.getTotalBet());
        assertEquals(true, board.isDruid());

        board.getStatusBoard().setNbrTurn(4);
        board.getStatusBoard().setNbrRound(3);
        board.getStatusBoard().tryEndRound();

        assertEquals(60,board.getPlayers().get(0).getCoin());
        assertEquals(false, board.isDruid());
    }

    @Test
    public void PriestPowerPay1AndBecomeLeader(){
        board.getPlayers().get(0).getHand().add(new Metallic(50, "Dragon D'Or", 13, new Gold(), Metal.GOLD));
        board.getPlayers().get(0).getHand().add(new Metallic(50, "Dragon D'Or", 13, new Gold(), Metal.GOLD));
        board.getPlayers().get(1).getHand().add(new Mortal(65, "Le Prêtre", 5, new Priest()));
        board.getPlayers().get(1).getHand().add(new Mortal(65, "Le Prêtre", 5, new Priest()));

        board.processEvent(new PlayerPlayCard(0,2));
        board.processEvent(new PlayerPlayCard(1,2));

        assertEquals(11,board.getTotalBet());
        assertEquals(1, board.getIdPlayerPriest());

        board.getStatusBoard().setNbrTurn(4);
        board.getStatusBoard().tryEndRound();

        assertEquals(1,board.getStatusBoard().getCurrentPlayerId());
        assertEquals(-1, board.getIdPlayerPriest());
    }

    @Test
    public void BahamutPowerAdd10CoinPerFlightWithBadAndGood(){
        for(int i = 0; i < board.getNumberPlayer(); i++) {
            resetBoard();

            for(Player p: board.getPlayers()){
               p.getFlight().add(new Mortal(64, "L'Idiot",3, new Fool()));
            }

            for (int j = 1; j <= i; j++) {
                board.getPlayers().get(j).getFlight().add(new Metallic(50, "Dragon D'Or", 13, new Gold(), Metal.GOLD));
                board.getPlayers().get(i).getFlight().add(new Colored(1, "Dragon Blanc", 7, new White(), Color.WHITE));
            }

            board.getPlayers().get(0).getHand().add(new Metallic(1, "Bahamut", 13, new Bahamut(), Metal.DIVIN));
            board.getPlayers().get(0).getHand().add(new Metallic(1, "Bahamut", 13, new Bahamut(), Metal.DIVIN));

            board.processEvent(new PlayerPlayCard(0,2));

            assertEquals(50+i*10, board.getPlayers().get(0).getCoin());
            for (int j = 1; j <= i; j++) {
                assertEquals(40, board.getPlayers().get(j).getCoin());
            }
        }
    }

    @Test
    public void FlightWithTiamatAndGoodShouldNotWin(){
        board.getPlayers().get(1).getFlight().add(new Metallic(50, "Dragon D'Or", 13, new Gold(), Metal.GOLD));
        board.getPlayers().get(0).getFlight().add(new Metallic(50, "Dragon D'Or", 13, new Gold(), Metal.GOLD));
        board.getPlayers().get(0).getFlight().add(new Colored(1, "Dragon Blanc", 7, new White(), Color.WHITE));

        board.getPlayers().get(0).getHand().add(new Colored(67, "Tiamat", 13, new Tiamat(), Color.DIVIN));
        board.getPlayers().get(0).getHand().add(new Colored(67, "Tiamat", 13, new Tiamat(), Color.DIVIN));

        board.processEvent(new PlayerPlayCard(0,0));
        board.getStatusBoard().setNbrTurn(4);
        board.getStatusBoard().setNbrRound(3);
        board.getStatusBoard().tryEndRound();
        assertEquals(60,board.getPlayers().get(1).getCoin());
    }

    @Test
    public void FlightWithBahamutAndBadShouldNotWin(){
        board.getPlayers().get(1).getFlight().add(new Metallic(50, "Dragon D'Or", 13, new Gold(), Metal.GOLD));
        board.getPlayers().get(0).getFlight().add(new Metallic(50, "Dragon D'Or", 13, new Gold(), Metal.GOLD));
        board.getPlayers().get(0).getFlight().add(new Colored(1, "Dragon Blanc", 7, new White(), Color.WHITE));

        board.getPlayers().get(0).getHand().add(new Metallic(1, "Bahamut", 13, new Bahamut(), Metal.DIVIN));
        board.getPlayers().get(0).getHand().add(new Metallic(1, "Bahamut", 13, new Bahamut(), Metal.DIVIN));

        board.processEvent(new PlayerPlayCard(0,0));
        board.getStatusBoard().setNbrTurn(4);
        board.getStatusBoard().setNbrRound(3);
        board.getStatusBoard().tryEndRound();
        assertEquals(60,board.getPlayers().get(1).getCoin());
    }

    @Test
    public void ThiefPowerSteal1CoinAndSetAnteIfTotalBetHas1(){
        board.setTotalBet(1);
        board.getPlayers().get(0).getHand().add(new Mortal(69, "La Voleuse", 7, new Thief()));
        board.getPlayers().get(0).getHand().add(new Mortal(69, "La Voleuse", 7, new Thief()));

        board.processEvent(new PlayerPlayCard(0,2));
        assertEquals(0, board.getTotalBet());
        assertEquals( StatusId.CHOICE, board.getStatusBoard().getStateId());

        board.processEvent(new ChoiceMade(0,0));
        assertEquals( StatusId.ANTE, board.getStatusBoard().getStateId());
    }

    @Test
    public void ThiefPowerSteal7CoinAndSetAnteIfTotalBetHas7(){
        board.setTotalBet(7);
        board.getPlayers().get(0).getHand().add(new Mortal(69, "La Voleuse", 7, new Thief()));
        board.getPlayers().get(0).getHand().add(new Mortal(69, "La Voleuse", 7, new Thief()));

        board.processEvent(new PlayerPlayCard(0,2));
        assertEquals(0, board.getTotalBet());
        assertEquals( StatusId.CHOICE, board.getStatusBoard().getStateId());

        board.processEvent(new ChoiceMade(0,0));
        assertEquals( StatusId.ANTE, board.getStatusBoard().getStateId());
    }

    @Test
    public void ThiefPowerSteal7CoinIfTotalBetHasMoreThan7(){
        board.setTotalBet(10);
        board.getPlayers().get(0).getHand().add(new Mortal(69, "La Voleuse", 7, new Thief()));
        board.getPlayers().get(0).getHand().add(new Mortal(69, "La Voleuse", 7, new Thief()));

        board.processEvent(new PlayerPlayCard(0,2));
        assertEquals(3, board.getTotalBet());
        assertEquals( StatusId.CHOICE, board.getStatusBoard().getStateId());

        board.processEvent(new ChoiceMade(0,0));
        assertEquals( StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
    }

    @Test
    public void DragonSlayerPowerSendWeakerDragonFromFlightToDiscard(){
        board.getPlayers().get(1).getFlight().add(new Colored(1, "Dragon Blanc", 7, new White(), Color.WHITE));

        board.getPlayers().get(0).getHand().add(new Mortal(68, "Le Tueur De Dragon", 8, new Dragonslayer()));
        board.getPlayers().get(0).getHand().add(new Mortal(68, "Le Tueur De Dragon", 8, new Dragonslayer()));

        board.processEvent(new PlayerPlayCard(0,2));
        assertEquals( StatusId.CHOICE, board.getStatusBoard().getStateId());

        board.processEvent(new ChoiceMade(0,0));
        assertEquals( StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
    }

    @Test
    public void DragonSlayerPowerCanKillYourDragon(){
        board.getPlayers().get(0).getFlight().add(new Colored(1, "Dragon Blanc", 7, new White(), Color.WHITE));

        board.getPlayers().get(0).getHand().add(new Mortal(68, "Le Tueur De Dragon", 8, new Dragonslayer()));
        board.getPlayers().get(0).getHand().add(new Mortal(68, "Le Tueur De Dragon", 8, new Dragonslayer()));

        board.processEvent(new PlayerPlayCard(0,2));
        assertEquals( StatusId.CHOICE, board.getStatusBoard().getStateId());

        board.processEvent(new ChoiceMade(0,0));
        assertEquals( StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
    }

    @Test
    public void DragonSlayerPowerCanNotTargetStrongerDragon(){
        board.getPlayers().get(1).getFlight().add(new Colored(1, "Dragon Blanc", 9, new White(), Color.WHITE));

        board.getPlayers().get(0).getHand().add(new Mortal(68, "Le Tueur De Dragon", 8, new Dragonslayer()));
        board.getPlayers().get(0).getHand().add(new Mortal(68, "Le Tueur De Dragon", 8, new Dragonslayer()));

        board.processEvent(new PlayerPlayCard(0,2));
        assertEquals( StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
    }



    @Test
    public void DracolichPowerUseHisStrengthWithOtherPower(){

        board.getPlayers().get(1).getFlight().add(new Colored(1, "Dragon Vert", 7, new Green(), Color.GREEN));

        board.getPlayers().get(0).getHand().add(new Mortal(68, "Le Tueur De Dragon", 8, new Dragonslayer()));
        board.getPlayers().get(0).getHand().add(new Mortal(68, "Le Tueur De Dragon", 8, new Dragonslayer()));

        board.processEvent(new PlayerPlayCard(0,2));
        board.processEvent(new ChoiceMade(0,0));
        //TODO
    }

    @Test
    public void DracolichPowerCanTargetYourDragon(){
        board.getPlayers().get(0).getFlight().add(new Colored(1, "Dragon Noir", 7, new Black(), Color.BLACK));

        board.getPlayers().get(0).getHand().add(new None(2, "Dracolich", 10, new Dracolich(), "Dragon Mort-Vivant"));
        board.getPlayers().get(0).getHand().add(new None(2, "Dracolich", 10, new Dracolich(), "Dragon Mort-Vivant"));

        board.processEvent(new PlayerPlayCard(0,2));
        assertEquals( StatusId.CHOICE, board.getStatusBoard().getStateId());

        board.processEvent(new ChoiceMade(0,0));
        assertEquals( StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
        assertEquals(8, board.getTotalBet());
    }

    @Test
    public void DracolichPowerCanNotTargetGoodDragon(){
        board.getPlayers().get(1).getFlight().add(new Metallic(1, "Dragon D'or", 13, new Gold(), Metal.GOLD));

        board.getPlayers().get(0).addCardHand(new None(2, "Dracolich", 10, new Dracolich(), "Dragon Mort-Vivant"));
        board.getPlayers().get(0).addCardHand(new None(2, "Dracolich", 10, new Dracolich(), "Dragon Mort-Vivant"));

        board.processEvent(new PlayerPlayCard(0,2));
        assertEquals( StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
    }

    @Test
    public void ArchMagePowerCopyPowerFromAnte(){
        board.setStatusBoard(new WaitingPlay(board));
        board.setAnte(new ArrayList<>());
        board.getAnte().add(new Colored(1, "Dragon Blanc", 7, new White(), Color.WHITE));
        board.getAnte().add(new Colored(1, "Dragon Vert", 1, new Green(), Color.GREEN));

        board.getPlayers().get(0).getHand().add(new Mortal(0, "L'Archimage", 9, new Archmage()));
        board.getPlayers().get(0).getHand().add(new Mortal(0, "L'Archimage", 9, new Archmage()));
        board.getPlayers().get(0).getHand().add(new Mortal(0, "L'Archimage", 9, new Archmage()));

        board.processEvent(new PlayerPlayCard(0,2));
        assertEquals( StatusId.CHOICE, board.getStatusBoard().getStateId());

        board.processEvent(new ChoiceMade(0,0));
        assertEquals( StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
        assertEquals( 8, board.getTotalBet());
    }

    @Test
    public void ArchMageAndDracolichUseTheirStrenghtWhenCopyPower(){
        board.setStatusBoard(new WaitingPlay(board));
        board.setAnte(new ArrayList<>());
        board.getAnte().add(new Colored(1, "Dragon Vert", 1, new Green(), Color.GREEN));

        board.getPlayers().get(0).addCardHand(new None(3, "Dracolich", 10, new Dracolich(), "Dragon Mort-Vivant"));
        board.getPlayers().get(0).addCardFlight(new Colored(2, "Dragon Vert", 5, new Green(), Color.GREEN));

        board.getPlayers().get(1).addCardHand(new Mortal(1, "L'Archimage", 9, new Archmage()));
        board.getPlayers().get(1).addCardHand(new Colored(4, "Dragon Vert", 1, new Green(), Color.GREEN));
        board.getPlayers().get(1).addCardHand(new Colored(5, "Dragon Vert", 5, new Green(), Color.GREEN));
        board.getPlayers().get(1).addCardHand(new Colored(6, "Dragon Vert", 9, new Green(), Color.GREEN));

        board.getPlayers().get(2).addCardHand(new Colored(7, "Dragon Vert", 1, new Green(), Color.GREEN));
        board.getPlayers().get(2).addCardHand(new Colored(8, "Dragon Vert", 5, new Green(), Color.GREEN));
        board.getPlayers().get(2).addCardHand(new Colored(9, "Dragon Vert", 10, new Green(), Color.GREEN));

        board.processEvent( new PlayerPlayCard(0,2));
        assertEquals( StatusId.CHOICE, board.getStatusBoard().getStateId());

        board.processEvent( new ChoiceMade(0,0));
        board.processEvent( new ChoiceMade(1,2));
        assertEquals( StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
        assertEquals( 5, board.getPlayers().get(0).getHand().get(2).getStrength());

        board.processEvent(new PlayerPlayCard(1,2));
        assertEquals( StatusId.CHOICE, board.getStatusBoard().getStateId());

        board.processEvent(new ChoiceMade(1,0));
        board.processEvent(new ChoiceMade(2,2));

        assertEquals( 5, board.getPlayers().get(1).getHand().get(4).getStrength());
    }



    @Test
    public void PrincessPower(){
        ArrayList<Card> goodList = new ArrayList<>();
        goodList.add( new Metallic(0, "Dragon De Cuivre", 10, new Copper(), Metal.COPPER));
        goodList.add( new Metallic(1, "Dragon d'Airain", 4, new Brass(), Metal.BRASS));
        goodList.add( new Metallic(2, "Dragon D'or", 13, new Gold(), Metal.GOLD));
        goodList.add( new Metallic(3, "Dragon D'Argent", 3, new Silver(), Metal.SILVER));
        goodList.add( new Metallic(4, "Dragon De Bronze", 3, new Bronze(), Metal.BRONZE));
        goodList.add( new Metallic(5, "Bahamut", 13, new Bahamut(), Metal.DIVIN));

        for(int i = 0; i < goodList.size(); i++){
            resetBoard();
            board.setAnte(new ArrayList<>());

            for(int j = 0; j < i; j++){
                board.getPlayers().get(0).addCardFlight(goodList.get(j));
            }

            board.getPlayers().get(0).addCardHand( new Mortal(66, "La Princesse", 4, new Princess()));
            board.getPlayers().get(0).addCardHand( new Mortal(66, "La Princesse", 4, new Princess()));

            board.processEvent( new PlayerPlayCard(0, 2));

            for(int k = 0; k < i; k++) {
                assertEquals( StatusId.CHOICE, board.getStatusBoard().getStateId());
                assertEquals( true, board.getStatusBoard().isPrincess());
                board.processEvent( new ChoiceMade(0, 0));
            }
            assertEquals( false, board.getStatusBoard().isPrincess());
        }
    }


    private void initBronzeTests(int handSize, int[] strength){
        resetBoard();
        for(int j = 0; j < 3; j++){
            board.getAnte().add(new Card( j,"Card", strength[j]));
        }

        //avoir assez cartes avant de jouer pour tester la limite de main
        for(int j = 2; j < handSize; j++) {
            board.getPlayers().get(0).addCardHand( new Metallic(28, "Dragon De Bronze", 3, new Bronze(), Metal.BRONZE));
        }

        board.processEvent( new PlayerPlayCard(0, 2));
    }

    @Test
    public void BronzePowerCase2DifferentStrength8To10Hand(){
        for( int i = 0; i < 3; i++) {
            initBronzeTests(9 + i, new int[]{1,2,3});

            assertEquals( StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
            assertEquals( Board.NB_MAX_CARD_HAND, board.getPlayers().get(0).getSizeHand());
            for(int j = 0; j < 2; j++) {
                if(j >= i) {
                    assertEquals( j - i, board.getPlayers().get(0).getHand().get(j + 8).getIdCard());
                }
                else{
                    assertEquals(28, board.getPlayers().get(0).getHand().get(j + 8).getIdCard());
                }
            }
        }
    }

    @Test
    public void BronzePowerCase3MinimumStrength8Hand() {
        initBronzeTests(9, new int[]{1, 1, 1});
        assertEquals(StatusId.CHOICE, board.getStatusBoard().getStateId());

        board.processEvent(new ChoiceMade(0, 1));
        assertEquals(StatusId.CHOICE, board.getStatusBoard().getStateId());

        board.processEvent(new ChoiceMade(0, 1));
        assertEquals(StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());

        assertEquals(Board.NB_MAX_CARD_HAND, board.getPlayers().get(0).getSizeHand());
        assertEquals(1, board.getPlayers().get(0).getHand().get(8).getIdCard());
        assertEquals(2, board.getPlayers().get(0).getHand().get(9).getIdCard());
    }

    @Test
    public void BronzePowerCase3MinimumStrength9Hand() {
        initBronzeTests(10, new int[]{1, 1, 1});
        assertEquals(StatusId.CHOICE, board.getStatusBoard().getStateId());

        board.processEvent(new ChoiceMade(0, 1));
        assertEquals(StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());

        assertEquals(Board.NB_MAX_CARD_HAND, board.getPlayers().get(0).getSizeHand());
        assertEquals(28, board.getPlayers().get(0).getHand().get(8).getIdCard());
        assertEquals(1, board.getPlayers().get(0).getHand().get(9).getIdCard());
    }

    @Test
    public void BronzePowerCase3MinimumStrength10Hand() {
        initBronzeTests(11, new int[]{1, 1, 1});
        assertEquals(StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());

        assertEquals(Board.NB_MAX_CARD_HAND, board.getPlayers().get(0).getSizeHand());
        assertEquals(28, board.getPlayers().get(0).getHand().get(8).getIdCard());
        assertEquals(28, board.getPlayers().get(0).getHand().get(9).getIdCard());
    }

    @Test
    public void BronzePowerCase1minimumStrength2Stronger8Hand() {
        initBronzeTests(9, new int[]{1, 2, 2});
        assertEquals(Board.NB_MAX_CARD_HAND - 1, board.getPlayers().get(0).getSizeHand());
        assertEquals(StatusId.CHOICE, board.getStatusBoard().getStateId());

        board.processEvent(new ChoiceMade(0, 1));
        assertEquals(StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());

        assertEquals(Board.NB_MAX_CARD_HAND, board.getPlayers().get(0).getSizeHand());
        assertEquals(0, board.getPlayers().get(0).getHand().get(8).getIdCard());
        assertEquals(2, board.getPlayers().get(0).getHand().get(9).getIdCard());
    }

    @Test
    public void BronzePowerCase1minimumStrength2Stronger9Hand(){
        initBronzeTests(10, new int[]{1, 2, 2});

        assertEquals(StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
        assertEquals( Board.NB_MAX_CARD_HAND, board.getPlayers().get(0).getSizeHand());
        assertEquals( 28, board.getPlayers().get(0).getHand().get(8).getIdCard());
        assertEquals( 0, board.getPlayers().get(0).getHand().get(9).getIdCard());
    }

    @Test
    public void BronzePowerCase1minimumStrength2Stronger10Hand() {
        initBronzeTests(11, new int[]{1, 2, 2});
        assertEquals(StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());

        assertEquals(Board.NB_MAX_CARD_HAND, board.getPlayers().get(0).getSizeHand());
        assertEquals(28, board.getPlayers().get(0).getHand().get(8).getIdCard());
        assertEquals(28, board.getPlayers().get(0).getHand().get(9).getIdCard());
    }

    @Test
    public void BronzePowerCase2minimumStrengthWith8Hand(){
        initBronzeTests( 9, new int[]{1, 1, 2});
        assertEquals(StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
        assertEquals( Board.NB_MAX_CARD_HAND, board.getPlayers().get(0).getSizeHand());
        assertEquals( 0, board.getPlayers().get(0).getHand().get(8).getIdCard());
        assertEquals( 1, board.getPlayers().get(0).getHand().get(9).getIdCard());
    }

    @Test
    public void BronzePowerCase2minimumStrength9Hand(){
        initBronzeTests( 10, new int[]{1, 1, 2});
        assertEquals(StatusId.CHOICE, board.getStatusBoard().getStateId());

        board.processEvent(new ChoiceMade(0, 1));
        assertEquals(StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());

        assertEquals( Board.NB_MAX_CARD_HAND, board.getPlayers().get(0).getSizeHand());
        assertEquals( 28, board.getPlayers().get(0).getHand().get(8).getIdCard());
        assertEquals( 1, board.getPlayers().get(0).getHand().get(9).getIdCard());
    }

    @Test
    public void BronzePowerCase2minimumStrength10Hand() {
        initBronzeTests(11, new int[]{1, 1, 2});
        assertEquals(StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());

        assertEquals(Board.NB_MAX_CARD_HAND, board.getPlayers().get(0).getSizeHand());
        assertEquals(28, board.getPlayers().get(0).getHand().get(8).getIdCard());
        assertEquals(28, board.getPlayers().get(0).getHand().get(9).getIdCard());
    }

    @Test
    public void CopperPowerReplaceHimSelfByFirstInDeckIfIsCardPlayed(){
        board.setAnte(new ArrayList<>());

        board.getPlayers().get(0).addCardHand( new Metallic(0, "Dragon De Cuivre", 10, new Copper(), Metal.COPPER));
        board.getPlayers().get(0).addCardHand( new Metallic(0, "Dragon De Cuivre", 10, new Copper(), Metal.COPPER));

        board.processEvent( new PlayerPlayCard(0, 2));
        assertEquals("L'Archimage 9", board.getCardPlayed().get(0).toString());
        assertEquals( StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
    }

    @Test
    public void CopperPowerReplaceHimSelfByFirstInDeckIfIsNotCardPlayed(){
        board.setAnte(new ArrayList<>());

        board.getPlayers().get(0).addCardFlight( new Metallic(0, "Dragon De Cuivre", 10, new Copper(), Metal.COPPER));

        board.getPlayers().get(0).addCardHand( new Mortal(66, "La Princesse", 4, new Princess()));
        board.getPlayers().get(0).addCardHand( new Mortal(66, "La Princesse", 4, new Princess()));

        board.processEvent( new PlayerPlayCard(0, 2));
        board.processEvent( new ChoiceMade(0, 0));

        assertEquals( StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
        assertEquals("L'Archimage 9", board.getPlayers().get(0).getFlight().get(0).toString());
    }

    private ArrayList<Card> createListBad(){
        ArrayList<Card> badList = new ArrayList<>();

        badList.add(new Colored(8, "Tiamat", 9, new Tiamat(), Color.DIVIN));
        badList.add(new Colored(4, "Dragon Vert", 1, new Green(), Color.GREEN));
        badList.add(new Colored(5, "Dragon Rouge", 5, new Red(), Color.RED));
        badList.add(new Colored(0, "Dragon Bleu", 5, new Green(), Color.BLUE));
        badList.add(new Colored(6, "Dragon Noir", 9, new Black(), Color.BLACK));
        badList.add(new Colored(7, "Dragon Blanc", 9, new White(), Color.WHITE));

        return badList;
    }

    @Test
    public void BluePowerOptionStealCoinEqualToNbBadInCurrentPlayerFlight(){
        resetBoard();
        ArrayList<Card> badList = createListBad();


        for(int i = 0; i < badList.size(); i++){
            resetBoard();

            for(int j = 0; j < i ; j++) {
                board.getPlayers().get(0).addCardFlight(badList.get(i));
            }

            board.getPlayers().get(0).addCardHand(new Colored(0, "Dragon Bleu", 10, new Blue(), Color.BLUE));
            board.getPlayers().get(0).addCardHand(new Colored(0, "Dragon Bleu", 10, new Blue(), Color.BLUE));

            board.processEvent(new PlayerPlayCard(0, 2));
            assertEquals(StatusId.CHOICE, board.getStatusBoard().getStateId());

            board.processEvent(new ChoiceMade(0, 0));
            assertEquals(9-i, board.getTotalBet());
            assertEquals(StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
        }
    }

    @Test
    public void BluePowerOptionOtherAddCoinToBetEqualToNbBadInCurrentPlayerFlight(){
        resetBoard();
        board.setTotalBet(50);
        ArrayList<Card> bad = createListBad();

        for(int i = 0; i < bad.size(); i++){
            resetBoard();

            for(int j = 0; j < i ; j++) {
                board.getPlayers().get(0).addCardFlight(bad.get(i));
            }

            board.getPlayers().get(0).addCardHand(new Colored(0, "Dragon Bleu", 10, new Blue(), Color.BLUE));
            board.getPlayers().get(0).addCardHand(new Colored(0, "Dragon Bleu", 10, new Blue(), Color.BLUE));

            board.processEvent(new PlayerPlayCard(0, 2));
            assertEquals(StatusId.CHOICE, board.getStatusBoard().getStateId());

            board.processEvent(new ChoiceMade(0, 1));
            assertEquals( 10 + (board.getNumberPlayer()-1)*(i+1), board.getTotalBet());
            assertEquals(StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
        }
    }

    @Test
    public void GreenPowerAffectLeftPlayerAndOptionAreGoldAndWeakerBadInOpponentHand(){
        for(int i = 0; i <= 5; i++) {
            resetBoard();

            board.getPlayers().get(0).addCardHand(new Colored(0, "Dragon Vert", 10, new Green(), Color.GREEN));
            board.getPlayers().get(0).addCardHand(new Colored(0, "Dragon Vert", 10, new Green(), Color.GREEN));

            board.getPlayers().get(1).addCardHand(new Mortal(1, "L'Archimage", 9, new Archmage()));
            board.getPlayers().get(1).addCardHand(new Metallic(2, "Dragon Or", 6, new Gold(), Metal.GOLD));
            board.getPlayers().get(1).addCardHand(new Colored(3, "Dragon Bleu", 5, new Blue(), Color.BLUE));
            board.getPlayers().get(1).addCardHand(new Colored(4, "Dragon Vert", 1, new Green(), Color.GREEN));
            board.getPlayers().get(1).addCardHand(new Colored(5, "Dragon Rouge", 5, new Red(), Color.RED));
            board.getPlayers().get(1).addCardHand(new Colored(6, "Dragon Noir", 9, new Black(), Color.BLACK));
            board.getPlayers().get(1).addCardHand(new Colored(7, "Dragon Blanc", 9, new White(), Color.WHITE));
            board.getPlayers().get(1).addCardHand(new Colored(8, "Tiamat", 9, new Tiamat(), Color.DIVIN));

            board.processEvent(new PlayerPlayCard(0, 2));
            assertEquals(StatusId.CHOICE, board.getStatusBoard().getStateId());

            board.processEvent(new ChoiceMade(1, i));
            assertEquals(StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
            if(i == 0){
                assertEquals(55, board.getPlayers().get(0).getCoin());
            }
            else {
                assertEquals(i + 2, board.getPlayers().get(0).getHand().get(3).getIdCard());
            }
        }
    }

    @Test
    public void IfMoreThan1PlayerWithStrongestFlightBrassAndRedPowerAffectCurrentPlayerChooseTarget(){
        for(Player p : board.getPlayers()){
            p.addCardHand(new Metallic(0,"card", 5,null));
            p.addCardHand(new Metallic(0,"card", 5,null));
        }

        board.getPlayers().get(0).addCardFlight(new Card("card",8));
        board.getPlayers().get(1).addCardFlight(new Card("card",5));
        board.getPlayers().get(3).addCardFlight(new Card("card",5));

        board.getPlayers().get(0).addCardHand(new Colored(0, "Dragon Rouge", 5, new Red(), Color.RED));
        board.getPlayers().get(0).addCardHand(new Metallic(1, "Dragon d'Airain", 4, new Brass(), Metal.BRASS));
        board.getPlayers().get(0).addCardHand(new Mortal(2, "L'Archimage", 9, new Archmage()));

        board.processEvent(new PlayerPlayCard(0, 4));
        assertEquals(StatusId.CHOICE, board.getStatusBoard().getStateId());

        board.processEvent(new ChoiceMade(0, 0));
        assertEquals(StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());

        board.getStatusBoard().setCurrentPlayerId(0);
        board.processEvent(new PlayerPlayCard(0, 4));
        assertEquals(StatusId.CHOICE, board.getStatusBoard().getStateId());

        board.processEvent(new ChoiceMade(0, 0));
        assertEquals(StatusId.CHOICE, board.getStatusBoard().getStateId());

        board.processEvent(new ChoiceMade(1, 0));
        assertEquals(StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
    }

    @Test
    public void BrassPowerAffectPlayerWithStrongestFlightAndOptionAreGoldAndStrongerGoodInOpponentHand(){
        for(int i = 0; i < 4; i++) {
            resetBoard();
            board.getPlayers().get(1).addCardHand( new Metallic(10, "card", 3, null));
            for (int j = 0; j < i; j++) {
                board.getPlayers().get(1).addCardHand( new Metallic(j, "card", 6, null));
            }

            board.getPlayers().get(1).addCardFlight( new Card("card", 8));

            board.getPlayers().get(0).addCardHand( new Metallic(11, "Dragon d'Airain", 4, new Brass(), Metal.BRASS));
            board.getPlayers().get(0).addCardHand( new Metallic(11, "Dragon d'Airain", 4, new Brass(), Metal.BRASS));
            board.getPlayers().get(1).addCardHand( new Metallic(11, "Dragon d'Airain", 4, new Brass(), Metal.BRASS));

            board.processEvent( new PlayerPlayCard(0, 2));
            assertEquals(StatusId.CHOICE, board.getStatusBoard().getStateId());

            board.processEvent( new ChoiceMade(1, i));

            if( i == 0) {
                assertEquals(55, board.getPlayers().get(0).getCoin());
                assertEquals(45, board.getPlayers().get(1).getCoin());
            }
            else {
                assertEquals( i - 1, board.getPlayers().get(0).getHand().get(3).getIdCard());
            }
            assertEquals( StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
        }
    }

    @Test
    public void RedPowerSteal1CoinAnd1CardFromHandOfPlayerWithStrongestFlight(){
        for(Player p : board.getPlayers()){
            p.addCardHand( new Metallic(100,"card", 5,null));
        }

        board.getPlayers().get(1).addCardFlight( new Card("card",8));
        board.getPlayers().get(0).addCardHand( new Colored(0, "Dragon Rouge", 5, new Red(), Color.RED));

        board.processEvent( new PlayerPlayCard(0, 3));
        assertEquals( StatusId.WAITING_PLAY, board.getStatusBoard().getStateId());
        assertEquals( 51, board.getPlayers().get(0).getCoin());
        assertEquals( 49, board.getPlayers().get(1).getCoin());
        assertEquals(100, board.getPlayers().get(0).getHand().get(3).getIdCard());
        assertEquals(4, board.getPlayers().get(0).getSizeHand());
        assertEquals(2, board.getPlayers().get(1).getSizeHand());

    }
}

