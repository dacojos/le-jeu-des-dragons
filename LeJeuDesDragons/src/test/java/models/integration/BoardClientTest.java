package models.integration;

import controllers.Controller;
import game.Game;
import models.card.Card;
import models.event.AssignId;
import models.event.QuitGame;
import models.event.UpdateBoardClient;
import models.event.choice.ChoiceOption;
import models.player.PlayerClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import models.board.BoardClient;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BoardClientTest {
    static public BoardClient boardClient;

    @BeforeEach
    public void createBoardClient(){
        boardClient = new BoardClient();
    }

    @Test
    public void processEventAssignIdShouldSetPersonalId() {
        assertEquals(boardClient.getPersonalId(), -1);
        boardClient.processEvent(new AssignId(1));
        assertEquals(boardClient.getPersonalId(), 1);
    }

    @Test
    public void processEventUpdateBoardClientShouldSetBoardClient(){
        boardClient.getPlayers().add(new PlayerClient( 50,  "player0",  0,  7));
        boardClient.getPlayers().add(new PlayerClient( 50,  "player1",  1,  3));
        boardClient.getPlayers().add(new PlayerClient( 50,  "player2",  2,  2));

        ArrayList<Card> flight0 = new ArrayList<>();
        flight0.add(Game.cardGameList.get(12));
        flight0.add(Game.cardGameList.get(20));
        ArrayList<Card> flight1 = new ArrayList<>();
        flight1.add(Game.cardGameList.get(34));
        flight1.add(Game.cardGameList.get(35));
        ArrayList<Card> flight2 = new ArrayList<>();
        flight2.add(Game.cardGameList.get(45));
        flight2.add(Game.cardGameList.get(60));

        ArrayList<PlayerClient> playersClient = new ArrayList<PlayerClient>();
        playersClient.add(new PlayerClient( 40,  "player0",  0,  7,  flight0));
        playersClient.add(new PlayerClient( 40,  "player1",  1,  3,  flight1));
        playersClient.add(new PlayerClient( 40,  "player2",  2,  4,  flight2));

        UpdateBoardClient updateBoard = new UpdateBoardClient(
                "msg",
                70,
                30,
                0,
                0,
                null,
                null,
                null,
                playersClient
        );
        boardClient.processEvent(updateBoard);

        assertEquals(boardClient.getNbdrCardDeck(), updateBoard.getNbrCardDeck());
        assertEquals(boardClient.getTotalBet(), updateBoard.getTotalBet());
        assertEquals(boardClient.getCurrentPlayerId(), updateBoard.getCurrentPlayerId());

        if(updateBoard.getAnte() != null) {
            for (int i = 0; i < updateBoard.getAnte().size(); ++i) {
                assertEquals(boardClient.getAnte().get(i).getIdCard(), updateBoard.getAnte().get(i));
            }
        }

        if(updateBoard.getDiscard() != null) {
            for (int i = 0; i < updateBoard.getDiscard().size(); ++i) {
                assertEquals(boardClient.getDiscard().get(i).getIdCard(), updateBoard.getDiscard().get(i));
            }
        }

        if(updateBoard.getClientHand() != null) {
            for (int i = 0; i < updateBoard.getClientHand().size(); ++i) {
                assertEquals(boardClient.getClientHand().get(i).getIdCard(), updateBoard.getClientHand().get(i));
            }
        }

        PlayerClient player= null;
        PlayerClient updatePlayer= null;

        for(int i = 0; i < updateBoard.getPlayers().size(); i++){
            player = boardClient.getPlayers().get(i);
            updatePlayer = updateBoard.getPlayers().get(i);

            assertEquals( player.getCoin(), updatePlayer.getCoin());
            assertEquals( player.getHandSize(), updatePlayer.getHandSize());
            assertEquals( player.getId(), updatePlayer.getId());
            for(int j = 0; j < updatePlayer.getFlight().size(); j++){
                assertEquals( player.getFlight().get(j), updatePlayer.getFlight().get(j));
            }
            assertEquals( player.getName(), updatePlayer.getName());
        }
    }

    @Test
    public void processEventQuitGameShouldOpenMenuIfTrue(){
        boardClient.setui(new Controller());
        boardClient.getui().setApp(new Game());
        //todo comment je fais ca moi
        //boardClient.processEvent(new QuitGame(true));

    }

    @Test
    public void processEventQuitGameShouldNotOpenMenuIfFalse(){
        //boardClient.processEvent(new QuitGame(false));
        //todo comment je fais ca moi
    }


    @Test
    public void processEventChoiceOptionShouldOpenWindowWithListIfPersonalIdEqualTarget(){
        boardClient.setPersonalId(0);
        ArrayList<String> options = new ArrayList<>();
        options.add("la pilule bleu");
        options.add("la pilule rouge");
        //boardClient.processEvent(new ChoiceOption(0,options));
        //todo comment je fais ca moi
    }

    @Test
    public void processEventChoiceOptionShouldNotOpenWindowWithListIfPersonalIdNotEqualTarget(){
        boardClient.setPersonalId(0);
        //boardClient.processEvent(new ChoiceOption(1, null));
        //todo comment je fais ca moi
    }

}
