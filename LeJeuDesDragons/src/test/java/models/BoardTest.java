package models;

import models.board.Board;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BoardTest {
    public static Board board;

    @BeforeAll
    public static void CreateBoard(){
        board = new Board();
    }

    @Test
    public void BoardCreateTest(){
        assertEquals(0, board.getTotalBet());
        board.setTotalBet(50);
        assertEquals(50, board.getTotalBet());
    }

}
