package communication.server;

public interface ServerInterface {
    void start();

    void updateBoardClient(int idClient, String json);

    void assignId(int idClient, String json);

    void choiceOption(int idClient, String json);

    void quitGame(String json);

    void close(int exitCode);

    String getPublicIpAddress();

    void diffusePrivateIp();
}
