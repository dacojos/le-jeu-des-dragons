package communication.server;

import communication.protocol.DDProtocol;
import models.board.Board;
import models.jsonHandler.JsonCreator;
import models.jsonHandler.JsonReader;

import java.io.*;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Thread that handle a client and get its commands
 * and forward them to the board, which will call
 * the processEvent method with the parsed json
 * given in payload, after the command
 *
 * @author Simon Jobin
 */
public class ClientHandler implements Runnable {

    final static Logger LOG = Logger.getLogger(ClientHandler.class.getName());

    private ReceptionistWorker rw;

    private Socket clientSocket;
    private BufferedReader in = null;
    private PrintWriter out = null;

    private boolean shouldServe = true;

    /* Increment value each time a client has connected */
    private static int nbClient = 0;
    private int clientId;

    private Board board;
    private JsonCreator jsonCreator;
    private JsonReader jsonReader;

    private boolean debug = false;
    private boolean reconnected = false;

    public ClientHandler(ReceptionistWorker rw, Socket clientSocket, Board board) {

        try {
            this.rw = rw;
            this.clientSocket = clientSocket;
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            out = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
            this.board = board;
            jsonCreator = new JsonCreator();
            jsonReader = new JsonReader();
            clientId = nbClient++;
        } catch (IOException e) {
            logError(e.getMessage());
        }
    }

    public void run() {

        while(shouldServe) {
            serve();
        }
    }

    private void serve() {

        String args[] = {};
        String command;

        try {

            String headers = "";

            headers = in.readLine();
            if(headers != null)
                args = headers.split(DDProtocol.ARGS_DELIMITER);

            if(args.length > 0) {

                command = args[0];

                /*********************************COMMAND JOIN************************************************/
                if(command.equals(DDProtocol.JOIN)) {

                    String joinGameJson = getJson(in);
                    synchronized (board) {
                        if(board.processEvent(jsonReader.parseJoinGame(joinGameJson))) {
                            send(DDProtocol.STATUS_JOINED + " you joined the game");
                        } else {
                            send(DDProtocol.STATUS_CANT_JOIN + " server full, or game already started");
                        }
                    }
                /*********************************COMMAND START GAME******************************************/
                } else if(command.equals(DDProtocol.START)) {

                    String startGameJson = getJson(in);
                    synchronized (board) {
                        if(board.processEvent(jsonReader.parseStartGame(startGameJson))) {
                            send(DDProtocol.STATUS_GAME_STARTED + " have fun");
                        } else {
                            send(DDProtocol.STATUS_ALREADY_STARTED + " cannot start");
                        }
                    }
                /*********************************COMMAND PLAY************************************************/
                } else if(command.equals(DDProtocol.PLAY)) {

                    String playCardJson = getJson(in);
                    synchronized (board) {
                        if(board.processEvent(jsonReader.parsePlayerPlayCard(playCardJson))) {
                            send(DDProtocol.STATUS_CARD_PLAYED + " card played");
                        } else {
                            send(DDProtocol.STATUS_CANT_PLAY + " cannot play this card");
                        }
                    }
                /**********************************COMMAND CHOOSE END GAME************************************/
                } else if(command.equals(DDProtocol.CHOOSE_ENDGAME)) {

                    String restartJson = getJson(in);
                    synchronized (board) {
                        if (board.processEvent(jsonReader.parseChooseEndGame(restartJson))) {
                            send(DDProtocol.STATUS_VOTE_ENDGAME + " vote end game");
                        } else {
                            send(DDProtocol.STATUS_CANT_VOTE_ENDGAME + " cannot vote");
                        }
                    }
                /**********************************COMMAND OPTION CHOICE**************************************/
                } else if(command.equals(DDProtocol.CHOICE_MADE)) {
                    String choiceMade = getJson(in);

                    synchronized (board) {
                        if (board.processEvent(jsonReader.parseChoiceMade(choiceMade))) {
                            send(DDProtocol.STATUS_CHOICE_VALID + " choice made");
                        } else {
                            send(DDProtocol.STATUS_CHOICE_NOT_VALID + " cannot vote");
                        }
                    }

                /***********************************COMMAND SAY TO QUIT***************************************/
                } else if (command.equals(DDProtocol.SAY_TO_QUIT)){
                    String sendToQuit = getJson(in);

                    synchronized (board) {
                        if (board.processEvent(jsonReader.parseSendToQuit(sendToQuit))) {
                            send(DDProtocol.STATUS_QUIT_GAME + " quit game");
                        }
                    }
                /*********************************COMMAND QUIT************************************************/
                } else if(command.equals(DDProtocol.QUIT)) {
                    send(DDProtocol.STATUS_OK + " close connection. Goodbye !");
                    nbClient--;
                    close();
                } else {
                    send(DDProtocol.STATUS_BAD_REQUEST + " unrecognized command " + command);
                }
            } else {
                send(DDProtocol.STATUS_BAD_REQUEST + " empty request");
            }
        } catch (IOException e) {

            // connection lost, has to notify the receptionist
            this.rw.handleDisconnection(clientSocket, clientId);
            close();
        }
    }

    public void send(String str) {
        out.println(str);
        out.flush();
    }

    public void logInfo(String message) {
        LOG.log(Level.INFO, message);
    }

    public void logError(String message) {
        LOG.log(Level.SEVERE, message);
    }

    public void close() {

        logInfo("cleaning up ressources...");

        try {
            if (in != null) {
                in.close();
            }

            if (out != null) {
                out.close();
            }

            if (clientSocket != null) {
                clientSocket.close();
            }
            shouldServe = false;
        } catch(Exception e) {
            logError(e.getMessage());
        }

        // reset cause it is a static var
        nbClient = 0;
    }

    /**
     * get the json representing the body of the
     * client request
     * @return the json to be given to board
     */
    public String getJson(BufferedReader br) {
        String json = "";
        String line = "";
        try {
            while (!(line = br.readLine()).isEmpty()) {
                json += line;
            }

            if(debug)
                logInfo("parsed json : " + json);

        } catch(Exception e) {
            send(DDProtocol.STATUS_MALFORMED_JSON + " can't read json");
        }
        return json;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public void setReconnected(boolean reconnected) {
        this.reconnected = reconnected;
    }

    public Socket getClientSocket() {
        return clientSocket;
    }
}
