package communication.server;

import communication.protocol.DDProtocol;
import models.board.Board;
import models.statusBoard.StatusId;

import java.io.IOException;
import java.net.*;
import java.util.Collections;
import java.util.Enumeration;

/**
 * Main class for the server, which launch a runnable
 * receptionist
 */
public class DDServer implements ServerInterface {

    private ReceptionistWorker receptionistWorker;

    public DDServer(Board board) {
        this.receptionistWorker = new ReceptionistWorker(board, this);
        this.board = board;
    }

    public void start() {
        new Thread(this.receptionistWorker).start();
    }

    public void close(int exitCode) {
        this.receptionistWorker.close(exitCode);
    }

    /* Communication methods */

    /**
     * notify specific client to update his board state
     *
     * @param clientId          the client id
     * @param updateBoardClient json representing the event
     */
    public void updateBoardClient(int clientId, String updateBoardClient) {
        receptionistWorker.updateBoardClient(clientId, updateBoardClient);
    }

    /**
     * gives to a client that joined the game an id
     *
     * @param clientId the id given
     * @param assignId json representing the event
     */
    public void assignId(int clientId, String assignId) {
        receptionistWorker.assignId(clientId, assignId);
    }

    /**
     * notify the client to quit the game
     *
     * @param quitGame json representing the event
     */
    public void quitGame(String quitGame) {
        receptionistWorker.quitGame(quitGame);
    }

    /**
     * notify the client to quit the game
     *
     * @param choiceOption json representing the event
     */
    public void choiceOption(int clientId, String choiceOption) {
        receptionistWorker.choiceOption(clientId, choiceOption);
    }

    /**
     * function to quit the game, if a client
     * lost connection for example
     */
    public void endGame(int idClient) {
        board.endGame( idClient);
    }

    /**
     * Test function
     */
    public void broadcast(String message) {
        receptionistWorker.sendToAll(message);
    }

    /**
     * Method needed because it depends on OS
     * @return the private local ip address
     */
    public String getPublicIpAddress() {

        String ret = "";

        Enumeration<NetworkInterface> nets = null;
        try {
            nets = NetworkInterface.getNetworkInterfaces();
            for (NetworkInterface netint : Collections.list(nets)) {

                Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
                for (InetAddress inetAddress : Collections.list(inetAddresses)) {

                    if (inetAddress.toString().startsWith("/10.192") ||
                            // Skip virtual box default gateway
                            ((inetAddress.toString().startsWith("/192.168")) && (inetAddress.toString().equals("192.168.99.1")))) {
                        ret = inetAddress.toString();
                    }
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }

        try {
            if (ret == null) // on OSX getPublicIpAddress() can be null, this is a fallback
                ret = InetAddress.getLocalHost().getHostAddress();
            else
                return ret;
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void sendMulticastMessage(String message, String ipAddress, int port) {

        DatagramSocket socket = null;

        try {

            socket = new DatagramSocket();
            InetAddress group = InetAddress.getByName(ipAddress);

            byte[] msg = message.getBytes();
            DatagramPacket packet = new DatagramPacket(msg, msg.length, group, port);
            socket.send(packet);
            socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void diffusePrivateIp() {

        new Thread(() -> {

            while(this.board.getStatusBoard().getStateId().getId() == StatusId.WAITING_PLAYER.getId()) {

                sendMulticastMessage(this.getPublicIpAddress(),
                        DDProtocol.MULTICAST_ADDRESS, DDProtocol.MULTICAST_PORT);

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }).start();
    }

    /* Needs a reference to the Board */
    private Board board;
}
