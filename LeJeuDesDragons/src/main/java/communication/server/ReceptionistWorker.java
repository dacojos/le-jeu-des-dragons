package communication.server;

import communication.protocol.DDProtocol;
import models.board.Board;
import models.jsonHandler.JsonCreator;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @brief Class that manage client incoming connections and
 * delegate job to a "ClientHandler" which works on
 * its own thread
 *
 * @author Simon Jobin
 */
public class ReceptionistWorker implements Runnable {

    final static Logger LOG = Logger.getLogger(ReceptionistWorker.class.getName());

    private DDServer ddServer;
    private ServerSocket serverSocket;
    private boolean shouldRun = true;
    private int serverPort = DDProtocol.SERVER_PORT;

    // to manage clients (access them by getting their id)
    private ArrayList<ClientHandler> clientHandlers;

    // map of lost socket, with their client id
    private Map<Socket, Integer> lostClients;

    public ReceptionistWorker(Board board, DDServer ddServer) {
        this.ddServer = ddServer;
        clientHandlers = new ArrayList<ClientHandler>();
        lostClients = new HashMap<Socket, Integer>();
        this.board = board;
    }

    public void run() {

        try {
            serverSocket = new ServerSocket(DDProtocol.SERVER_PORT);
        } catch (IOException e) {
            logError(e.getMessage());
            return;
        }

        while (shouldRun) {

            LOG.log(Level.INFO, "Waiting for incoming client connection on port {0}", serverPort);
            try {

                Socket clientSocket = serverSocket.accept();
                ClientHandler newHandler = new ClientHandler(this, clientSocket, board);

                // check if it is a new client, or one trying to reconnect
                int clientId = -1;
                for(Map.Entry<Socket, Integer> entry : lostClients.entrySet()) {
                    // compare addresses
                    if(entry.getKey().getInetAddress().equals(clientSocket.getInetAddress())) {
                        clientId = entry.getValue();

                        // remove reco player from deco map
                        lostClients.remove(entry.getKey());
                        break;
                    }
                }

                // if reconnected client
                if(clientId > -1) {

                    LOG.info("Player " + clientId + " reconnected");
                    // notify all other players
                    sendToAll(DDProtocol.PLAYER_RECONNECTED + " Player " + clientId + " reconnected");

                    // "re-use" the index of clientsHandler ArrayList
                    newHandler.setClientId(clientId);
                    newHandler.setReconnected(true);

                    clientHandlers.set(clientId, newHandler);
                    new Thread(newHandler).start();

                    // notify the client himself, so he know he can rejoin and launch board
                    newHandler.send(DDProtocol.YOU_CAN_REJOIN);

                    assignId(clientId, new JsonCreator().assignId(clientId));
                    updateBoardClient(clientId, new JsonCreator().updateBoardClient("msg", board, clientId));

                } else {

                    LOG.info("A new client has arrived. Starting a new thread and delegating work to a new handler...");
                    // Must handle new client and add it to the list
                    clientHandlers.add(newHandler);
                    new Thread(newHandler).start();
                }

            } catch (IOException e) {
                logError(e.getMessage());
                close(DDProtocol.EXIT_FAILURE);
            }
        }
    }

    public void logInfo(String message) {
        LOG.log(Level.INFO, message);
    }

    public void logError(String message) {
        LOG.log(Level.SEVERE, message);
    }

    /**
     * notify clients to quit and clean
     * all ressources (socket, in and out
     * streams,..)
     * @param exitCode the exit status
     */
    public void close(int exitCode) {

        // If the server is closed, it has to notify all
        // players that the game is finished
        if(exitCode != 0)
            quitGame(new JsonCreator().quitGame(true, true));
        
        String message = "server closed with exit code " + exitCode;

        if(exitCode == DDProtocol.EXIT_SUCCESS) logInfo(message); else logError(message);

        for(ClientHandler c : clientHandlers) {
            c.close();
        }
        shouldRun = false;
        if(serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * broadcast a message to all players
     * @param str the broadcast message
     */
    public void sendToAll(String str) {
        for(ClientHandler c : clientHandlers) {
            c.send(str);
        }
    }

    /**
     * send a message to a specific player
     * @param playerId id of the player
     * @param str sent message
     */
    public void sendToPlayer(int playerId, String str) {
        try {
            for(ClientHandler c : clientHandlers) {
                // look for the right client
                if(c.getClientId() == playerId) {
                    c.send(str);
                    break;
                }
            }
        } catch (Exception ex) {
            logError("Player " + playerId + " doesn't exist");
        }
    }

    public void updateBoardClient(int clientId, String updateBoardClient) {
        String message = DDProtocol.UPDATE + DDProtocol.CRLF + updateBoardClient + DDProtocol.CRLF;
        sendToPlayer(clientId, message);
    }

    public void assignId(int clientId, String assignId) {
        String message = DDProtocol.ASSIGN_ID + DDProtocol.CRLF + assignId + DDProtocol.CRLF;
        sendToPlayer(clientId, message);
    }

    public void quitGame(String quitGame){
        String message = DDProtocol.SAY_TO_QUIT + DDProtocol.CRLF + quitGame + DDProtocol.CRLF;
        sendToAll(message);
    }

    public void choiceOption(int clientId, String choiceOption){
        String message = DDProtocol.CHOICE_OPTION + DDProtocol.CRLF + choiceOption + DDProtocol.CRLF;
        sendToPlayer(clientId, message);
    }

    public void handleDisconnection(Socket clientSocket, int clientId) {
        // notify all other players
        sendToAll(DDProtocol.PLAYER_LEFT + " Player " + clientId + " left");
        logInfo("Player " + clientId + " disconnected, attempting to recnnect...");
        
        lostClients.put(clientSocket, clientId);

        try {
            // the calling thread doesn't handle connection anymore
            // we can sleep without problems
            Thread.sleep(1000 * DDProtocol.MAX_TIME_DISCONNECTION);

            // if not reconnected, end game
            if(lostClients.containsKey(clientSocket)) {
                logInfo("client " + clientId + " not reconnected after " + DDProtocol.MAX_TIME_DISCONNECTION
                + " seconds, end game...");

                // notify all other players
                sendToAll(DDProtocol.PLAYER_LOST + " Player " + clientId + " lost :(");

                this.ddServer.endGame(clientId);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // Needs a reference to the Board
    private Board board;
}



