package communication.client;

import communication.protocol.DDProtocol;
import models.board.BoardClient;

import java.io.*;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @brief Class that just send message but nor read from response
 * or anything coming from server. Typical use of actions such as
 * 'JoinGame', 'PlayerPlayCard' or 'StartGame'
 *
 * @author Simon Jobin
 */
public class SenderWorker implements Runnable {

    final static Logger LOG = Logger.getLogger(SenderWorker.class.getName());

    private DDClient ddClient;
    private ListenerWorker listenerWorker;
    private Socket clientSocket;
    private PrintWriter out = null;

    private boolean debug = true;

    public SenderWorker(String ipAddress, BoardClient boardClient, DDClient ddClient) {

        this.boardClient = boardClient;
        this.ddClient = ddClient;

        try {
            // connect to the server
            clientSocket = new Socket(ipAddress, DDProtocol.SERVER_PORT);
            out = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
        } catch (IOException e) {
            logError("failed to connect to the server");
            close();
        }
    }

    /**
     * notify server that a player want to join
     * @param joinGame the json event to be parsed
     * @return true/false depending on board action
     */
    public boolean joinGame(String joinGame) {
        String message = DDProtocol.JOIN + DDProtocol.CRLF + joinGame + DDProtocol.CRLF;
        return send(message);
    }

    /**
     * notify server that a player want to start
     * @param startGame the json event to be parsed
     * @return true/false depending on board action
     */
    public boolean startGame(String startGame) {
        String message = DDProtocol.START + DDProtocol.CRLF + startGame + DDProtocol.CRLF;
        return send(message);
    }

    /**
     * notify server that a player played a card
     * @param playerPlayCard the json event to be parsed
     * @return true/false depending on board action
     */
    public boolean playerPlayCard(String playerPlayCard) {
        String message = DDProtocol.PLAY + DDProtocol.CRLF + playerPlayCard + DDProtocol.CRLF;
        return send(message);
    }

    /**
     * notify server that a player want to end game
     * @param chooseEndGame the json event to be parsed
     * @return tre/false depending on board action
     */
    public boolean chooseEndGame(String chooseEndGame) {
        String message = DDProtocol.CHOOSE_ENDGAME + DDProtocol.CRLF + chooseEndGame + DDProtocol.CRLF;
        return send(message);
    }

    /**
     * notify server that a player made a choice
     * @param choiceMade the json event to be parsed
     * @return tre/false depending on board action
     */
    public boolean choiceMade(String choiceMade){
        String message = DDProtocol.CHOICE_MADE + DDProtocol.CRLF + choiceMade + DDProtocol.CRLF;
        return send(message);
    }

    /**
     * notify server that a player quit the game
     * @param sendToQuit the json event to be parsed
     * @return true/false depending on board action
     */
    public boolean sendToQuit(String sendToQuit){
        String message = DDProtocol.SAY_TO_QUIT + DDProtocol.CRLF + sendToQuit + DDProtocol.CRLF;
        return send(message);
    }

    @Override
    public void run() {
        new Thread(this.listenerWorker = new ListenerWorker(boardClient, clientSocket, this, ddClient)).start();
    }

    /**
     * send message to the server and wait that
     * the listener notify him when he get a status
     * response from the server
     * @param str the sent message
     * @return true or false depending on status code
     */
    public boolean send(String str) {
        synchronized (this) {
            out.println(str);
            out.flush();
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if(listenerWorker.getServerResponse().startsWith("4"))
            return false;

        return true;
    }

    /**
     * logging information
     * @param message information message
     */
    public void logInfo(String message) {
        LOG.log(Level.INFO, message);
    }

    /**
     * logging error
     * @param message error message
     */
    public void logError(String message) {
        LOG.log(Level.SEVERE, message);
    }

    /**
     * clean resources, close
     * output stream and socket
     */
    public void close() {

        logInfo("cleaning up ressources...");

        /* close out */
        if(out != null) {
            out.close();
        }

        /* close in */
        this.listenerWorker.close();

        /* finally close socket */
        if(clientSocket != null) {
            try {
                clientSocket.close();
            } catch (IOException e) {
                logError(e.getMessage());
            }
        }
    }

    private BoardClient boardClient;
}
