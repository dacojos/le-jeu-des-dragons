package communication.client;

import communication.protocol.DDProtocol;
import models.board.BoardClient;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

/**
 * @brief Main class for the client.
 * @author Simon Jobin
 */
public class DDClient implements ClientInterface{

    // needs a reference to a BoardClient to call
    // its processEvent methods when parsing server
    // responses
    private final BoardClient boardClient;
    private SenderWorker senderWorker;

    private boolean reconnected = false;

    public DDClient(String ipAddress, BoardClient boardClient) {
        this.boardClient = boardClient;
        this.senderWorker = new SenderWorker(ipAddress, boardClient, this);
    }

    /**
     * start the sender thread, that will
     * start the listener thread (the socket is
     * shared between them)
     */
    public void start() {
        new Thread(this.senderWorker).start();
    }

    /**
     * clean resources
     */
    public void close() {
        senderWorker.close();
    }

    /**
     * function used when a client wants to
     * join game
     * @param joinGame json representing the event
     */
    public boolean joinGame(String joinGame) {
        return senderWorker.joinGame(joinGame);
    }

    /**
     * function used by a user to start the game
     * @param startGame json representing the event
     */
    public boolean startGame(String startGame) {
        return senderWorker.startGame(startGame);
    }

    /**
     * notify server that client want to play a card
     * @param playerPlayCard json representing the event
     */
    public void playerPlayCard(String playerPlayCard) {
        senderWorker.playerPlayCard(playerPlayCard);
    }

    /**
     * notify server that a client vote at the end of the game
     * @param chooseEndGame json representing the event
     */
    public void chooseEndGame(String chooseEndGame){ senderWorker.chooseEndGame(chooseEndGame);}

    /**
     * notify server that a client has made a choice
     * @param choiceMade json representing the event
     */
    public void choiceMade(String choiceMade){ senderWorker.choiceMade(choiceMade);}

    /**
     * notify server that a client has made a choice
     * @param sendToQuit json representing the event
     */
    public void sendToQuit(String sendToQuit){
        senderWorker.sendToQuit(sendToQuit);
        close();
    }

    /**
     * function called by receptionist when a player
     * reconnect, in order he can be recognized as
     * left player and go back in game
     * @param reconnected true if reconnected
     */
    public void setReconnected (boolean reconnected) {
        this.reconnected = reconnected;
    }

    /**
     * @return true if reconnected
     */
    public boolean getReconnected() {
        return reconnected;
    }

    public static String receiveUDPMessage(String ipAdress, int port, byte[] buffer)  {

        try {

            MulticastSocket socket = new MulticastSocket(port);

            InetAddress group = InetAddress.getByName(ipAdress);
            socket.joinGroup(group);
            String msg = "";

            while (true) {

                DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                socket.receive(packet);
                msg = new String(packet.getData(), packet.getOffset(), packet.getLength());

                break;
            }

            socket.leaveGroup(group);
            socket.close();
            return msg.substring(1);

        } catch(IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
