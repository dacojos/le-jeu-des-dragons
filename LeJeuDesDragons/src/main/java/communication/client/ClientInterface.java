package communication.client;

public interface ClientInterface {
    void start();

    boolean joinGame(String json);

    boolean startGame(String json);

    void playerPlayCard(String json);

    void chooseEndGame(String json);

    void choiceMade(String json);

    void setReconnected(boolean reconnected);

    void sendToQuit(String json);

    boolean getReconnected();

    void close();
}
