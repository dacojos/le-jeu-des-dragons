package communication.client;

import communication.protocol.DDProtocol;
import models.board.BoardClient;
import models.jsonHandler.JsonCreator;
import models.jsonHandler.JsonReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @brief This class implements a server listener that will just listen to
 * server responses but will not send anything to him. It manages
 * incoming events such as 'UpdateBoardClient' that doesn't need that
 * client send any request before. It is also responsible to manage status
 * answer from the server following different message sent by client
 *
 * @author Simon Jobin
 */
public class ListenerWorker implements  Runnable {

    final static Logger LOG = Logger.getLogger(ListenerWorker.class.getName());

    private BufferedReader in;
    private SenderWorker sw;
    private DDClient ddClient;

    private boolean shouldRun = true;
    private JsonCreator jsonCreator;
    private JsonReader jsonReader;

    private boolean debug = false;

    private String serverResponse;

    public ListenerWorker(BoardClient boardClient, Socket clientSocket, SenderWorker sw, DDClient ddClient) {

        this.boardClient = boardClient;
        this.sw = sw;
        this.ddClient = ddClient;
        try {
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.jsonCreator = new JsonCreator();
        this.jsonReader = new JsonReader();
    }

    @Override
    public void run() {

        while(shouldRun) {

            try {
                String line = "";
                synchronized (in) {
                    while ((line = in.readLine()) != null) {

                        String[] args = line.split(DDProtocol.ARGS_DELIMITER);
                        String serverCommand = args[0];

                        /*************************************SERVER COMMANDS**********************************/
                        if (serverCommand.equals(DDProtocol.ASSIGN_ID)) {
                            boardClient.processEvent(jsonReader.parseAssignId(getJson(in)));
                        } else if (serverCommand.equals(DDProtocol.UPDATE)) {
                            boardClient.processEvent(jsonReader.parseUpdateBoardClient(getJson(in)));
                        } else if(serverCommand.equals(DDProtocol.SAY_TO_QUIT)) {
                            boardClient.processEvent(jsonReader.parseQuitGame(getJson(in)));
                        } else if(serverCommand.equals(DDProtocol.CHOICE_OPTION)) {
                            boardClient.processEvent(jsonReader.parseChoiceOption(getJson(in)));
                        } else if(serverCommand.equals(DDProtocol.YOU_CAN_REJOIN)) {
                            this.ddClient.setReconnected(true);
                        /*********************************SERVER RESPONSES (STATUS)*****************************/
                        } else {
                            manageMessage(line);
                        }
                    }
                }
            } catch(IOException e) {
                close();
            }
        }
    }

    /**
     * logging an error
     * @param message error message
     */
    public void logInfo(String message) {
        LOG.log(Level.INFO, message);
    }

    /**
     * logging an information
     * @param message information message
     */
    public void logError(String message) {
        LOG.log(Level.SEVERE, message);
    }

    /**
     * manage server response and notify the sender
     * so that he can return true or false depending
     * on server status answer after he sent something
     * @param message
     */
    public void manageMessage(String message) {
        boardClient.handleServerMessage(message.substring(3));
        synchronized (this.sw) {
            if(message != "") {
                if (message.startsWith("4")) {
                    logError(message);
                } else {
                    logInfo(message);
                }
                this.serverResponse = message;
                sw.notify();
            }
        }
    }

    /**
     * close the input stream
     */
    public void close() {
        logInfo("cleaning up ressources...");

        if(in != null) {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        shouldRun = false;
    }

    /**
     * get the json representing the body of the
     * server message
     * @return the json to be given to boardClient
     */
    public String getJson(BufferedReader br) {
        String json = "";
        String line = "";
        try {
            while (!(line = br.readLine()).isEmpty()) {
                json += line + DDProtocol.CRLF;
            }

            if(debug)
                logInfo("parsed json : " + json);

        } catch(Exception e) {
            logInfo("can't read json");
        }
        return json;
    }

    /**
     * function called by sender through synchronization
     * mechanism after he sent a message to the server
     * @return
     */
    public String getServerResponse() {
        return serverResponse;
    }

    // Needs a reference to a client board
    private BoardClient boardClient;
}
