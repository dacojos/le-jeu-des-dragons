package communication.protocol;

/**
 * Set of status and commands for the DD game
 */
public class DDProtocol {

    /* Relative to communication */
    public static final int SERVER_PORT = 5000;
    public static final String CRLF = "\r\n";
    public static final String ARGS_DELIMITER = " ";
    public static final int EXIT_SUCCESS = 0;
    public static final int EXIT_FAILURE = -1;
    public static final int MAX_TIME_DISCONNECTION = 120;
    public static final String MULTICAST_ADDRESS = "230.0.0.0";
    public static final int MULTICAST_PORT = 5555;

    /* Commands sent by clients */
    public static final String JOIN = "JOIN";
    public static final String PLAY = "PLAY";
    public static final String QUIT = "QUIT";
    public static final String START = "START";
    public static final String CHOICE_MADE = "CHOICE_MADE";

    public static final String CHOOSE_ENDGAME = "CHOOSE_ENDGAME";
    public static final String SAY_TO_QUIT = "SAY_TO_QUIT";


    /* Commands sent by server */
    public static final String UPDATE = "UPDATE";
    public static final String ASSIGN_ID = "ASSIGN_ID";
    public static final String CHOICE_OPTION = "CHOICE_OPTION";

    /* Status codes from server */
    public static final String STATUS_OK = "200";
    public static final String STATUS_JOINED = "201";
    public static final String STATUS_GAME_STARTED = "202";
    public static final String STATUS_CARD_PLAYED = "203";
    public static final String STATUS_VOTE_ENDGAME = "204";
    public static final String STATUS_CHOICE_NOT_VALID = "205";
    public static final String STATUS_CHOICE_VALID = "206";
    public static final String STATUS_QUIT_GAME = "207";
    public static final String STATUS_BAD_REQUEST = "400";
    public static final String STATUS_NOT_IN_GAME = "401";
    public static final String STATUS_MALFORMED_JSON = "402";
    public static final String STATUS_GAME_NOT_STARTED = "403";
    public static final String STATUS_CANT_JOIN = "404";
    public static final String STATUS_ALREADY_STARTED = "405";
    public static final String STATUS_CANT_PLAY = "406";
    public static final String STATUS_CANT_VOTE_ENDGAME = "407";
    public static final String STATUS_SERVER_FULL = "500";
    public static final String PLAYER_LEFT = "501";
    public static final String PLAYER_RECONNECTED = "502";
    public static final String YOU_CAN_REJOIN = "503";
    public static final String PLAYER_LOST = "504";

}
