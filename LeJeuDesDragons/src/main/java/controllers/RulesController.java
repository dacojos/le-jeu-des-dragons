package controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.stage.Stage;
import java.net.URL;
import java.util.ResourceBundle;

public class RulesController implements Initializable {

    // main pane size
    private static int HEIGHT = 533;
    private static int WIDTH = 800;
    private static int HORIZONTAL_SPACING = 130;
    private static int VERTICAL_SPACING = 90;
    private static int OFFSET_Y = 38;

    @FXML
    private Pane mainPane;

    private Controller mainController;
    private Stage currentEnlargedCard;

    public void setMainController(Controller mainController) {
        this.mainController = mainController;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        String descriptions[] = new String[]{"Metal Dragons", "Colored Dragons", "Special Dragons", "Mortals"};

        int metalDragons[]    = new int[]{3, 9, 27, 33, 45};
        int coloredDragons[]  = new int[]{15, 21, 39, 51, 57};
        int specialDragons[]  = new int[]{1, 2, 67};
        int mortalss[]        = new int[]{0, 63, 64, 65, 66, 68, 69};

        int allCards[][]      = new int[][]{metalDragons, coloredDragons, specialDragons, mortalss};

        for(int i = 0; i < allCards.length; ++i) {
            Label description = new Label(descriptions[i]);
            description.setStyle("-fx-background-color: transparent;");
            description.setFont(Font.font("fantasy", FontPosture.ITALIC, 20));
            description.setTranslateY(5 + i * 132);
            description.setTranslateX((WIDTH - (description.getText().length() * 10)) / 2);

            mainPane.getChildren().add(description);
            addRowToMainPane(allCards[i], i);
        }

        // Add the rules on the bottom of the window
        ImageView rules = new ImageView(new Image("/img/rulesImg2.png"));
        mainPane.getChildren().add(rules);
        rules.setTranslateY(533);

        mainPane.setOnMouseClicked(event -> {
            if(currentEnlargedCard != null)
                currentEnlargedCard.close();
        });
    }

    public void addRowToMainPane(int cardIds[], int rowIndex) {

        Pane pane = new StackPane();

        for(int i : cardIds) {
            addCardToPane(i, pane);
        }

        mainPane.getChildren().add(pane);

        pane.setTranslateX((WIDTH - pane.getChildren().size() * VERTICAL_SPACING) / 2);
        pane.setTranslateY(OFFSET_Y + HORIZONTAL_SPACING * rowIndex);
    }

    public void addCardToPane(int cardId, Pane pane) {
        ImageView newCard = new ImageView("cards/" + cardId + ".png");
        Button tmp = new Button("", newCard);

        tmp.setTranslateX(VERTICAL_SPACING * pane.getChildren().size());
        tmp.setMinWidth(60);
        tmp.setMaxWidth(60);
        tmp.setMinHeight(105);
        tmp.setMaxHeight(105);
        newCard.setFitWidth(55);
        newCard.setFitHeight(100);
        tmp.setOnMouseClicked(event -> {
            if(currentEnlargedCard != null)
                currentEnlargedCard.close();
            currentEnlargedCard = mainController.enlargeCard(cardId, false);
        });

        pane.getChildren().add(tmp);
    }
}
