package controllers;

import game.Game;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import models.board.BoardClient;
import models.card.Card;
import models.player.PlayerClient;
import models.statusBoard.StatusId;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.ResourceBundle;


import static game.Game.cardGameList;

public class Controller implements Initializable, UserInterface {

    Game app;

    // relative to first player in front of window
    @FXML
    private StackPane p1hand;

    // relative to other players
    @FXML
    private Label p1coins, p2coins, p3coins, p4coins, p5coins, p6coins;

    @FXML
    private Label p2hand, p3hand, p4hand, p5hand, p6hand;

    @FXML
    private Label p1points, p2points, p3points, p4points, p5points, p6points;

    @FXML
    private StackPane p1cards, p2cards, p3cards, p4cards, p5cards, p6cards;

    @FXML
    private Label p1name, p2name, p3name, p4name, p5name, p6name;

    @FXML
    private Pane p1img, p2img, p3img, p4img, p5img, p6img;

    // other fx components
    @FXML
    private Label draw, bet, anteCard, discard, ipLabel, coinsAnimation, anteCardsNb, infoPopup, infoMsg;

    @FXML
    private ImageView lastDisc;

    @FXML
    private Button startGame, quitGame, cardsAndRules, discardBtn, anteBtn, help;

    @FXML
    private Text serverMsg;

    @FXML
    private TextField name, serverAddr;

    @FXML
    private AnchorPane centralBoard;

    private Label[] coins;
    private Label[] hands;
    private Label[] points;
    private StackPane[] cards;
    private Label[] names;
    private Pane[] imgs;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        coins = new Label[]{p1coins, p2coins, p3coins, p4coins, p5coins, p6coins};
        hands = new Label[]{null, p2hand, p3hand, p4hand, p5hand, p6hand};
        points = new Label[]{p1points, p2points, p3points, p4points, p5points, p6points};
        cards = new StackPane[]{p1cards, p2cards, p3cards, p4cards, p5cards, p6cards};
        names = new Label[]{p1name, p2name, p3name, p4name, p5name, p6name};
        imgs = new Pane[]{p1img, p2img, p3img, p4img, p5img, p6img};
    }

    // primary stage corresponds to the menu,
    // gameStage to the current stage
    private Stage primaryStage, currentEnlargedCard, gameStage = null;

    private int lastNbPlayers = 0;
    private int lastPhase = -1 ;
    private ArrayList<Stage> currentOpenedWindow = new ArrayList<>();
    private boolean gameStarted = false;

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public void setServerAddr(String message) {
        this.serverAddr.setText(message);
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    /**
     * go back to menu window and kill
     * server and client for an eventual
     * next game
     */
    public void backToMenu() {

        app.launchMenu(new Stage());

        if(app.getBoardClient() != null) {
            app.getBoardClient().getClient().close();
        }
        if(app.getBoard() != null) {
            app.getBoard().getServer().close(0);
        }

        if(gameStage != null)
            gameStage.close();
    }

    /**
     * associated to the quitGame button
     */
    @FXML
    private void quitGame() {
        // If game is started, notify server to end
        if(gameStarted) {
            app.getBoardClient().sendToQuit();
        }
    }

    /**
     * open the window containing ante cards
     * when player click on them
     */
    @FXML
    private void showAnteCards() {

        ArrayList<Card> anteCards = app.getBoardClient().getAnte();

        VBox cardsView;
        try {
            if (!anteCards.isEmpty()) {

                cardsView = new VBox();
                Stage stage = new Stage();
                stage.setTitle("Ante Cards");

                cardsView.setAlignment(Pos.CENTER);
                Scene scene = new Scene(cardsView, 200 * anteCards.size() + 6, 369);
                scene.setOnKeyPressed(event -> stage.close());
                cardsView.setStyle("-fx-border-color: #69645e; -fx-border-width: 3px;");

                GridPane cardList = new GridPane();
                for (int i = 0; i < anteCards.size(); ++i) {
                    ImageView newCard = new ImageView("cards/" + anteCards.get(i).getIdCard() + ".png");
                    newCard.setFitHeight(363);
                    newCard.setFitWidth(200);
                    cardList.add(newCard, i, 0);
                    cardList.setStyle("-fx-border-color: #69645e; -fx-border-width: 3px;");
                }
                cardsView.getChildren().add(cardList);

                stage.setScene(scene);
                stage.show();
                cardList.setOnMouseClicked(event -> { stage.close(); });
                currentOpenedWindow.add(stage);
            }
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * display discard when a player click
     * on them
     */
    @FXML
    private void showDiscard() {

        ArrayList<Card> discard = app.getBoardClient().getDiscard();

        try {
            Parent root;

            FXMLLoader fxml = new FXMLLoader(getClass().getResource("/views/discard.fxml"));
            root = fxml.load();

            DiscardController ctr = fxml.getController();
            ctr.setDiscard(discard);

            Stage stage = new Stage();
            stage.setTitle("Discard");
            stage.setScene(new Scene(root, 800, 600));
            stage.setResizable(false);
            stage.setAlwaysOnTop(true);
            stage.show();
            currentOpenedWindow.add(stage);

            ctr.setDiscard(discard);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * launch the main window
     * @param event event to get the source
     *              context
     */
    @FXML
    private void launchBoard(MouseEvent event) {
        Parent root;
        try {

            FXMLLoader fxml = new FXMLLoader(getClass().getClassLoader().getResource("views/board.fxml"));
            fxml.setController(this);
            root = fxml.load();

            Stage stage = new Stage();
            stage.setTitle("Jeu des Dragons");
            stage.setScene(new Scene(root, 1200, 800));
            stage.setResizable(false);
            stage.getScene().setOnMouseClicked(e -> {
                if (e.getButton() == MouseButton.PRIMARY) {
                    if (currentOpenedWindow.size() != 0) {
                        for (Stage s : currentOpenedWindow)
                            s.close();

                        currentOpenedWindow.clear();
                    }
                }
            });
            stage.show();
            this.gameStage = stage;

            // close menu stage because it can be open again later
            primaryStage.close();

            // here is the code to terminate app properly
            stage.setOnCloseRequest(we -> {

                // need to close the server so that
                // he can inform players before system exit
                if(app.getBoardClient() != null)
                    app.getBoardClient().getClient().close();
                if(app.getBoard() != null)
                    app.getBoard().getServer().close(1);

                // kill all threads inside app
                System.exit(0);
                Platform.exit();
            });

        }
        catch (Exception e) { e.printStackTrace(); }
    }

    /**
     * function called each time the server send
     * an update event to refresh the graphical
     * components
     * @param board the client board
     * @param msg a specific message to display
     */
    @FXML
    public void updateBoard(BoardClient board, String msg) {

        Platform.runLater( () -> {

            ArrayList<PlayerClient> players = board.getPlayers();
            int nbPlayers = players.size();

            // set only playing players, update lastNbPlayers
            if (nbPlayers != lastNbPlayers) {

                // player 1
                p1name.setText(players.get(board.getPersonalId()).getName());

                // players 2 - 6
                for(int p = 1; p < nbPlayers; p++) {
                    displayPlayer(imgs[p], coins[p], hands[p], names[p], points[p]);
                    names[p].setText(players.get((board.getPersonalId() + p) % nbPlayers).getName());
                }

                lastNbPlayers = nbPlayers;
            }

            // stuff in the middle
            draw.setText(String.valueOf(board.getNbdrCardDeck()));
            bet.setText(String.valueOf(board.getTotalBet()));
            discard.setText(String.valueOf(board.getDiscard().size()));
            if (board.getDiscard().size() != 0) {

                ImageView discard = new ImageView("cards/" +
                        board.getDiscard().get(board.getDiscard().size() - 1).getIdCard() + ".png");
                discard.setFitHeight( 100.0);
                discard.setPreserveRatio(true);
                discardBtn.setGraphic(discard);
                discardBtn.setOnMouseClicked(event ->{ showDiscard();});
            } else {
                discardBtn.setGraphic(null);
            }
            anteBtn.setOnMouseClicked(event -> {showAnteCards();});
            anteCardsNb.setText(String.valueOf(board.getAnte().size()));

            int id = board.getPersonalId();

            int currentPhase = board.getFlagState();

            if(currentPhase != lastPhase) {
                // if we pass from ante to waiting play
                if (lastPhase == StatusId.ANTE.getId()) {
                    if (currentPhase == StatusId.WAITING_PLAY.getId()) {
                        showAnteCards();
                        animatePlayerMessage("Let's go !");
                    }
                } else if (currentPhase == StatusId.ANTE.getId()) {
                    gameStarted = true;
                    ipLabel.setVisible(false);
                    animatePlayerMessage("Choose Ante Cards !");
                } else if (currentPhase == StatusId.CHOICE.getId()) {
                    animatePlayerMessage("Choosing Step !");
                }
            }

            infoMsg.setText( msg);

            lastPhase = currentPhase;

            // p1
            PlayerClient curPlay = players.get(id);

            // animated coin value changes, (just for me player one)
            int coinsBefore = Integer.valueOf(p1coins.getText());
            int coinsAfter = curPlay.getCoin();
            int diff = coinsAfter - coinsBefore;
            animateCoins(diff);

            // animate info player message if it's his turn
            if(id == board.getCurrentPlayerId() && gameStarted == true)
                animatePlayerMessage("Your Turn !");

            p1coins.setText(String.valueOf(curPlay.getCoin()));
            p1points.setText("vol: " + curPlay.getFlightStrength());
            p1hand.getChildren().clear();
            p1cards.getChildren().clear();
            addPlayableCardsTo( board.getClientHand(), p1hand);
            addCardsTo( curPlay.getFlight(), p1cards);
            if (id == board.getCurrentPlayerId()) p1name.setStyle("-fx-background-color: b7b7a3;");
            else p1name.setStyle("-fx-background-color: ceceb7;");

            for(int p = 1; p < nbPlayers; ++p) {
                id = (id + 1) % players.size();
                updatePlayer(players.get(id), id == board.getCurrentPlayerId(),
                        coins[p], points[p], hands[p], cards[p], names[p], imgs[p]);
            }

            animateCoins(diff);
        });
    }

    /**
     * animation to see whan a player gain or lose
     * money
     * @param diff the coin difference before and after
     *             the update
     */
    public void animateCoins(int diff) {

        if(diff != 0) {

            Timeline timeline = new Timeline();
            timeline.setCycleCount(Timeline.INDEFINITE);
            timeline.getKeyFrames().add(
                new KeyFrame(Duration.millis(60),
                    new EventHandler<ActionEvent>() {
                    int v = 0;
                    @Override
                    public void handle(ActionEvent event) {

                        String sign = diff < 0 ? "-" : "+";

                        if(v++ < Math.abs(diff)) {
                            coinsAnimation.setText(sign + String.valueOf(v));
                        } else {
                            coinsAnimation.setStyle("-fx-font-size: 33");
                        }

                        if (v > 20 + Math.abs(diff)) {
                            timeline.stop();
                            coinsAnimation.setText("");
                            coinsAnimation.setStyle("-fx-font-size: 22");
                        }
                    }
                })
            );
            timeline.playFromStart();
        }
    }

    /**
     * animate the last played card with
     * a growing effect
     * @param node the card on which to
     *             apply the effect
     */
    public void animatePlayedCard(Node node) {
        Timeline timeline = new Timeline(
            new KeyFrame(Duration.ZERO,
                new KeyValue(node.scaleXProperty(), 1.2),
                new KeyValue(node.scaleYProperty(), 1.2)),
            new KeyFrame(Duration.millis(400),
                new KeyValue(node.scaleXProperty(), 1),
                new KeyValue(node.scaleYProperty(),1))
        );
        timeline.playFromStart();
    }

    /**
     * animate important messages on the center
     * of window with a growing effect
     * @param message the message to alert players
     */
    public void animatePlayerMessage(String message) {

        infoPopup.setText(message);
        infoPopup.setTranslateX(400);
        infoPopup.setTranslateY(300);
        infoPopup.setFont(Font.font(60));
        infoPopup.getStylesheets().add("/css/infoMsg.css");

        Timeline timeline = new Timeline(
            new KeyFrame(Duration.ZERO,
                new KeyValue(infoPopup.scaleXProperty(), 1),
                new KeyValue(infoPopup.scaleYProperty(), 1)),
            new KeyFrame(Duration.ZERO,
                new KeyValue(infoPopup.opacityProperty(), 1)),
            new KeyFrame(Duration.millis(2000),
                new KeyValue(infoPopup.scaleXProperty(), 2.5),
                new KeyValue(infoPopup.scaleYProperty(),2.5)),
            new KeyFrame(Duration.millis(2000),
                new KeyValue(infoPopup.opacityProperty(), 0.5))
        );
        timeline.setOnFinished(event -> {infoPopup.setText("");});
        timeline.playFromStart();
    }

    /**
     * display player components. Called when
     * a player arrive in the game
     * @param img gridpane region of the player
     * @param coins coins label
     * @param hand hand label
     * @param name name label
     * @param points score label
     */
    private void displayPlayer(Node img, Node coins, Node hand, Node name, Node points) {
        img.setVisible(true);
        coins.setVisible(true);
        hand.setVisible(true);
        name.setVisible(true);
        points.setVisible(true);
    }

    /**
     * refresh the graphic pane containing specific
     * player components
     * @param curPlay the player
     * @param isCurPlay true or false to know if
     *                  we have to apply special effect
     *                  if it is his turn
     * @param coins coins label
     * @param points score label
     * @param hand hand label
     * @param cards cards pane
     * @param name name label
     * @param img gridpane region of the player
     */
    private void updatePlayer (PlayerClient curPlay, boolean isCurPlay,
                               Label coins, Label points, Label hand, StackPane cards, Label name, Pane img) {

        coins.setText(String.valueOf(curPlay.getCoin()));
        points.setText("vol: " + curPlay.getFlightStrength());
        hand.setText(String.valueOf(curPlay.getHandSize()));
        cards.getChildren().clear();
        addCardsTo( curPlay.getFlight(), cards);

        // add a 'shadow' effect for the current player
        // or disable it
        if (isCurPlay) {
            img.setStyle("-fx-background-radius: 32;" +
                         "-fx-border-radius: 32;" +
                         "-fx-background-color: grey;" +
                         "-fx-opacity: 0.5;" +
                         "-fx-border-color: #69645e;");
        } else {
            img.setStyle("-fx-background-color: transparent;" +
                         "-fx-opacity: 1.0;" +
                         "-fx-border-color: transparent;");
        }
    }

    /**
     * enlarge the clicked card
     * @param id card id
     * @param canPlayCard true if it is a playable card
     *                    i.e. one in the front player
     * @return the opened stage
     */
    public Stage enlargeCard (int id, boolean canPlayCard) {
        VBox card;
        try {

            if(currentEnlargedCard != null)
                currentEnlargedCard.close();

            card = new VBox();
            Stage stage = new Stage();
            stage.setTitle(cardGameList.get(id).getName());
            stage.initStyle(StageStyle.UNDECORATED);

            card.setAlignment(Pos.CENTER);

            Scene scene = new Scene(card, 320, 600);
            scene.setOnKeyPressed(event -> stage.close() );
            card.setStyle( "-fx-border-color: #69645e; -fx-border-width: 3px;");

            ImageView newCard = new ImageView("cards/" + id + ".png");
            newCard.setFitHeight( 568.0);
            newCard.setFitWidth( 314);

            card.getChildren().add(newCard);
            Label label = new Label();
            label.getStylesheets().add("/css/customButton.css");
            if(canPlayCard) {
                label.setText("Play this Card !");
                label.setOnMouseClicked(event -> {
                    BoardClient board = app.getBoardClient();
                    if (board.getCurrentPlayerId() == board.getPersonalId() )
                        board.playCard(board.getClientHand().indexOf(board.searchCardById(id)));
                    else if (board.getFlagState() == StatusId.ANTE.getId())
                        board.playCard(board.getClientHand().indexOf(board.searchCardById(id)));
                    stage.close();
                });
            } else {
                label.setText("Press Any Key to close");
            }
            label.setMinHeight(26);
            label.setMaxHeight(26);
            label.setPrefWidth(314);
            label.setWrapText(true);
            card.getChildren().add(label);

            stage.setScene(scene);
            currentOpenedWindow.add(stage);
            currentEnlargedCard = stage;
            stage.show();

            return stage;
        }
        catch (Exception e) { e.printStackTrace(); }
        return null;
    }

    /**
     * add a card with corresponding image on
     * the correct player stackPane
     * @param cards list of player's cards
     * @param pane the player's stackPane
     */
    private void addCardsTo (ArrayList<Card> cards, StackPane pane) {

        for (Card card : cards) {

            ImageView newCard = new ImageView("cards/" + card.getIdCard() + ".png");
            Button tmp = new Button("", newCard);

            tmp.setMinWidth(60);
            tmp.setMaxWidth(60);
            tmp.setMinHeight(105);
            tmp.setMaxHeight(105);
            newCard.setFitWidth(55);
            newCard.setFitHeight(100);
            tmp.setTranslateX(25 * pane.getChildren().size());

            tmp.setOnMouseClicked(event -> { enlargeCard(card.getIdCard(), false); });
            tmp.getStylesheets().add("/css/cards.css");
            pane.getChildren().add(tmp);

            // this is here because the position of the Stackpane is calculated without rotation,
            // which means the pane is moving to the right when we add cards.
            if (pane == p2cards)
                AnchorPane.setLeftAnchor(p2cards, 70 -p2cards.getWidth() /2 );
            else if (pane == p5cards)
                AnchorPane.setRightAnchor(p5cards, 70 -p5cards.getWidth() /2 );
        }
    }

    /**
     * add cards in the front stackpane
     * @param cards the card list
     * @param pane the front stackPane
     */
    private void addPlayableCardsTo (ArrayList<Card> cards, StackPane pane) {

        for (Card card : cards) {

            ImageView newCard = new ImageView("cards/" + card.getIdCard() + ".png");
            Button tmp = new Button("", newCard);

            tmp.setMinWidth(148);
            tmp.setMaxWidth(148);
            newCard.setFitWidth(138);
            newCard.setFitHeight(250);
            tmp.setTranslateX(70 * pane.getChildren().size());

            tmp.setOnMouseClicked(event -> { enlargeCard(card.getIdCard(), true); });
            tmp.getStylesheets().add("/css/cards.css");
            pane.getChildren().add(tmp);
        }
    }

    /**
     * set a reference to the game
     * @param app the game
     */
    public void setApp(Game app) {
        this.app = app;
    }

    /**
     * @return the game
     */
    public Game getApp() {
        return app;
    }

    /**
     * reset menu window styles after user
     * entered text or got an error
     */
    private void resetMenuStyles() {
        name.setOnMouseClicked(event1 -> {
            name.setText("");
            name.setStyle("-fx-text-fill: BLACK");
        });
        serverAddr.setOnMouseClicked(event1 -> {
            serverAddr.setText("");
            serverAddr.setStyle("-fx-text-fill: BLACK");
        });
    }

    /**
     * launch the main board if a user is
     * accepted or give an error message
     * (calling game join method)
     * @param event event to get the source
     *              context
     */
    @FXML
    private void joinGame (MouseEvent event) {

        resetMenuStyles();

        if (serverAddr.getText().length() == 0) {
            serverAddr.setStyle("-fx-text-fill: RED");
            serverAddr.setText("HOST IP REQUIRED");
            return;
        }
        if (!serverAddr.getText().matches("([\\d]{1,3}\\.){3}[\\d]{1,3}")) {
            serverAddr.setStyle("-fx-text-fill: RED");
            serverAddr.setText("wrong format");
            return;
        }
        if (name.getText().length() == 0) {
            name.setStyle("-fx-text-fill: RED");
            name.setText("NAME REQUIRED");
            return;
        }

        if(app.join(serverAddr.getText(), name.getText(), this)) {
            launchBoard(event);
            startGame.setVisible(false);
        }
        // check if reconnected client
        else if(app.getBoardClient().getClient().getReconnected()) {
            launchBoard(event);
            startGame.setVisible(false);
        } else {
            serverAddr.setStyle("-fx-text-fill: RED");
            serverAddr.setText("You cannot join");
        }
    }

    /**
     * launch the main board and call the host
     * method in Game
     * @param event event to get the source
     *              context
     */
    @FXML
    private void hostGame (MouseEvent event) {

        resetMenuStyles();

        if (name.getText().length() == 0) {
            name.setStyle("-fx-text-fill: RED");
            name.setText("NAME REQUIRED");
            return;
        }
        launchBoard(event);

        app.host(name.getText(), this);
        String ip = app.getBoard().getServer().getPublicIpAddress();
        try {
            if (ip == null) // on OSX getPublicIpAddress() can be null, this is a fallback
                ip = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        ipLabel.setText("Give this ip to your co-players: " + ip);
    }

    /**
     * start the game and make the
     * button invisible
     */
    @FXML
    private void startGame () {
        if(app.getBoardClient().startGame())
            startGame.setVisible(false);
    }

    /**
     * display on the bottom right server
     * responses
     * @param msg server responses
     */
    public void displayServerMessage(String msg) {
        if(serverMsg != null) {
            serverMsg.setFill(Color.RED);
            serverMsg.setText(msg);
        }
    }

    /**
     * open choice window with choice list
     * @param choices choice list
     */
    public void showChoices(ArrayList<String> choices) {

        Platform.runLater(() -> {

            try {

                Parent root;

                FXMLLoader fxml = new FXMLLoader(getClass().getResource("/views/choices.fxml"));
                root = fxml.load();

                Stage stage = new Stage();
                stage.setTitle("Choose option");
                stage.setScene(new Scene(root, 450, 450));
                stage.setResizable(false);
                stage.setAlwaysOnTop(true);
                stage.initStyle(StageStyle.UNDECORATED);
                stage.show();

                ChoicesController choicesController = fxml.getController();
                choicesController.setMainController(this);
                choicesController.showChoices(choices);

            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * open endGame window, that allow to
     * restart or to leave
     * @param isHardQuit
     */
    @FXML
    public void showEndgame(boolean isHardQuit) {

        Platform.runLater(() -> {

            try {

                Parent root;

                FXMLLoader fxml = new FXMLLoader(getClass().getResource("/views/endgame.fxml"));
                root = fxml.load();

                Stage stage = new Stage();
                stage.setTitle("Game Finished");
                stage.setScene(new Scene(root, 800, 533));
                stage.setResizable(false);
                stage.setAlwaysOnTop(true);
                stage.show();

                EndgameController endgameController = fxml.getController();
                endgameController.setMainController(this, isHardQuit);

                /* close the game window */

            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * open a window containing all the cards
     * and the rules
     */
    @FXML
    public void showRules() {

        Parent root;

        try {

            FXMLLoader fxml = new FXMLLoader(getClass().getResource("/views/rules.fxml"));
            root = fxml.load();

            Stage stage = new Stage();
            stage.setTitle("Cards and Rules");
            stage.setScene(new Scene(root, 800, 533));
            stage.setResizable(false);
            stage.show();

            RulesController rc = fxml.getController();
            rc.setMainController(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
