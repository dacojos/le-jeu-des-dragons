package controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import models.card.Card;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class DiscardController {

    private static int WIDTH = 800;
    private static int HEIGHT = 600;
    private static int OFFSET_X = 30;
    private static int OFFSET_Y = 30;
    private static int CARD_WIDTH = 242;
    private static int CARD_HEIGHT = 440;
    private static int CARD_SPACING = 10;

    private ArrayList<Card> discard;

    @FXML
    private Pane cardsPane;
    @FXML
    private ScrollPane scrollPane;

    public void setDiscard(ArrayList<Card> discard) {

        this.discard = discard;

        int nbCardsPerRow = WIDTH / (CARD_SPACING + CARD_WIDTH);

        for(int i = 0; i < discard.size(); ++i) {
            ImageView newCard = new ImageView("cards/" + discard.get(i).getIdCard() + ".png");
            Button tmp = new Button("", newCard);

            int colIndex = i % nbCardsPerRow;
            int rowIndex = i / nbCardsPerRow;

            tmp.setTranslateX(OFFSET_X + colIndex * (CARD_SPACING + CARD_WIDTH));
            tmp.setTranslateY(OFFSET_Y + rowIndex * (CARD_SPACING + CARD_HEIGHT));
            tmp.setMinWidth(CARD_WIDTH);
            tmp.setMaxWidth(CARD_WIDTH);
            tmp.setMinHeight(CARD_HEIGHT);
            tmp.setMaxHeight(CARD_HEIGHT);
            newCard.setFitWidth(CARD_WIDTH - 5);
            newCard.setFitHeight(CARD_HEIGHT - 5);

            cardsPane.getChildren().add(tmp);
        }

        int height = (CARD_HEIGHT + CARD_SPACING) * discard.size() / nbCardsPerRow
                + CARD_HEIGHT * (1 - discard.size() % 2) + OFFSET_Y;
        cardsPane.setPrefHeight(Math.max(height, 600));
    }
}
