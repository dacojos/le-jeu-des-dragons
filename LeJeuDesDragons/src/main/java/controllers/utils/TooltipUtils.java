package controllers.utils;

import javafx.scene.Node;
import javafx.scene.control.Tooltip;

public class TooltipUtils {

    public static void bindTooltip(final Node node, final Tooltip tooltip) {

        node.setOnMouseEntered(event -> {
            tooltip.show(node, event.getScreenX() + 30, event.getScreenY() + 30);
        });
        node.setOnMouseExited(event -> {
            tooltip.hide();
        });

    }
}
