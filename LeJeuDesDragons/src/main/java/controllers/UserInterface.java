package controllers;

import game.Game;
import models.board.BoardClient;

import java.util.ArrayList;


public interface UserInterface {
    void setApp(Game game);

    void updateBoard(BoardClient board, String msg);

    void displayServerMessage(String msg);

    void showChoices(ArrayList<String> choices);

    void showEndgame(boolean isHardQuit);
}
