package controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.util.ArrayList;


public class ChoicesController {

    // need to keep a ref to the main ctr to call its method
    private Controller mainController;

    @FXML
    private ListView<Button> choicesList;
    private final ObservableList<Button> listItems = FXCollections.observableArrayList();

    /**
     * function that open a window with the different
     * choices in a list (clickable items)
     * @param choices the list of choices
     */
    public void showChoices(ArrayList<String> choices) {

        choicesList.setItems(listItems);

        for(String choice : choices) {

            Button button = new Button();
            button.setText(choice);

            // custom button containing text
            button.setTextFill(Color.BLACK);
            button.setMinWidth(400);
            button.setMaxWidth(400);
            button.setMaxHeight(1600);
            button.setStyle("-fx-background-color: transparent");
            button.setWrapText(true);
            button.setTextAlignment(TextAlignment.LEFT);

            listItems.add(button);

            // send choice id to game engine and close window
            button.setOnMouseClicked(event -> {

                mainController.getApp().getBoardClient().sendChoice(choices.indexOf(choice));

                Stage stage = (Stage)button.getScene().getWindow();
                stage.close();
            });
        }
    }

    /**
     * keep a reference to the main controller
     * (so it is possible to call with server when
     * a choice is made)
     * @param mainController the controller of the main window
     */
    public void setMainController(Controller mainController) {
        this.mainController = mainController;
    }
}
