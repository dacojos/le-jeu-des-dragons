package controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import models.player.PlayerClient;

import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.ResourceBundle;

public class EndgameController implements Initializable {

    /* need to keep a ref to the main ctr
     * to call its method
     */
    private Controller mainController;
    private boolean isHardQuit;

    @FXML
    private Button restart, backToMenu;

    @FXML
    private Label firstName, secondName, thirdName, fourthName, fifthName, sixthName,
            firstIcon, secondIcon, thirdIcon, fourthIcon, fifthIcon, sixthIcon;

    private Label[] names;
    private Label[] icons;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        names = new Label[]{firstName, secondName, thirdName, fourthName, fifthName, sixthName};
        icons = new Label[]{firstIcon, secondIcon, thirdIcon, fourthIcon, fifthIcon, sixthIcon};
    }

    public void setMainController(Controller mainController, boolean isHardQuit) {

        this.mainController = mainController;
        this.isHardQuit = isHardQuit;

        ArrayList<PlayerClient> players = mainController.getApp().getBoardClient().getPlayers();

        // custom the buttons
        if(isHardQuit) {
            restart.setVisible(false);
        } else {
            restart.setOnMouseClicked(event -> {
                // Player choose to restart
                mainController.getApp().getBoardClient().chooseEndGame(true);

                // close current window
                Node source = (Node) event.getSource();
                Stage endStage  = (Stage) source.getScene().getWindow();
                endStage.close();
            });
        }

        // display ranking
        players.sort((p1, p2) -> p2.getCoin() - p1.getCoin());
        for(int i = 0; i < 6; ++i){
            if(i < players.size()) {
                names[i].setVisible(true);
                names[i].setText(players.get(i).getName());
                icons[i].setVisible(true);
            } else {
                names[i].setVisible(false);
                names[i].setText("");
                icons[i].setVisible(false);
            }
        }
      
        backToMenu.setOnMouseClicked(event -> {
            // close current window
            Node source = (Node) event.getSource();
            Stage endStage  = (Stage) source.getScene().getWindow();
            endStage.close();

            // re-launch menu stage
            mainController.backToMenu();
        });
    }
}
