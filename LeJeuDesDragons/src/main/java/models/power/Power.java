package models.power;

import models.board.Board;
import models.event.choice.ChoiceMade;
import models.player.Player;

/**
 * @brief   : Class interface pour les pouvoirs offrant 2 méthodes une qui active le pouvoir et le second qui applique
 * un choix réalisé par le joueur
 * @Date    : 28.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public interface Power {
    /**
     * @brief               : Active le pouvoir de la carte
     * @param board         : Board du plateau de jeu
     * @param currentPlayer : Player ayant joué la carte
     */
    void activatePower(Board board, Player currentPlayer);

    /**
     * @brief               : Réalise l'action en fonction du choix fait
     * @param board         : Board du plateau de jeu
     * @param currentPlayer : Player ayant activé le pouvoir
     * @param choiceMade    : ChoiceMade contenant la réponse au choix
     */
    void applyChoice(Board board, Player currentPlayer, ChoiceMade choiceMade);
}
