package models.power.dragon.color;

import models.board.Board;
import models.event.choice.ChoiceMade;
import models.player.Player;
import models.power.Power;

/**
 * @brief   : Class implémentant le pouvoir des dragons noirs : vole 2 pièces d'or à la mise
 * @Date    : 28.11.2019
 * @Author  : Gomes Da Costa Joshua
 */
public class Black implements Power {
    private final int GOLD_STEAL = 2;

    @Override
    public void activatePower(Board board, Player currentPlayer) {
        int coins = board.trySub(GOLD_STEAL);
        board.subBet( coins);
        currentPlayer.addCoin(GOLD_STEAL);
    }

    @Override
    public void applyChoice(Board board, Player currentPlayer, ChoiceMade choiceMade) {

    }

    @Override
    public String toString(){
        return "Volez 2 pieces d'or a la mise.";
    }
}
