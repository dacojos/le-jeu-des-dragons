package models.power.dragon.color;

import models.board.Board;
import models.card.Card;
import models.event.choice.ChoiceMade;
import models.player.Player;
import models.power.Power;

/**
 * @brief   : Class implémentant le pouvoir des dragons blancs : prend 3 pièces d'or à la mise si un mortelle est dans
 * un vol.
 * @Date    : 28.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class White implements Power {
    private final int GOLD_STEAL = 3;

    @Override
    public void activatePower(Board board, Player currentPlayer) {

        for(Player p : board.getPlayers()){

            if(p.getFlight() != null) {
                for (Card c : p.getFlight()) {

                    if(c.getClass().getSimpleName().equals("Mortal")){
                        int sub = board.trySub(GOLD_STEAL);

                        currentPlayer.addCoin(sub);
                        board.subBet(sub);
                        return;
                    }
                }
            }
        }
    }

    @Override
    public void applyChoice(Board board, Player currentPlayer, ChoiceMade choiceMade) {

    }

    @Override
    public String toString(){
        return "Si un vol possede un mortel, volez 3 pieces \n" +
                "d'or a la mise.";
    }
}
