package models.power.dragon.color;

import models.board.Board;
import models.card.Card;
import models.event.choice.ChoiceMade;
import models.jsonHandler.JsonCreator;
import models.player.Player;
import models.power.Power;
import models.statusBoard.Choice;
import models.statusBoard.WaitingPlay;

import java.util.ArrayList;

/**
 * @brief   : Class implémentant le pouvoir des dragons bleus : vole 1 pièces d'or à la mise pour chaque dragon mauvais
 * dans son vol ou les autres donne ce nombre à la mise
 * @Date    : 28.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Blue implements Power {
    @Override
    public void activatePower(Board board, Player currentPlayer) {
        board.setStatusBoard(new Choice(board.getStatusBoard()));
        board.getStatusBoard().setCurrentChoicePlayerId(currentPlayer.getId());

        ArrayList<String> choiceOption = new ArrayList<>();

        choiceOption.add("Volez 1 piece d'or à la mise pour chaque dragon mauvais dans votre vol");
        choiceOption.add("Les autres joueurs doivent payez ce montant d'or a la mise.");


        board.getServer().choiceOption(board.getStatusBoard().getCurrentChoicePlayerId(),
                new JsonCreator().choiceOption(board.getStatusBoard().getCurrentChoicePlayerId(), choiceOption));
    }

    @Override
    public void applyChoice(Board board, Player currentPlayer, ChoiceMade choiceMade) {
        int valueToSteal = 0;
        for(Card c : currentPlayer.getFlight()){
            if(c.isBad()){
                valueToSteal++;
            }
        }

        if(choiceMade.getIndexChoose() == 0){
            board.setMsg( currentPlayer.getName() + " a choisi de prendre " + valueToSteal + " po à la mise.");
            valueToSteal = board.trySub(valueToSteal);
            currentPlayer.addCoin(valueToSteal);
            board.subBet(valueToSteal);

        } else {
            board.setMsg( currentPlayer.getName() + " a choisi de forcer les joueurs à donner " + valueToSteal + " po à la mise.n");
            for (Player p : board.getPlayers()){
                if(p.getId() != currentPlayer.getId()){
                    p.subCoin(valueToSteal);
                    board.addBet(valueToSteal);
                }
            }
        }

        board.getStatusBoard().setCurrentChoicePlayerId(-1);
        board.setStatusBoard(new WaitingPlay(board.getStatusBoard()));
    }

    @Override
    public String toString(){
        return "Choisissez : volez 1 piece d'or à la mise pour \n" +
                "chaque dragon mauvais dans votre vol ; \n" +
                "ou les autres joueurs doivent payez ce \n" +
                "montant d'or a la mise.";
    }
}
