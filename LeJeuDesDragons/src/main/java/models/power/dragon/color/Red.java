package models.power.dragon.color;

import models.board.Board;
import models.event.choice.ChoiceMade;
import models.jsonHandler.JsonCreator;
import models.player.Player;
import models.power.Power;
import models.statusBoard.Choice;
import models.statusBoard.WaitingPlay;

import java.util.ArrayList;
import java.util.Random;

/**
 * @brief   : Class implémentant le pouvoir des dragons rouges : le joueur avec le vol le plus puissant
 * (à choix si plusieurs) donne 1 pièce d'or et une cartes au hasard de sa main
 * @Date    : 28.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Red implements Power {
    private final int GOLD_STEAL = 1;
    private Random rand = new Random();
    private ArrayList<Player> bestPlayer = new ArrayList<>();

    @Override
    public void activatePower(Board board, Player currentPlayer) {
        bestPlayer = new ArrayList<>();

        for(Player p : board.getPlayers()){
            if(p.getId() != currentPlayer.getId()){
                if(bestPlayer.isEmpty()){
                    bestPlayer.add(p);
                } else if (bestPlayer.get(0).getFlightStrength() < p.getFlightStrength()){
                    bestPlayer = new ArrayList<>();
                    bestPlayer.add(p);
                } else if (bestPlayer.get(0).getFlightStrength() == p.getFlightStrength()){
                    bestPlayer.add(p);
                }
            }
        }

        if(bestPlayer.size() == 1){
            bestPlayer.get(0).subCoin(GOLD_STEAL);
            currentPlayer.addCoin(GOLD_STEAL);

            int nbrRandom = rand.nextInt(bestPlayer.get(0).getSizeHand());

            board.moveCard(bestPlayer.get(0).getHand(), currentPlayer.getHand(), nbrRandom);
        } else if(bestPlayer.size() > 1){
            board.setStatusBoard(new Choice(board.getStatusBoard()));
            board.getStatusBoard().setCurrentChoicePlayerId(currentPlayer.getId());
            board.getStatusBoard().setCardActivated(board.getCardPlayed().get(currentPlayer.getId()));

            ArrayList<String> choiceOption = new ArrayList<>();

            for (Player p : bestPlayer){
                choiceOption.add(p.getName());
            }

            board.getServer().choiceOption(board.getStatusBoard().getCurrentChoicePlayerId(),
                    new JsonCreator().choiceOption(board.getStatusBoard().getCurrentChoicePlayerId(), choiceOption));
        }
    }

    @Override
    public void applyChoice(Board board, Player currentPlayer, ChoiceMade choiceMade) {
        board.getStatusBoard().setCurrentChoicePlayerId(-1);
        board.setStatusBoard(new WaitingPlay(board.getStatusBoard()));
        board.setMsg( currentPlayer.getName() + " a choisi de cibler " + bestPlayer.get(choiceMade.getIndexChoose()).getName() + ".");

        bestPlayer.get(choiceMade.getIndexChoose()).subCoin(GOLD_STEAL);
        currentPlayer.addCoin(GOLD_STEAL);


        int nbrRandom = rand.nextInt(bestPlayer.get(choiceMade.getIndexChoose()).getSizeHand());
        board.moveCard(bestPlayer.get(choiceMade.getIndexChoose()).getHand(), currentPlayer.getHand(), nbrRandom);
    }

    @Override
    public String toString(){
        return "Le joueur adverse possedant le vol le plus \n" +
                "puissant vous paye 1 piece d'or. \n" +
                "Piocher une carte au hasard de sa main.";
    }
}
