package models.power.dragon.color;

import models.board.Board;
import models.event.choice.ChoiceMade;
import models.player.Player;
import models.power.Power;

/**
 * @brief   : Class implémentant le pouvoir de tiamat : ne possède pas de pouvoir actif (Dans cette version)
 * @Date    : 28.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Tiamat implements Power {
    @Override
    public void activatePower(Board board, Player currentPlayer) {

    }

    @Override
    public void applyChoice(Board board, Player currentPlayer, ChoiceMade choiceMade) {

    }

    @Override
    public String toString(){
        return "Dragon Divin - Tiamat est simultanement \n" +
                "un Dragon Blanc, Bleu, Noire, Rouge et Vert. \n" +
                "Tant que vous avez Tiamat et un Dragon \n" +
                "bon dans votre vol vous ne pouvez pas \n" +
                "gagnez la partie";
    }
}
