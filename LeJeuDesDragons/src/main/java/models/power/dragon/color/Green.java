package models.power.dragon.color;

import models.board.Board;
import models.card.Card;
import models.event.choice.ChoiceMade;
import models.jsonHandler.JsonCreator;
import models.player.Player;
import models.power.Power;
import models.statusBoard.Choice;
import models.statusBoard.WaitingPlay;

import java.util.ArrayList;

/**
 * @brief   : Class implémentant le pouvoir des dragons verts : le joueur à gauche donne 5 pièces d'or ou une carte
 * plus faible que celle joué
 * @Date    : 28.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Green implements Power {
    private final int GOLD_STEAL = 5;
    private Player targetPlayer = null;
    private ArrayList<Card> listChoice = new ArrayList<>();

    @Override
    public void activatePower(Board board, Player currentPlayer) {
        targetPlayer = null;
        listChoice = new ArrayList<>();

        int targetPlayerId = (currentPlayer.getId() + 1) % board.getNumberPlayer();
        targetPlayer = board.getPlayers().get(targetPlayerId);

        board.setStatusBoard(new Choice(board.getStatusBoard()));
        board.getStatusBoard().setCurrentChoicePlayerId(targetPlayerId);

        ArrayList<String> choiceOption = new ArrayList<>();

        choiceOption.add("Donner 5 po");
        for (Card c : targetPlayer.getHand()){
            if(c.getStrength() < board.getCardPlayed().get(currentPlayer.getId()).getStrength() && c.isBad()){
                listChoice.add(c);
                choiceOption.add(c.toString());
            }
        }

        board.getServer().choiceOption(board.getStatusBoard().getCurrentChoicePlayerId(),
                new JsonCreator().choiceOption(board.getStatusBoard().getCurrentChoicePlayerId(), choiceOption));
    }

    @Override
    public void applyChoice(Board board, Player currentPlayer, ChoiceMade choiceMade) {
        if(choiceMade.getIndexChoose() == 0){
            board.setMsg( targetPlayer.getName() + " a choisi de donner 5 po à " + currentPlayer.getName() + ".");
            currentPlayer.addCoin(GOLD_STEAL);
            targetPlayer.subCoin(GOLD_STEAL);
        } else {
            board.setMsg( targetPlayer.getName() + " a choisi de donner " + listChoice.get(choiceMade.getIndexChoose() - 1) + " à " + currentPlayer.getName() + ".");
            for(int i = 0; i < targetPlayer.getSizeHand(); ++i){
                if(targetPlayer.getHand().get(i).getIdCard() == listChoice.get(choiceMade.getIndexChoose()-1).getIdCard()){
                    board.moveCard(targetPlayer.getHand(), currentPlayer.getHand(), i);
                    break;
                }
            }
        }

        board.getStatusBoard().setCurrentChoicePlayerId(-1);
        board.setStatusBoard(new WaitingPlay(board.getStatusBoard()));
    }

    @Override
    public String toString(){
        return "Le joueur a votre gauche choisit de vous \n" +
                "donner de sa main un dragon mauvais \n" +
                "plus faible que celui-ci (montrez-le aux \n" +
                "autres joueurs) ou de vous payer \n" +
                "5 piece d'or.";
    }
}
