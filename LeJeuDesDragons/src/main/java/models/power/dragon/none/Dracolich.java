package models.power.dragon.none;

import models.board.Board;
import models.card.Card;
import models.card.dragon.Color;
import models.event.choice.ChoiceMade;
import models.jsonHandler.JsonCreator;
import models.player.Player;
import models.power.Power;
import models.statusBoard.Choice;
import models.statusBoard.WaitingPlay;

import java.util.ArrayList;

/**
 * @brief   : Class implémentant le pouvoir du Dracoliche : le joueur choisie un autre dragon mauvais parmis les vols
 * et active ce pouvoir
 * @Date    : 28.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Dracolich implements Power {
    private ArrayList<Card> listOfDragon = new ArrayList<>();

    @Override
    public void activatePower(Board board, Player currentPlayer) {
        listOfDragon = new ArrayList<>();

        for (Player p : board.getPlayers()){
            for(Card c : p.getFlight()){
                if(c.isBad() && c.getColor() != Color.DIVIN){
                    listOfDragon.add(c);
                }
            }
        }

        if(!listOfDragon.isEmpty()) {
            board.setStatusBoard(new Choice(board.getStatusBoard()));
            board.getStatusBoard().setCurrentChoicePlayerId(currentPlayer.getId());
            board.getStatusBoard().setCardActivated(board.getCardPlayed().get(currentPlayer.getId()));

            ArrayList<String> choiceOption = new ArrayList<>();

            for (Card c : listOfDragon) {
                choiceOption.add(c.getName() + ": " + c.getPower().toString());
            }

            board.getServer().choiceOption(board.getStatusBoard().getCurrentChoicePlayerId(),
                    new JsonCreator().choiceOption(board.getStatusBoard().getCurrentChoicePlayerId(), choiceOption));
        }
    }

    @Override
    public void applyChoice(Board board, Player currentPlayer, ChoiceMade choiceMade) {
        board.setMsg(currentPlayer.getName() + " a choisi d'utiliser le pouvoir suivant : " + listOfDragon.get(choiceMade.getIndexChoose()) + ".");
        board.getStatusBoard().setCurrentChoicePlayerId(-1);
        board.setStatusBoard( new WaitingPlay(board.getStatusBoard()));

        board.getStatusBoard().setCardActivated(listOfDragon.get(choiceMade.getIndexChoose()));
        listOfDragon.get(choiceMade.getIndexChoose()).activatePower(board, currentPlayer);

    }

    @Override
    public String toString(){
        return "Copiez le pouvoir d'un dragon mauvais \n"+
                "du vol de votre choix";
    }
}