package models.power.dragon.metal;

import models.board.Board;
import models.card.Card;
import models.event.choice.ChoiceMade;
import models.jsonHandler.JsonCreator;
import models.player.Player;
import models.power.Power;
import models.statusBoard.Choice;
import models.statusBoard.WaitingPlay;

import java.util.ArrayList;

/**
 * @brief   : Class implémentant le pouvoir des dragons bronzes : prend les 2 crates d'anté les plus faibles
 * @Date    : 28.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Bronze implements Power {
    private ArrayList<Card> listChoice = new ArrayList<>();  //Contient la position de la carte dans l'ante et la carte
    private int maxCardToTake;

    private void moveAnteWeaker(int strength, Board board, Player p){
        for (int i = 0; i < board.getAnte().size(); ) {
            if (board.getAnte().get(i).getStrength() < strength) {
                board.moveCard(board.getAnte(), p.getHand(), i);
                maxCardToTake--;
            } else {
                ++i;
            }
        }
    }

    @Override
    public void activatePower(Board board, Player currentPlayer) {
        listChoice = new ArrayList<>();
        int sum = 0;
        maxCardToTake = 0;

        int[] countingSort = new int[Card.MAX_STRENGTH];
        for(Card c : board.getAnte()){
            countingSort[c.getStrength() - 1]++;
        }

        maxCardToTake = Math.min(Board.NB_MAX_CARD_HAND - currentPlayer.getSizeHand(), 2);

        if(board.getAnte().size() > maxCardToTake) {
            int strength = 0;   //force maximal + 1

            while (sum < maxCardToTake) {
                sum += countingSort[strength++];
            }

            //S'il y a pas de choix à faire alors prend les cartes
            if (sum == maxCardToTake) {
                moveAnteWeaker( strength + 1, board, currentPlayer);
            //sinon regarde les cartes qu'il doit choisire et les soumets
            } else {
                for(int i = 0; i < strength; ++i){
                    if(countingSort[i] < maxCardToTake){
                        moveAnteWeaker(i + 2,board,currentPlayer);
                    } else {
                        board.setStatusBoard(new Choice(board.getStatusBoard()));
                        board.getStatusBoard().setCurrentChoicePlayerId(currentPlayer.getId());

                        for (Card c : board.getAnte()){
                            if(c.getStrength() - 1 == i) {
                                listChoice.add(c);
                            }
                        }

                        ArrayList<String> choiceOption = new ArrayList<>();

                        for(int j = 0; j < listChoice.size(); ++j){
                            choiceOption.add(listChoice.get(j).toString());
                        }

                        board.getServer().choiceOption(board.getStatusBoard().getCurrentChoicePlayerId(),
                                new JsonCreator().choiceOption(board.getStatusBoard().getCurrentChoicePlayerId(), choiceOption));
                    }
                }
            }
        } else {
            while (!board.getAnte().isEmpty()){
                board.moveCard(board.getAnte(), currentPlayer.getHand(), 0);
            }
        }
    }

    @Override
    public void applyChoice(Board board, Player currentPlayer, ChoiceMade choiceMade) {
        board.setMsg(currentPlayer.getName() + " a choisi de prendre " + listChoice.get(choiceMade.getIndexChoose()) + ".");
        for( int i = 0; i < board.getAnte().size(); i++) {
            if(board.getAnte().get(i).getIdCard() == listChoice.get(choiceMade.getIndexChoose()).getIdCard()) {
                listChoice.remove(choiceMade.getIndexChoose());
                board.moveCard(board.getAnte(), currentPlayer.getHand(), i);
                maxCardToTake--;
            }
        }

        if( maxCardToTake == 0) {
            board.getStatusBoard().setCurrentChoicePlayerId(-1);
            board.setStatusBoard( new WaitingPlay(board.getStatusBoard()));
        }
        else {
            ArrayList<String> choiceOption = new ArrayList<>();

            for(int j = 0; j < listChoice.size(); ++j){
                choiceOption.add( listChoice.get(j).toString());
            }

            board.getServer().choiceOption( board.getStatusBoard().getCurrentChoicePlayerId(),
                    new JsonCreator().choiceOption( board.getStatusBoard().getCurrentChoicePlayerId(), choiceOption));
        }
    }

    @Override
    public String toString(){
        return "Recuperez les deux cartes d'ante les plus \n" +
                "faibles dans votre main";
    }
}
