package models.power.dragon.metal;

import models.board.Board;
import models.card.Card;
import models.event.choice.ChoiceMade;
import models.player.Player;
import models.power.Power;

/**
 * @brief   : Class implémentant le pouvoir des dragons dorés : pioche 1 carte par dragon bon dans son vol (celui-ci
 * compris)
 * @Date    : 28.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Gold implements Power {
    @Override
    public void activatePower(Board board, Player currentPlayer) {
        int nbrCardToDraw = 0;

        for(Card c : currentPlayer.getFlight()){
            if(c.isGood()){
                ++nbrCardToDraw;
            }
        }

        if(nbrCardToDraw != 0){
            board.draw(currentPlayer, nbrCardToDraw);
        }

    }

    @Override
    public void applyChoice(Board board, Player currentPlayer, ChoiceMade choiceMade) {

    }

    @Override
    public String toString(){
        return "Piocher une carte pour chaque dragon bon \n"+
                "dans votre vol.";
    }
}
