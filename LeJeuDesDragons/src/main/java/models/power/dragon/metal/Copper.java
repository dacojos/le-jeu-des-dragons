package models.power.dragon.metal;

import models.board.Board;
import models.card.Card;
import models.event.choice.ChoiceMade;
import models.player.Player;
import models.power.Power;

/**
 * @brief   : Class implémentant le pouvoir des dragons cuivres : le joueur se défausse de cette carte, prend la
 * première carte de la pioche et la place dans son vol puis active son pouvoir.
 * @Date    : 28.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Copper implements Power {
    @Override
    public void activatePower(Board board, Player currentPlayer) {
        int pos = 0;
        for(int i = 0; i < currentPlayer.getSizeFlight(); ++i) {
            if(board.getStatusBoard().getCardActivated().getIdCard() == currentPlayer.getFlight().get(i).getIdCard()) {
                board.moveCard(currentPlayer.getFlight(), board.getDiscard(), i);
                pos = i;
                break;
            }
        }


        currentPlayer.getFlight().add(pos, board.getDeck().remove(0));
        Card cardAdd = currentPlayer.getFlight().get(pos);

        if(pos == currentPlayer.getSizeFlight() - 1) {
            board.getCardPlayed().set(currentPlayer.getId(), cardAdd);
        }

        board.getStatusBoard().setCardActivated(cardAdd);
        cardAdd.activatePower(board, currentPlayer);
    }

    @Override
    public void applyChoice(Board board, Player currentPlayer, ChoiceMade choiceMade) {

    }

    @Override
    public String toString(){
        return "Défaussez cette carte et remplacez-la par \n" +
                "la premiere carte de la pioche. Le pouvoir \n" +
                "de la carte piochee se declenche quelle \n" +
                "que soit sa force";
    }
}
