package models.power.dragon.metal;

import models.board.Board;
import models.card.Card;
import models.event.choice.ChoiceMade;
import models.player.Player;
import models.power.Power;

/**
 * @brief   : Class implémentant le pouvoir des dragons argents : tous les joueurs qui possèdent un dragon bon dans
 * leurs vols pioche une carte
 * @Date    : 28.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Silver implements Power {
    private final int NBR_CARD_DRAW = 1;

    @Override
    public void activatePower(Board board, Player currentPlayer) {
        for (Player p : board.getPlayers()){
            if(p.getFlight() != null){
                for (Card c : p.getFlight()){
                    if(c.isGood()){
                        board.draw(p, NBR_CARD_DRAW);
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void applyChoice(Board board, Player currentPlayer, ChoiceMade choiceMade) {

    }

    @Override
    public String toString(){
        return "Chaque joueur possedant au moin un \n" +
                "dragon bon dans son vol pioche une carte.";
    }
}
