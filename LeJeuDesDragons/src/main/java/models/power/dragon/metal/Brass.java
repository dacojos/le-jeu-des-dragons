package models.power.dragon.metal;

import models.board.Board;
import models.card.Card;
import models.event.choice.ChoiceMade;
import models.jsonHandler.JsonCreator;
import models.player.Player;
import models.power.Power;
import models.statusBoard.Choice;
import models.statusBoard.WaitingPlay;

import java.util.ArrayList;

/**
 * @brief   : Class implémentant le pouvoir des dragons arains : le joueur avec  Le vol le plus puissant
 * (au choix si égalité) doit donner un dragon plus fort que celui qui a activé le pouvoir ou bien de donner 5 po
 * @Date    : 28.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Brass implements Power {
    private final int GOLD_STEAL = 5;
    private ArrayList<Player> targetPlayer = null;
    private Player finalSelectedPlayer = null;
    private ArrayList<Card> listChoice = new ArrayList<>();

    @Override
    public void activatePower(Board board, Player currentPlayer) {
        targetPlayer = new ArrayList<>();
        listChoice = new ArrayList<>();
        finalSelectedPlayer = null;

        for(Player p : board.getPlayers()){
            if(p.getId() != currentPlayer.getId()){
                if(targetPlayer.isEmpty()){
                    targetPlayer.add(p);
                } else if (targetPlayer.get(0).getFlightStrength() < p.getFlightStrength()){
                    targetPlayer = new ArrayList<>();
                    targetPlayer.add(p);
                } else if (targetPlayer.get(0).getFlightStrength() == p.getFlightStrength()){
                    targetPlayer.add(p);
                }
            }
        }

        board.setStatusBoard(new Choice(board.getStatusBoard()));
        board.getStatusBoard().setCardActivated(board.getCardPlayed().get(currentPlayer.getId()));

        ArrayList<String> choiceOption = new ArrayList<>();

        //Si 1 seul joueur envoie directement la liste de choix
        if(targetPlayer.size() == 1){
            finalSelectedPlayer = targetPlayer.get(0);

            choiceOption.add("Donner 5 po");
            board.getStatusBoard().setCurrentChoicePlayerId(finalSelectedPlayer.getId());

            for (Card c : finalSelectedPlayer.getHand()){
                if(c.getStrength() > board.getStatusBoard().getCardActivated().getStrength() && c.isGood()){
                    listChoice.add(c);
                    choiceOption.add(c.toString());
                }
            }
        //Autrement envoie la liste des joueurs potentielles
        } else {
            board.getStatusBoard().setCurrentChoicePlayerId(currentPlayer.getId());

            for (int i = 0; i < targetPlayer.size(); i++) {
                choiceOption.add(targetPlayer.get(i).getName());
            }
        }

        board.getServer().choiceOption(board.getStatusBoard().getCurrentChoicePlayerId(),
                new JsonCreator().choiceOption(board.getStatusBoard().getCurrentChoicePlayerId(), choiceOption));
    }

    @Override
    public void applyChoice(Board board, Player currentPlayer, ChoiceMade choiceMade) {
        //Si le joueur courrant est la cible alors la cible a été choisi
        if(currentPlayer.getId() == board.getStatusBoard().getCurrentChoicePlayerId()){
            ArrayList<String> choiceOption = new ArrayList<>();
            finalSelectedPlayer = targetPlayer.get(choiceMade.getIndexChoose());
            board.setMsg( currentPlayer.getName() + " a choisi de cibler " + finalSelectedPlayer.getName() + ".");
            board.getStatusBoard().setCurrentChoicePlayerId(finalSelectedPlayer.getId());

            choiceOption.add("Donner 5 po");
            for (Card c : targetPlayer.get(choiceMade.getIndexChoose()).getHand()){
                if(c.getStrength() > board.getStatusBoard().getCardActivated().getStrength() && c.isGood()){
                    listChoice.add(c);
                    choiceOption.add(c.toString());
                }
            }

            board.getServer().choiceOption(board.getStatusBoard().getCurrentChoicePlayerId(),
                    new JsonCreator().choiceOption(board.getStatusBoard().getCurrentChoicePlayerId(), choiceOption));
        //autrement c'est l'autre joueur potentielle qui a choisi
        } else {
            board.setMsg( finalSelectedPlayer.getName() + " a choisi de donner 5 po à " + currentPlayer.getName() + ".");
            if(choiceMade.getIndexChoose() == 0){
                finalSelectedPlayer.subCoin(GOLD_STEAL);
                currentPlayer.addCoin(GOLD_STEAL);
            } else {
                board.setMsg( finalSelectedPlayer.getName() + " a choisi de donner " + listChoice.get(choiceMade.getIndexChoose()-1) + " à " + currentPlayer.getName() + ".");
                for (int pos = 0; pos < finalSelectedPlayer.getHand().size(); ++pos) {
                    if (finalSelectedPlayer.getHand().get(pos).getIdCard() == listChoice.get(choiceMade.getIndexChoose()-1).getIdCard()) {
                        board.moveCard(finalSelectedPlayer.getHand(), currentPlayer.getHand(), pos);
                    }
                }
            }

            board.getStatusBoard().setCurrentChoicePlayerId(-1);
            board.setStatusBoard(new WaitingPlay(board.getStatusBoard()));
        }

    }

    @Override
    public String toString(){
        return "Le joueur adverse possedant le vol le plus \n" +
                "puissant choisit de vous donner de sa main \n" +
                "un dragon bon plus fort que celui-ci \n" +
                "(Montrez-le aux autres joueurs) ou de \n" +
                "vous payez 5 pieces d'or.";
    }
}
