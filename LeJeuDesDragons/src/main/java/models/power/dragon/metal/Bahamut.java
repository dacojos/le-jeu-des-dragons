package models.power.dragon.metal;

import models.board.Board;
import models.card.Card;
import models.event.choice.ChoiceMade;
import models.player.Player;
import models.power.Power;

/**
 * @brief   : Class implémentant le pouvoir de Bahamut : Prend 10 pièce d'or à chaque joueur possédant un dragon mauvais
 * et un dragon bon
 * @Date    : 28.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Bahamut implements Power {
    private final int GOLD_STEAL = 10;

    @Override
    public void activatePower(Board board, Player currentPlayer) {

        for(Player p : board.getPlayers()){
            boolean hasGood = false;
            boolean hasBad = false;

            if(currentPlayer.getId() != p.getId()) {
                for (Card c : p.getFlight()) {
                    if (c.isGood()) {
                        hasGood = true;
                    }

                    if (c.isBad()) {
                        hasBad = true;
                    }
                }

                if (hasBad && hasGood) {
                    currentPlayer.addCoin(GOLD_STEAL);
                    p.subCoin(GOLD_STEAL);
                }
            }
        }
    }

    @Override
    public void applyChoice(Board board, Player currentPlayer, ChoiceMade choiceMade) {

    }

    @Override
    public String toString(){
        return "Les joueurs possedant a la fois des dragons bons et \n" +
                "mauvais dans leurs vol vous paye 10 pieces d'or.\n" +
                "Dragon Divin - Tant que vous avez Bahamut \n" +
                "est un dragon mauvais dans votre vol, vous \n" +
                "ne pouvez pas gagner la partie.";
    }
}