package models.power.mortal;

import models.board.Board;
import models.card.mortal.Mortal;
import models.event.choice.ChoiceMade;
import models.player.Player;
import models.power.Power;

/**
 * @brief   : Class implémentant le pouvoir du prêtre : Le joueur est forcément le leader au prochain tour
 * @Date    : 28.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Priest implements Power {
    @Override
    public void activatePower(Board board, Player currentPlayer) {
        currentPlayer.subCoin(Mortal.PRICE_TO_PAY);
        board.addBet(Mortal.PRICE_TO_PAY);
        board.setIdPlayerPriest(currentPlayer.getId());
    }

    @Override
    public void applyChoice(Board board, Player currentPlayer, ChoiceMade choiceMade) {

    }

    @Override
    public String toString(){
        return "Payez 1 piece d'or à la mise.\n" +
                "Vous ete automatiquement le leader \n" +
                "pour le prochain tour de cette partie.";
    }
}
