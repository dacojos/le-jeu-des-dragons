package models.power.mortal;

import models.board.Board;
import models.card.mortal.Mortal;
import models.event.choice.ChoiceMade;
import models.player.Player;
import models.power.Power;

/**
 * @brief   : Class implémentant le pouvoir de l'idiot : le joueur donne 1 pièces d'or à la mise et pioche
 * 1 carte pour chaque joueur ayant un vol plus puissant que le sien.
 * @Date    : 28.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Fool implements Power {
    @Override
    public void activatePower(Board board, Player currentPlayer) {
        currentPlayer.subCoin(Mortal.PRICE_TO_PAY);
        board.addBet(Mortal.PRICE_TO_PAY);

        int nbrCardToDraw = 0;

        for(Player p : board.getPlayers()){
            if(currentPlayer.getFlightStrength() < p.getFlightStrength()){
                nbrCardToDraw++;
            }
        }

        board.draw(currentPlayer, nbrCardToDraw);
    }

    @Override
    public void applyChoice(Board board, Player currentPlayer, ChoiceMade choiceMade) {

    }

    @Override
    public String toString(){
        return "Payez 1 piece d'or à la mise.\n" +
                "Piochez une carte pour chaque joueur \n" +
                "qui possede un vol plus puissant que \n" +
                "le votre.";
    }
}
