package models.power.mortal;

import models.board.Board;
import models.card.mortal.Mortal;
import models.event.choice.ChoiceMade;
import models.jsonHandler.JsonCreator;
import models.player.Player;
import models.power.Power;
import models.statusBoard.Choice;
import models.statusBoard.WaitingPlay;

import java.util.ArrayList;

/**
 * @brief   : Class implémentant le pouvoir de l'archimage : Le joueur paye 1 pèce d'or et choisie une carte dans
 * l'anté et active son pouvoir
 * @Date    : 28.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Archmage implements Power {

    @Override
    public void activatePower(Board board, Player currentPlayer) {
        currentPlayer.subCoin(Mortal.PRICE_TO_PAY);
        board.addBet(Mortal.PRICE_TO_PAY);

        if (!board.getAnte().isEmpty()) {
            board.setStatusBoard(new Choice(board.getStatusBoard()));
            board.getStatusBoard().setCurrentChoicePlayerId(currentPlayer.getId());

            ArrayList<String> choiceOption = new ArrayList<>();
            for(int i = 0; i < board.getAnte().size(); ++i){
                choiceOption.add(board.getAnte().get(i).getName() + ": " + board.getAnte().get(i).getPower().toString());
            }
            board.getServer().choiceOption(board.getStatusBoard().getCurrentChoicePlayerId(),
                    new JsonCreator().choiceOption(board.getStatusBoard().getCurrentChoicePlayerId(), choiceOption));
        }
    }

    @Override
    public void applyChoice(Board board, Player currentPlayer, ChoiceMade choiceMade) {
        board.getStatusBoard().setCurrentChoicePlayerId(-1);
        board.setStatusBoard(new WaitingPlay(board.getStatusBoard()));
        board.setMsg( currentPlayer.getName() + " a choisi d' utiliser le pouvoir suivant : " + board.getAnte().get(choiceMade.getIndexChoose()) + "\n");

        board.getStatusBoard().setCardActivated(board.getAnte().get(choiceMade.getIndexChoose()));
        board.getAnte().get(choiceMade.getIndexChoose()).activatePower(board, currentPlayer);
    }

    @Override
    public String toString(){
        return "Payez 1 piece d'or à la mise.\n"+
                "Copiez le pouvoir d'une carte d'ante.";
    }

}
