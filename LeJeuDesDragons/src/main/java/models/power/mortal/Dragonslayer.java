package models.power.mortal;

import javafx.util.Pair;
import models.board.Board;
import models.card.Card;
import models.card.mortal.Mortal;
import models.event.choice.ChoiceMade;
import models.jsonHandler.JsonCreator;
import models.player.Player;
import models.power.Power;
import models.statusBoard.Choice;
import models.statusBoard.WaitingPlay;

import java.util.ArrayList;

/**
 * @brief   : Class implémentant le pouvoir du tueur de dragon : Le joueur choisie un dragon du vol de son choix
 * et tue le dragon d'une force inférieur au tueur de dragon
 * @Date    : 28.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Dragonslayer implements Power {
    private ArrayList<Pair<Integer, Card>> listOfDragon = new ArrayList<>();

    @Override
    public void activatePower(Board board, Player currentPlayer) {
        listOfDragon = new ArrayList<>();

        currentPlayer.subCoin(Mortal.PRICE_TO_PAY);
        board.addBet(Mortal.PRICE_TO_PAY);

        for(Player p : board.getPlayers()){
            for (Card c : p.getFlight()){
                if(!c.isMortal() && c.getStrength() < board.getCardPlayed().get(currentPlayer.getId()).getStrength()) {
                    listOfDragon.add(new Pair<>(p.getId(), c));
                }
            }
        }

        if(!listOfDragon.isEmpty()) {
            board.setStatusBoard(new Choice(board.getStatusBoard()));
            board.getStatusBoard().setCurrentChoicePlayerId(currentPlayer.getId());

            ArrayList<String> choiceOption = new ArrayList<>();

            for (int i = 0; i < listOfDragon.size(); ++i) {
                choiceOption.add(listOfDragon.get(i).getValue().toString());
            }

            board.getServer().choiceOption(board.getStatusBoard().getCurrentChoicePlayerId(),
                                        new JsonCreator().choiceOption(board.getStatusBoard().getCurrentChoicePlayerId(), choiceOption));
        }


    }

    @Override
    public void applyChoice(Board board, Player currentPlayer, ChoiceMade choiceMade) {
        board.setMsg( currentPlayer.getName() + " a choisi de tuer " + listOfDragon.get(choiceMade.getIndexChoose()) + "\n");
        Player p = board.getPlayers().get(listOfDragon.get(choiceMade.getIndexChoose()).getKey());
        int posCard = -1;

        for(int i = 0; i < p.getFlight().size(); ++i){
            if (p.getFlight().get(i).getIdCard() == listOfDragon.get(choiceMade.getIndexChoose()).getValue().getIdCard()) {
                posCard = i;
                break;
            }
        }

        if(posCard != -1) {
            if(board.getCardPlayed().get(p.getId()) != null){
                board.getCardPlayed().set(p.getId(), null);
            }
            board.moveCard(p.getFlight(), board.getDiscard(), posCard);
        }

        board.getStatusBoard().setCurrentChoicePlayerId(-1);
        board.setStatusBoard(new WaitingPlay(board.getStatusBoard()));
    }

    @Override
    public String toString(){
        return "Payez 1 piece d'or à la mise.\n" +
                "Defaussez du vol de votre choix un \n" +
                "dragon plus faible au tueur de dragon.";
    }
}
