package models.power.mortal;

import models.board.Board;
import models.event.choice.ChoiceMade;
import models.jsonHandler.JsonCreator;
import models.player.Player;
import models.power.Power;
import models.statusBoard.Choice;
import models.statusBoard.WaitingPlay;

import java.util.ArrayList;

/**
 * @brief   : Class implémentant le pouvoir du voleur : Prend 7 pièces d'or à la mise et doit se défausser d'une cartes
 * @Date    : 28.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Thief implements Power {
    private final int GOLD_STEAL = 7;

    @Override
    public void activatePower(Board board, Player currentPlayer) {
        board.setStatusBoard(new Choice(board.getStatusBoard()));
        board.getStatusBoard().setCurrentChoicePlayerId(currentPlayer.getId());

        int coin = board.trySub(GOLD_STEAL);

        currentPlayer.addCoin(coin);
        board.subBet(coin);

        ArrayList<String> choiceOption = new ArrayList<>();

        for (int i = 0; i < currentPlayer.getSizeHand(); ++i){
            choiceOption.add(currentPlayer.getHand().get(i).toString());
        }

        board.getServer().choiceOption(currentPlayer.getId(), new JsonCreator().choiceOption(currentPlayer.getId(), choiceOption));
    }

    @Override
    public void applyChoice(Board board, Player currentPlayer, ChoiceMade choiceMade) {
        board.setMsg( currentPlayer.getName() + " a choisi de défausser " + currentPlayer.getHand().get(choiceMade.getIndexChoose()) + "\n");

        board.moveCard(currentPlayer.getHand(), board.getDiscard(), choiceMade.getIndexChoose());

        board.getStatusBoard().setCurrentChoicePlayerId(-1);
        board.setStatusBoard(new WaitingPlay(board.getStatusBoard()));
    }

    @Override
    public String toString(){
        return "Volez 7 piece d'or a la mise.\n"+
                "Choisissez une carte de votre main et \n" +
                "defaussez la.";
    }
}
