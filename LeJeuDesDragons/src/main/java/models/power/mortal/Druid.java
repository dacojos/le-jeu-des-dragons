package models.power.mortal;

import models.board.Board;
import models.card.mortal.Mortal;
import models.event.choice.ChoiceMade;
import models.player.Player;
import models.power.Power;

/**
 * @brief   : Class implémentant le pouvoir du druide : inverse la règle du vainqueurs, c'est le vol le moin puissant
 * qui gagne
 * @Date    : 28.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Druid implements Power {


    @Override
    public void activatePower(Board board, Player currentPlayer) {
        currentPlayer.subCoin(Mortal.PRICE_TO_PAY);
        board.addBet(Mortal.PRICE_TO_PAY);
        board.setDruid(true);
    }

    @Override
    public void applyChoice(Board board, Player currentPlayer, ChoiceMade choiceMade) {

    }

    @Override
    public String toString(){
        return "Payez 1 piece d'or à la mise.\n"+
                "Le joueur avec le vol le plus faible \n" +
                "gagne la partie au lieu du joueur avec \n" +
                "le vol le plus fort.";
    }
}
