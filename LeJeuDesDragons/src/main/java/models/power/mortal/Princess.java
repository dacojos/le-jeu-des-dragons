package models.power.mortal;

import models.board.Board;
import models.card.Card;
import models.card.mortal.Mortal;
import models.event.choice.ChoiceMade;
import models.jsonHandler.JsonCreator;
import models.player.Player;
import models.power.Power;
import models.statusBoard.Choice;
import models.statusBoard.StatusId;
import models.statusBoard.WaitingPlay;

import java.util.ArrayList;

/**
 * @brief   : Class implémentant le pouvoir de la princesse : Ractive les pouvoir des dragons bons dans l'ordre de
 * choix du joueur
 * @Date    : 28.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Princess implements Power {
    private ArrayList<Card> listOfDragon;

    @Override
    public void activatePower(Board board, Player currentPlayer) {
        listOfDragon = new ArrayList<>();

        currentPlayer.subCoin(Mortal.PRICE_TO_PAY);
        board.addBet(Mortal.PRICE_TO_PAY);

        for(Card c : currentPlayer.getFlight()){
            if(c.isGood()){
                listOfDragon.add(c);
            }
        }

        if(listOfDragon.size() != 0){
            board.getStatusBoard().setPrincess(true);
            board.setStatusBoard( new Choice(board.getStatusBoard()));
            board.getServer().choiceOption(currentPlayer.getId(), new JsonCreator().choiceOption(currentPlayer.getId(), choiceOptionGenerator()));
        }
    }

    @Override
    public void applyChoice(Board board, Player currentPlayer, ChoiceMade choiceMade){
        board.setMsg( currentPlayer.getName() + " a choisi de réactiver " + listOfDragon.get(choiceMade.getIndexChoose()) + "\n");
        board.getStatusBoard().setCurrentChoicePlayerId(-1);
        board.setStatusBoard(new WaitingPlay(board.getStatusBoard()));

        board.getStatusBoard().setCardActivated(listOfDragon.get(choiceMade.getIndexChoose()));
        listOfDragon.remove(choiceMade.getIndexChoose()).activatePower(board, currentPlayer);

        tryReActivatePower(board, currentPlayer);
    }

    /**
     * @brief               : Tente de réactiver les pouvoirs de la liste de la princess
     * @param board         : Board du plateau de jeu
     * @param currentPlayer : Player ayant activé le pouvoir
     */
    public void tryReActivatePower(Board board, Player currentPlayer){
        if (listOfDragon.size() != 0 && board.getStatusBoard().getStateId() != StatusId.CHOICE) {

            board.getServer().choiceOption(currentPlayer.getId(), new JsonCreator().choiceOption(currentPlayer.getId(), choiceOptionGenerator()));
        } else if(listOfDragon.size() == 0){
            board.getStatusBoard().setPrincess(false);
        }
    }

    /**
     * @brief   : Génère la liste des choix
     * @return  : ArrayList<String> des choix
     */
    private ArrayList<String> choiceOptionGenerator(){
        ArrayList<String> choiceOption = new ArrayList<>();

        for(Card c : listOfDragon){
            choiceOption.add(c.toString());
        }

        return choiceOption;
    }

    @Override
    public String toString(){
        return "Payez 1 piece d'or à la mise.\n" +
                "Le pouvoir de chacun des dragons bons \n" +
                "de votre vol se déclenche dans l'ordre \n" +
                "de votre choix.";
    }
}
