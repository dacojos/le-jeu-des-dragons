package models.player;

import models.card.Card;

import java.util.ArrayList;

/**
 * @brief   : Classe gérant le joueur avec sa main et les cartes qu'il a joué (vol)
 * @Date    : 21.10.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Player {
    private ArrayList<Card> hand;
    private ArrayList<Card> flight;

    private int coin;
    private String name;
    private int id;

    public Player(int coin, String name){
        this.coin = coin;
        this.name = name;

        hand = new ArrayList<>();
        flight = new ArrayList<>();
    }

    public Player(int coin, String name, int id){
        this(coin, name);
        this.id = id;
    }

    public Player(int coin, String name, int id, ArrayList<Card> hand){
        this(coin, name, id);

        if(hand.size() <= 10) {
            this.hand = new ArrayList<>(hand);
        } else {
            for(int i = 0; i < 10; i++){
                this.hand.add(hand.get(i));
            }
        }
    }

    public Player(int coin, String name, int id, Card... hand){
        this(coin, name, id);
        if (hand.length <= 10) {
            for(int i = 0; i < hand.length; i++){
                this.hand.add(hand[i]);
            }
        } else {
            for(int i = 0; i < 10; ++i){
                this.hand.add(hand[i]);
            }
        }
    }

    private Player(int coin, String name, int id, ArrayList<Card> hand, ArrayList<Card> flight){
        this(coin, name, id, hand);
        this.flight = new ArrayList<>(flight);
    }

    public void setCoin(int coin) {
        this.coin = coin;
    }

    public int getCoin() {
        return coin;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setHand(ArrayList<Card> hand) {
        this.hand = hand;
    }

    public ArrayList<Card> getHand() {
        return hand;
    }

    public void setFlight(ArrayList<Card> flight) {
        this.flight = flight;
    }

    public ArrayList<Card> getFlight() {
        return flight;
    }

    public void addCardHand(Card card){
        hand.add(card);
    }

    public void addCardFlight(Card card){
        flight.add(card);
    }

    public boolean removeCardHand(Card card){
        return hand.remove(card);
    }

    public Card removeCardHand(int pos){
        return hand.remove(pos);
    }

    public boolean removeCardFlight(Card card){
        return flight.remove(card);
    }

    public Card removeCardFlight(int pos){
        return flight.remove(pos);
    }

    public int getSizeHand(){
        return hand.size();
    }

    public void addCoin(int nbr){
        coin += nbr;
    }

    public void subCoin(int nbr){
        coin -= nbr;
    }

    public int getSizeFlight(){
        return flight.size();
    }

    /**
     * @brief   : Calcule la force du vol du joueur
     * @return  : int de la valeur de la force
     */
    public int getFlightStrength() {
        int somme = 0;
        for (Card c: flight){
            somme += c.getStrength();
        }

        return somme;
    }
}
