package models.player;

import models.card.Card;

import java.util.ArrayList;

/**
 * @brief   : Classe représentant les autres joueurs indiquant les infos minimum d'affichage
 * @Date    : 21.10.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class PlayerClient {
    private int coin;
    private String name;
    private int id;

    private int handSize = 0;
    private ArrayList<Card> flight = new ArrayList<>();

    public PlayerClient(int coin, String name, int id){
        this.coin = coin;
        this.name = name;
        this.id = id;
    }

    public PlayerClient(int coin, String name, int id, int handSize){
        this(coin, name, id);
        this.handSize = handSize;
    }

    public PlayerClient(int coin, String name, int id, int handSize, ArrayList<Card> flight){
        this(coin, name, id, handSize);
        this.flight = new ArrayList<>(flight);
    }

    public PlayerClient(PlayerClient p){
        this.coin = p.getCoin();
        this.name = p.getName();
        this.id = p.getId();

        this.handSize = p.getHandSize();
        this.flight = new ArrayList<>(p.getFlight());
    }

    public void setCoin(int coin) {
        this.coin = coin;
    }

    public int getCoin() {
        return coin;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setHandSize(int handSize) {
        this.handSize = handSize;
    }

    public int getHandSize() {
        return handSize;
    }

    public void setFlight(ArrayList<Card> flight) {
        this.flight = flight;
    }

    public ArrayList<Card> getFlight() {
        return flight;
    }

    /**
     * @brief   : Calcule la force du vol du joueur
     * @return  : int de la valeur de la force
     */
    public int getFlightStrength() {
        int somme = 0;
        for (Card c: flight){
            somme += c.getStrength();
        }

        return somme;
    }
}
