package models.statusBoard;

import models.board.Board;

/**
 * @brief   : Classe enfant de statusBoard et permet à un joueur de jouer alors que ce n'est normalement pas à lui
 * @Date    : 22.10.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Choice extends StatusBoard {
    private int currentChoicePlayerId;

    public Choice(Board board) {
        super(board);
        this.stateId = StatusId.CHOICE;
    }

    public Choice(StatusBoard oldStatus) {
        super(oldStatus);
        this.stateId = StatusId.CHOICE;
    }

    @Override
    public int getCurrentChoicePlayerId() {
        return currentChoicePlayerId;
    }

    @Override
    public void setCurrentChoicePlayerId(int currentChoicePlayerId) {
        this.currentChoicePlayerId = currentChoicePlayerId;
    }
}
