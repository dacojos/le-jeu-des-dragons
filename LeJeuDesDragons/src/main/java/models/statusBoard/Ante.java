package models.statusBoard;

import models.board.Board;
import models.card.Card;
import models.player.Player;

/**
 * @brief   : Classe enfant de statusBoard est le status où les joueurs choisisse l'ante et place l'argent
 * @Date    : 22.10.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Ante extends StatusBoard {
    public Ante(Board board) {
        super(board);
        stateId = StatusId.ANTE;
        currentPlayerId = -1;
    }

    public Ante(StatusBoard oldStatus) {
        super(oldStatus);
        stateId = StatusId.ANTE;
        currentPlayerId = -1;
    }

    /**
     * @brief   : Regarde si tous le monde à jouer une carte pour l'Ante
     */
    public void tryStartPlay(){
        for(Card c : board.getAnte()){
            if(c == null){
                return;
            }
        }
        tryInitPlay();
    }

    /**
     * @brief   : Regarde si la partie peu commencer en indiquand le leader
     */
    private void tryInitPlay(){
        int idLead = searchLeader(board.getAnte());

        if(idLead != -1){
            currentPlayerId = idLead;
            int strength = 0;
            for(Card c : board.getAnte()){
                if(c.getStrength() > strength){
                    strength = c.getStrength();
                }
            }

            for(Player p : board.getPlayers()){
                board.addBet(strength);
                p.subCoin(strength);
            }

            board.setStatusBoard(new WaitingPlay(this));
        } else {
            board.cleanBoard();
            for(Player p : board.getPlayers()){
                board.draw(p, 1);
            }
        }
    }

}
