package models.statusBoard;

public enum StatusId {
    WAITING_PLAYER(0),
    ANTE(1),
    WAITING_PLAY(2),
    CHOICE(3),
    END_GAME(4);

    private int id;

    StatusId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
