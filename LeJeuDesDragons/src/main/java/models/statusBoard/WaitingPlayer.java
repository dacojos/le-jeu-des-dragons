package models.statusBoard;

import models.board.Board;

/**
 * @brief   : Classe enfant de statusBoard représente le status ou l'host attend que les clients
 * viennent et lance la partie quand tous est bon.
 * @Date    : 22.10.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class WaitingPlayer extends StatusBoard {

    public WaitingPlayer(Board board) {
        super(board);
        stateId = StatusId.WAITING_PLAYER;
    }

    public WaitingPlayer(StatusBoard oldStatus) {
        super(oldStatus);
        stateId = StatusId.WAITING_PLAYER;
    }

    /**
     * @brief   : Fonction faisant changer d'état la Board en la passant à WaitingPlay.
     */
    @Override
    public void startGame() {
        board.setStatusBoard(new Ante(this));
        board.start();
    }
}
