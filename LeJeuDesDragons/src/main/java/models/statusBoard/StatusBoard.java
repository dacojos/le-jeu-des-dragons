package models.statusBoard;

import models.board.Board;
import models.card.Card;

import java.util.ArrayList;

/**
 * @brief   : Classe abstraite pour les status de la board
 * @Date    : 22.10.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public abstract class StatusBoard {
    protected Board board;
    protected int currentPlayerId;
    protected int nbrRound;
    protected int nbrTurn;
    protected StatusId stateId;
    protected Card cardActivated;
    protected boolean isPrincess;

    protected StatusBoard(Board board){
        this.board = board;
        currentPlayerId = 0;
        nbrRound = 0;
        nbrTurn = 0;
    }

    protected StatusBoard(StatusBoard oldStatus){
        this.board = oldStatus.board;
        this.currentPlayerId = oldStatus.currentPlayerId;
        this.nbrRound = oldStatus.nbrRound;
        this.nbrTurn = oldStatus.nbrTurn;
        this.cardActivated = oldStatus.cardActivated;
        this.isPrincess = oldStatus.isPrincess;
    }

    public void setCurrentPlayerId(int currentPlayerId) {
        this.currentPlayerId = currentPlayerId;
    }

    public int getCurrentPlayerId() {
        return currentPlayerId;
    }

    public void setNbrTurn(int nbrTurn) {
        this.nbrTurn = nbrTurn;
    }

    public Card getCardActivated() {
        return cardActivated;
    }

    public void setCardActivated(Card cardActivated) {
        this.cardActivated = cardActivated;
    }

    public void setNbrRound(int nbrRound) {
        this.nbrRound = nbrRound;
    }

    public int getNbrRound() {
        return nbrRound;
    }

    public StatusId getStateId() {
        return stateId;
    }

    public void setStateId(StatusId stateId) {
        this.stateId = stateId;
    }

    public int getCurrentChoicePlayerId() {
        return -1;
    }

    public void setCurrentChoicePlayerId(int currentChoicePlayerId) {}

    public boolean isPrincess() {
        return isPrincess;
    }

    public void setPrincess(boolean princess) {
        isPrincess = princess;
    }

    //Redéfini dans WaitingPlayer
    public void startGame(){}

    //Redéfini dans WaitingPlay
    public boolean playCard(int playerID){
        return false;
    }

    //Redéfinie WaitingPlay
    public void tryEndRound(){}

    //Refédinie dans Ante
    public void tryStartPlay(){}

    //Redéfini dans EndGame
    public void addVote(int playerId, boolean vote){}

    /**
     * @brief       : Recherche le leader sans égalité dans une liste de carte
     * @param list  : ArrayList des cartes pour rechercher le leader
     * @return      : int de l'id du nouveau leader, retourne -1 si pas trouvé
     */
    public int searchLeader(ArrayList<Card> list){
        int[] strMax = new int[Card.MAX_STRENGTH];
        for(Card c : list){
            if(c != null) {
                strMax[c.getStrength() - 1]++;
            }
        }

        for(int i = Card.MAX_STRENGTH-1; i > 0; --i){
            if(strMax[i] == 1){
                for(int j = 0; j < list.size(); ++j){
                    if(list.get(j).getStrength() == i+1){
                        return j;
                    }
                }
            }
        }
        return -1;
    }
}