package models.statusBoard;

import models.board.Board;
import models.card.Card;
import models.player.Player;

import java.util.ArrayList;
import java.util.Collections;


/**
 * @brief   : Classe enfant de statusBoard c'est le status ou les joueurs joue leurs cartes
 * @Date    : 22.10.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class WaitingPlay extends StatusBoard {
    public WaitingPlay(Board board) {
        super(board);
        this.stateId = StatusId.WAITING_PLAY;
    }

    public WaitingPlay(StatusBoard oldStatus) {
        super(oldStatus);
        this.stateId = StatusId.WAITING_PLAY;
    }

    /**
     * @brief           : Vérifie si le joueur peut joueur la carte si oui passe la main au suivant
     * @param playerID  : int de l'id du joueur voulant jouer
     * @return          : boolean si le joueur peut joueur.
     *              true: le joueur peut jouer
     *              false: le joueur ne peut pas jouer
     */
    @Override
    public boolean playCard(int playerID){
        if(playerID == currentPlayerId){
            nbrTurn++;
            return true;
        }
        return false;
    }

    /**
     * @brief   : test s'il peut mettre fin au round si oui il met fin au round et
     * calcule le prochain leader
     */
    public void tryEndRound(){
        currentPlayerId = (currentPlayerId + 1) % board.getNumberPlayer();
        if (nbrTurn == board.getNumberPlayer() || board.getTotalBet() == 0){
            nbrRound++;
            nbrTurn = 0;

            int idLead = board.getIdPlayerPriest() != -1 ? board.getIdPlayerPriest() : searchLeader(board.getCardPlayed());
            if(idLead != -1){
                currentPlayerId = idLead;
            }
            board.setIdPlayerPriest(-1);
            board.setCardPlayed(new ArrayList<>(Collections.nCopies(board.getNumberPlayer(), null)));
            tryEndBet();
        }

    }

    /**
     * @brief   : génère la liste des candidat possible pour connaître le gagant d'une fin de mise
     * @return  : ArrayList des joueurs pouvoir être choisie comme gagnant
     */
    private ArrayList<Player> generateCandidat(){
        ArrayList<Player> candidat = new ArrayList<>();

        for(Player p : board.getPlayers()){
            boolean hasTiamat = false;
            boolean hasBahamut = false;
            boolean hasGood = false;
            boolean hasBad = false;

            for(Card c : p.getFlight()){
                if(c.getIdCard() == 1){
                    hasBahamut = true;
                } else if(c.getIdCard() == 67){
                    hasTiamat = true;
                }

                if(c.isBad()){
                    hasBad = true;
                } else if(c.isGood()){
                    hasGood = true;
                }
            }

            if(!hasBahamut && !hasTiamat){
                candidat.add(p);
            } else if(hasBahamut && !hasBad){
                candidat.add(p);
            } else if (hasTiamat && !hasGood) {
                candidat.add(p);
            }
        }

        return candidat;
    }

    /**
     * @brief   : Vérifie si c'est la fin de mise si oui redémarre une nouvelle mise (Statut Ante)
     */
    private void tryEndBet(){
        if(nbrRound >= board.NBR_ROUND_MIN || board.getTotalBet() == 0){
            ArrayList<Player> candidat = generateCandidat();
            Player winner = null;
            boolean hasEquality = false;

            if(!candidat.isEmpty()) {
                winner = candidat.get(0);
                Player player = null;
                for (int i = 1; i < candidat.size(); ++i) {
                    player = candidat.get(i);

                    if (!board.isDruid() && winner.getFlightStrength() < player.getFlightStrength()) {
                        winner = player;
                        hasEquality = false;
                    } else if (board.isDruid() && winner.getFlightStrength() > player.getFlightStrength()) {
                        winner = player;
                        hasEquality = false;
                    } else if (winner.getFlightStrength() == player.getFlightStrength()) {
                        hasEquality = true;
                    }
                }

                if(!hasEquality || board.getTotalBet() == 0){
                    winner.addCoin( board.getTotalBet());

                    board.setTotalBet(0);
                    board.cleanBoard();
                    nbrRound = 0;
                    board.setDruid(false);
                    tryEndGame();
                }
            }
        }
    }

    /**
     * @brief   : met fin à la partie si un joueur n'a plus de pièce ou relance une nouvelle mise
     */
    private void tryEndGame(){
        //si un joueur n'a plus de pièce on termine
        for(Player p : board.getPlayers()){
            if(p.getCoin() <= 0){
                board.setStatusBoard( new EndGame(this));
                return;
            }
        }
        //sinon on prépare la prochaine mise
        for(Player p : board.getPlayers()){
            board.draw(p, 2);
        }
        board.setStatusBoard( new Ante(this));
    }

}
