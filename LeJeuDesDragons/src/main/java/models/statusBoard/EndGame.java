package models.statusBoard;

import models.board.Board;
import models.jsonHandler.JsonCreator;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @brief   : Classe enfant de statusBoard et c'est le status quand la partie est fini. Ils peuvent
 * choisir de restart ou de quitter
 * @Date    : 22.10.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class EndGame extends StatusBoard {
    private ArrayList<Boolean> voteRestart;
    private boolean end = false;

    public EndGame(Board board) {
        super(board);
        this.stateId = StatusId.END_GAME;
        this.voteRestart = new ArrayList<>(Collections.nCopies(board.getNumberPlayer(), null));
    }

    public EndGame(StatusBoard oldStatus) {
        super(oldStatus);
        this.stateId = StatusId.END_GAME;
        this.voteRestart = new ArrayList<>(Collections.nCopies(board.getNumberPlayer(), null));
    }

    /**
     * @brief           : Ajoute un vote pour savoir si on restart
     * @param playerId  : int de l'id du joueur ayant voté
     * @param vote      : boolean du choix du joueur.
     *                  true : veut restart
     *                  false: veut quitter
     */
    public void addVote(int playerId, boolean vote){
        this.voteRestart.set(playerId, vote);
        tryRestart();
    }

    /**
     * @brief   : Vérifie si on doit encore attendre des votes, restart la partie ou bien dire à tous de quitter
     */
    private void tryRestart(){
        for(int i = 0; i < board.getNumberPlayer(); ++i){
            if(voteRestart.get(i) == null) {
                return;
            }
            if(end == false && voteRestart.get(i) == false){
                end = true;
            }
        }

        if(end == true) {
            board.getServer().quitGame(new JsonCreator().quitGame(end, false));
        } else {
            board.setStatusBoard(new Ante(this));
            board.restart();
        }
    }
}
