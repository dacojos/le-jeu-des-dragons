package models.board;

import communication.server.ServerInterface;
import models.card.dragon.Color;
import models.card.dragon.Metal;
import models.event.*;

import models.card.Card;
import game.Game;
import models.event.choice.*;
import models.jsonHandler.JsonCreator;
import models.player.Player;
import models.power.mortal.Princess;
import models.statusBoard.*;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @brief : Classe permettant de jouer au jeu des dragons. C'est elle qui gère la totalité du jeu
 * comme la pile, les cartes joué, etc... mais aussi qui décide quel joueur joue et quels actions
 * ils peuvent entreprendre
 * @Date : 21.10.2019
 * @Author : Bouyiatiotis Stéphane
 */
public class Board {
    private int totalBet;

    private ArrayList<Card> deck;
    private ArrayList<Card> cardPlayed;
    private ArrayList<Card> discard;
    private ArrayList<Card> ante;
    private ArrayList<Player> players;

    private String msg;
    private boolean isDruid = false;
    private boolean hasChoice = false;
    private int idPlayerPriest = -1;
    private ArrayList<Boolean> hasBrelan;

    private ArrayList<Boolean> hasColor;

    private StatusBoard statusBoard;
    private ServerInterface server;

    public static final int NBR_COIN_START = 50;
    public static final int NBR_PLAYER_MAX = 6;
    public static final int NBR_PLAYER_MIN = 2;
    public static final int NBR_ROUND_MIN = 3;
    public static final int NB_MAX_CARD_HAND = 10;

    public Board() {
        totalBet = 0;
        msg = "";
        this.deck = new ArrayList<>(Game.cardGameList);
        discard = new ArrayList<>();
        ante = new ArrayList<>();
        players = new ArrayList<>();

        statusBoard = new WaitingPlayer(this);
    }

    public String getMsg(){
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setTotalBet(int totalBet) {
        this.totalBet = totalBet;
    }

    public int getTotalBet() {
        return totalBet;
    }

    public void setAnte(ArrayList<Card> ante) {
        this.ante = ante;
    }

    public ArrayList<Card> getAnte() {
        return ante;
    }

    public void setDeck(ArrayList<Card> deck) {
        this.deck = deck;
    }

    public ArrayList<Card> getDeck() {
        return deck;
    }

    public void setCardPlayed(ArrayList<Card> cardPlayed) {
        this.cardPlayed = cardPlayed;
    }

    public ArrayList<Card> getCardPlayed() {
        return cardPlayed;
    }

    public void setDiscard(ArrayList<Card> discard) {
        this.discard = discard;
    }

    public ArrayList<Card> getDiscard() {
        return discard;
    }

    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public void addCardDeck(Card card) {
        deck.add(card);
    }

    public void addCardAnte(Card card) {
        ante.add(card);
    }

    public void addCardDiscard(Card card) {
        discard.add(card);
    }

    public void addCardPlayed(Card card) {
        cardPlayed.add(card);
    }

    public void addPlayerBoard(Player player) {
        players.add(player);
    }

    public void addPlayerBoard(String name) {
        players.add(new Player(NBR_COIN_START, name, players.size()));
    }

    public int getSizeDeck() {
        return deck.size();
    }

    public int getSizeDiscard() {
        return discard.size();
    }

    public int getSizeAnte() {
        return ante.size();
    }

    public int getSizeCardPlayed() {
        return cardPlayed.size();
    }

    public int getNumberPlayer() {
        return players.size();
    }

    public boolean removeCardDeck(Card card) {
        return deck.remove(card);
    }

    public boolean removeCardDiscard(Card card) {
        return discard.remove(card);
    }

    public boolean removeCardAnte(Card card) {
        return ante.remove(card);
    }

    public boolean removeCardCardPlayed(Card card) {
        return cardPlayed.remove(card);
    }

    public boolean removePlayer(Player player) {
        return players.remove(player);
    }

    public Card removeCardDeck(int card) {
        return deck.remove(card);
    }

    public Card removeCardDiscard(int card) {
        return discard.remove(card);
    }

    public Card removeCardAnte(int card) {
        return ante.remove(card);
    }

    public Card removeCardPlayed(int card) {
        return cardPlayed.remove(card);
    }

    public Player removePlayer(int player) {
        return players.remove(player);
    }

    public void setStatusBoard(StatusBoard statusBoard) {
        this.statusBoard = statusBoard;
    }

    public StatusBoard getStatusBoard() {
        return statusBoard;
    }

    public void addBet(int sum) {
        totalBet += sum;
    }

    public void subBet(int sum) {
        totalBet -= sum;
    }

    public ServerInterface getServer() {
        return server;
    }

    public void setServer(ServerInterface server) { this.server = server; }

    public ArrayList<Boolean> getHasBrelan() {
        return hasBrelan;
    }

    public void setHasBrelan(ArrayList<Boolean> hasBrelan) {
        this.hasBrelan = hasBrelan;
    }

    public ArrayList<Boolean> getHasColor() {
        return hasColor;
    }

    public void setHasColor(ArrayList<Boolean> hasColor) {
        this.hasColor = hasColor;
    }

    public boolean isDruid() {
        return isDruid;
    }

    public void setDruid(boolean druid) {
        isDruid = druid;
    }

    public int getIdPlayerPriest() {
        return idPlayerPriest;
    }

    public void setIdPlayerPriest(int idPlayerPriest) {
        this.idPlayerPriest = idPlayerPriest;
    }

    //------- Ici commence les méthodes de jeu ----------------------------------------------------------------

    /**
     * @brief : Mélange le deck et fait piocher 6 cartes à chaque joueur et initialise les listes
     */
    public void start() {
        Collections.shuffle(deck);

        cardPlayed = new ArrayList<>(Collections.nCopies(players.size(), null));
        ante = new ArrayList<>(Collections.nCopies(players.size(), null));
        hasBrelan = new ArrayList<>(Collections.nCopies(players.size(), false));
        hasColor = new ArrayList<>(Collections.nCopies(players.size(), false));

        for (Player p : players) {
            draw(p, 6);
        }
    }

    /**
     * @brief : Permet de faire un restart du jeu
     * Netoye le board, réinitialise la défausse, crée un nouveau deck et le mélange et fait piocher 6 cartes
     * à chaque joueur.
     */
    public void restart() {
        cleanBoard();

        discard = new ArrayList<>();
        deck = new ArrayList<>(Game.cardGameList);
        Collections.shuffle(deck);

        for (Player p : players) {
            p.setHand(new ArrayList<>());
            draw(p, 6);
            p.setCoin(NBR_COIN_START);
        }
        statusBoard = new Ante(statusBoard);
        msg = "choisissez votre carte d'anté";
        updatePlayers();
    }

    /**
     * @param player: Player devant piocher
     * @param nbr     : int du nombre de carte à piocher
     * @brief : Fait piocher a un joueur un certain nombre de carte
     */
    public void draw(Player player, int nbr) {

        if ((nbr + player.getSizeHand()) > NB_MAX_CARD_HAND) {
            nbr = 10 - player.getSizeHand();
        }

        for (int i = 0; i < nbr; ++i) {
            tryRefullDeck();

            if (deck.size() == 0)
                return;

            player.addCardHand(deck.remove(0));
        }
    }

    /**
     * @param coins : int du nombre de pièce que l'on veut retirer
     * @return : int du reste, retourne coins si l'ont peut retirer cette somme autrement retourne la mise
     * @brief : Regarde et indique si le nombre de pièce que l'on veux retirer de la mise est possible sinon
     * retourne retourne le reste.
     */
    public int trySub(int coins) {
        return (totalBet > coins ? coins : totalBet);
    }

    /**
     * @brief : Regarde si le deck est vide si oui le remplie avec la discard
     */
    private void tryRefullDeck() {
        if (deck.size() == 0) {
            deck = discard;
            discard = new ArrayList<>(70);
            Collections.shuffle(deck);
        }
    }
    /**

     * @brief : retire toutes les cartes sur le plateau pour les placer dans discard
     */
    public void cleanBoard() {

        for (Player p : players) {
            while (p.getSizeFlight() > 0) {
                moveCard(p.getFlight(), discard, 0);
            }
        }

        while (ante.size() > 0) {
            moveCard(ante, discard, 0);
        }

        cardPlayed = new ArrayList<>(Collections.nCopies(players.size(), null));
        ante = new ArrayList<>(Collections.nCopies(players.size(), null));
    }

    /**
     * @param player: Player dont on veut vérifier la main
     * @brief : Vérifie si la main du joueur est vide et si oui lui fait acheter des cartes
     */
    public void checkEmptyHand(Player player) {
        if (player.getSizeHand() == 0) {
            buyCard(player);
        }
    }

    /**
     * @param player: Player dont on veut acheter les cartes
     * @brief : Achète des cartes au joueur
     */
    public void buyCard(Player player) {
        tryRefullDeck();
        if (deck.size() != 0) {
            Card card = moveCard(deck, discard, 0);
            totalBet += card.getStrength();
            player.setCoin(player.getCoin() - card.getStrength());
            draw(player, 4 - player.getSizeHand());
        }

    }

    /**
     * @param from    : ArrayList d'origine de la carte
     * @param to      : ArrayList d'arrivé de la carte
     * @param posFrom : int de la position d'origine de la carte
     * @return : Card de la carte déplacé
     * @brief : Déplace une carte d'un point a vers un point b
     */
    public Card moveCard(ArrayList<Card> from, ArrayList<Card> to, int posFrom) {
        Card card = from.remove(posFrom);
        to.add(card);
        return card;
    }

    /**
     * @param player : Player du joueur jouant la carte
     * @param pos    : int de la position de la carte dans la main
     * @brief : Fait jouer une carte à un joueur en la plaçant dans son vol
     */
    private void playCard(Player player, int pos) {
        Card card = moveCard(player.getHand(), player.getFlight(), pos);
        cardPlayed.set(player.getId(), card);
    }

    /**
     * @brief : demande au serveur de mettre à jour les clients
     */
    private void updatePlayers() {
        //( server != null) {
        for (int id = 0; id < players.size(); id++) {
            server.updateBoardClient(id, new JsonCreator().updateBoardClient( msg,this, id));
        }
        //}
    }

    //------- Ici commence les méthodes de traitement d'évenement --------------------------------------------------

    /**
     * @param event : JoinGame de l'évènement
     * @return : boolean indiquant si on peut rejoindre la partie
     * @brief : Lecture de l'évènement JoinGame pour rajouter un joueur dans board
     */
    public boolean processEvent(JoinGame event) {
        if (players.size() == NBR_PLAYER_MAX || !(statusBoard.getStateId() == StatusId.WAITING_PLAYER)) {
            for (Player player : players) {
                if (player.getName().equals(event.getName())) {
                    return false;
                }
            }
            return false;
        }

        addPlayerBoard(event.getName());
        server.assignId(players.size() - 1, new JsonCreator().assignId(players.size() - 1));
        msg = event.getName() + " a rejoint la partie";
        updatePlayers();
        return true;
    }

    /**
     * @param event : StartGame de l'évènement
     * @return : boolean indiquant si la partie à commencé
     * @brief : Lecture de l'évènement StartGame faisont commencé la partie
     */
    public boolean processEvent(StartGame event) {
        if (players.size() < NBR_PLAYER_MIN || event.getIdPlayer() != 0 || !(statusBoard.getStateId() == StatusId.WAITING_PLAYER)) {
            return false;
        }

        statusBoard.startGame();
        msg = "choisissez votre carte d'anté";
        updatePlayers();
        return true;
    }

    /**
     * @param playerPlayCard: PLayerPlayCard de l'évènement
     * @return : boolean indiquant si la carte à pu être joué
     * @brief : Lecture de l'évènement quand un joueur joue une carte
     */
    public boolean processEvent(PlayerPlayCard playerPlayCard) {
        msg = "";
        //Si on est en phase Ante
        if (statusBoard.getStateId() == StatusId.ANTE) {
            Player player = players.get(playerPlayCard.getIdPlayer());

            //Gestion si un Ante a déjà été choisie
            if (ante.get(player.getId()) == null) {
                ante.set(player.getId(), player.getHand().get(playerPlayCard.getPosCard()));
                player.removeCardHand(playerPlayCard.getPosCard());
            }

            server.updateBoardClient( player.getId(), new JsonCreator().updateBoardClient( "Attendez ques les autres joueurs choisissent leur carte d'anté",this, player.getId()));
            statusBoard.tryStartPlay();

            if(statusBoard.getStateId() == StatusId.WAITING_PLAY){
                msg = "la mise est de " + totalBet/players.size() + " et c'est à " + players.get(statusBoard.getCurrentPlayerId()).getName() + " de jour";
            }
        } else {    //Si on est en WaitingPlay

            if (!statusBoard.playCard(playerPlayCard.getIdPlayer()) || !(statusBoard.getStateId() == StatusId.WAITING_PLAY)) {
                return false;
            }

            Player player = players.get(playerPlayCard.getIdPlayer());
            int posCard = playerPlayCard.getPosCard();
            Card card = player.getHand().get(posCard);

            playCard(player, posCard);
            msg = player.getName() + " a joué " + card.toString();
            updatePlayers();

            int idPrevPlayer = (player.getId() + players.size() - 1) % players.size();
            if (cardPlayed.get(idPrevPlayer) == null || cardPlayed.get(idPrevPlayer).getStrength() >= card.getStrength()) {
                statusBoard.setCardActivated(card);
                card.activatePower(this, player);
            }


            updatePlayers();

            tryEndPlayCard();

            if ( statusBoard.getStateId() == StatusId.CHOICE){
                msg = players.get(statusBoard.getCurrentChoicePlayerId()).getName() + " doit faire un choix";
            }
            else if  ( statusBoard.getStateId() == StatusId.ANTE){
                msg = "choisissez votre carte d'anté";
            } else if ( statusBoard.getStateId() == StatusId.END_GAME){
                msg = "Fin de partie";
            }

        }

        updatePlayers();

        return true;
    }

    /**
     * @param idPlayer : int de l'id du joueur qui doit choisire les cartes
     * @brief : Envoie au joueur le choix des cartes à prendre dans l'ante pour le brelant
     */
    private void sendBrelanOption(int idPlayer) {
        ArrayList<String> choix = new ArrayList<>();
        for (Card c : ante) {
            choix.add(c.toString());
        }

        server.choiceOption( idPlayer, new JsonCreator().choiceOption(idPlayer,choix));
    }

    /**
     * @brief : Regarde si le joueur peut réaliser un brelant si oui alors réalise l'effets du brelant
     */
    private void tryBrelan() {
        Player player = players.get(statusBoard.getCurrentPlayerId());
        int idPlayer = player.getId();

        if (!hasBrelan.get(idPlayer)) {
            int nbSameStrength = 0;
            int strengthPlayed = cardPlayed.get(idPlayer).getStrength();
            for (Card c : player.getFlight()) {
                if (c.getStrength() == strengthPlayed) {
                    nbSameStrength++;
                }
            }

            if (nbSameStrength >= 3) {
                msg += players.get(idPlayer).getName() + " a fait un brelan de force " + cardPlayed.get(idPlayer).getStrength() + ".";
                hasBrelan.set(idPlayer, true);
                //vol de pièce à la mise
                int coin = trySub(cardPlayed.get(idPlayer).getStrength());
                subBet(coin);
                player.addCoin(coin);

                //récupère les ante peut amener à un choix
                if (player.getSizeHand() + ante.size() <= NB_MAX_CARD_HAND) {
                    while (!ante.isEmpty()) {
                        moveCard(ante, player.getHand(), 0);
                    }
                } else {
                    hasChoice = true;
                    statusBoard = new Choice(statusBoard);
                    statusBoard.setCurrentChoicePlayerId(idPlayer);
                    sendBrelanOption(idPlayer);
                }
            }
        }
    }

    /**
     * @brief : Éssaie de calculer une fammile en fonction du nombre de couleur/métal qui compose le vol
     * @param flight: ArrayList<Card> du vol choisie
     * @param metal   : Enum Metal du métal choisie
     * @param color   : Enum Color de la couleur choisie
     * @return : int[] : le premier élément est le nombre d'exemplaire (selon la couleur/metal) le second
     * est la force du second plus fort de la famille
     */
    private int[] computeColorParam(ArrayList<Card> flight, Metal metal, Color color) {
        int nbSameColor = 0;
        int strengthSecond = 0;
        int strengthMax = 0;

        for (Card c : flight) {
            if (c.getMetal() == metal && c.getColor() == color || c.getColor() == Color.DIVIN) {
                nbSameColor++;
                if (c.getStrength() >= strengthMax) {
                    strengthSecond = strengthMax;
                    strengthMax = c.getStrength();
                } else if (c.getStrength() > strengthSecond) {
                    strengthSecond = c.getStrength();
                }
            }
        }

        return new int[]{nbSameColor, strengthSecond};
    }

    /**
     * @brief : Vérifie si un joueur a réaliser une famille après avoir joué une carte si oui applique les effets
     */
    private void tryColor() {
        Player player = players.get(statusBoard.getCurrentPlayerId());
        ArrayList<Card> flight = player.getFlight();
        int idPlayer = player.getId();

        if (!hasColor.get(idPlayer)) {
            Metal metal = cardPlayed.get(idPlayer).getMetal();
            Color color = cardPlayed.get(idPlayer).getColor();
            int[] colorParam = {0, 0};

            final int NB_CARD = 0;
            final int STRENGTH = 1;

            //cas particulier de tiamat qui possède toutes les couleurs
            if (color == Color.DIVIN) {
                int[] newColorParam;
                colorParam = computeColorParam(flight, metal, Color.values()[0]);
                for (int i = 1; i < Metal.values().length - 1; i++) {
                    newColorParam = computeColorParam(flight, metal, Color.values()[i]);
                    if (newColorParam[NB_CARD] >= 2 && newColorParam[STRENGTH] > colorParam[STRENGTH]) {
                        colorParam = newColorParam;
                    }
                }
            }
            //pas de calcul de couleur si la carte est dracolich ou bahamut
            else if ((metal != null || color != null) && metal != Metal.DIVIN) {
                colorParam = computeColorParam(flight, metal, color);
            }

            //on applique les effets d'une couleur si la couleur a au moins 3 cartes
            if (colorParam[NB_CARD] >= 3) {
                msg += players.get(idPlayer).getName() + " a fait une famille de force " + colorParam[STRENGTH] + ".";
                hasColor.set(idPlayer, true);
                for (Player p : players) {
                    p.subCoin(colorParam[STRENGTH]);
                    player.addCoin(colorParam[STRENGTH]);
                }
            }
        }
    }

    /**
     * @brief : Regarde si on peut terminer l'action d'avoir jouer une carte si oui alors applique la fin
     * du tour du currentPlayer
     */
    public void tryEndPlayCard() {
        if (statusBoard.getStateId() != StatusId.CHOICE) {
            int idPlayer = statusBoard.getCurrentPlayerId();
            if (players.get(idPlayer).getSizeFlight() >= 3 && !cardPlayed.get(idPlayer).isMortal()) {
                tryBrelan();
                tryColor();
            }

            statusBoard.tryEndRound();
            idPlayer = statusBoard.getCurrentPlayerId();

            if (totalBet != 0){
                for (Player p : players) {
                    if(p.getSizeHand() == 0) {
                        buyCard(p);
                    }
                }
                if(players.get(idPlayer).getSizeHand() == 1) {
                    buyCard(players.get(idPlayer));
                }
            }
        }
    }


    /**
     * @brief : Lecture de l'évènement du vote pour la fin de partie
     * @param chooseEndGame : ChooseEndGame de l'évènement
     * @return : boolean indiquant si le choix est possible
     */
    public boolean processEvent(ChooseEndGame chooseEndGame) {
        if (statusBoard.getStateId() != StatusId.END_GAME) {
            return false;
        }

        statusBoard.addVote(chooseEndGame.getPlayerId(), chooseEndGame.isRestart());
        server.updateBoardClient(chooseEndGame.getPlayerId(), new JsonCreator().updateBoardClient( msg,this, chooseEndGame.getPlayerId()));
        return true;
    }

    /**
     * @brief : Lecture de l'évènement lorsqu'un choix a été fait
     * @param choiceMade: ChoiceMade de l'évènement
     * @return : boolean indiquant si le choix peu être fait
     */
    public boolean processEvent(ChoiceMade choiceMade) {
        int idPlayerChoice = statusBoard.getCurrentChoicePlayerId();
        Player currentPlayer = players.get(statusBoard.getCurrentPlayerId());
        msg = "";

        if (statusBoard.getStateId() != StatusId.CHOICE || choiceMade.getPlayerId() != idPlayerChoice) {
            return false;
        }

        if (hasChoice) {
            moveCard(ante, players.get(idPlayerChoice).getHand(), choiceMade.getIndexChoose());
            if (currentPlayer.getSizeHand() < NB_MAX_CARD_HAND) {
                sendBrelanOption(idPlayerChoice);
            } else {
                hasChoice = false;
                statusBoard = new WaitingPlay(statusBoard);
            }
        } else {
            statusBoard.getCardActivated().applyChoice(this, players.get(statusBoard.getCurrentPlayerId()), choiceMade);
            if(statusBoard.isPrincess() && statusBoard.getStateId() == StatusId.WAITING_PLAY){
                statusBoard.setCardActivated(cardPlayed.get(currentPlayer.getId()));
                statusBoard = new Choice(statusBoard);
                ((Princess) cardPlayed.get(currentPlayer.getId()).getPower()).tryReActivatePower(this, currentPlayer);
            }
        }

        tryEndPlayCard();

        if ( statusBoard.getStateId() == StatusId.CHOICE){
            msg += players.get(statusBoard.getCurrentChoicePlayerId()).getName() + " doit faire un choix";
        }
        else if  ( statusBoard.getStateId() == StatusId.ANTE){
            msg = "choisissez votre carte d'anté";
        } else if ( statusBoard.getStateId() == StatusId.END_GAME){
            msg = "Fin de partie";
        }

        updatePlayers();
        return true;
    }

    /**
     * @brief           : Indique à tous les joueurs que la partie est terminée
     * @param idPlayer  : int du joueur ayant quitté la partie
     */
    public void endGame(int idPlayer) {
        players.get(idPlayer).setCoin(0);
        statusBoard = new EndGame(statusBoard);
        msg = players.get(idPlayer).getName() + " a abandonné.";
        updatePlayers();
        server.quitGame(new JsonCreator().quitGame(true, true));
    }

    /**
     * @brief           : Process event regardant si un joueur quitte la partie
     * @param sendToQuit: SendToQuit de l'évènement
     * @return          : boolean si le joueur quitte
     */
    public boolean processEvent(SendToQuit sendToQuit){
        endGame(sendToQuit.getPlayerId());
        return true;
    }
}
