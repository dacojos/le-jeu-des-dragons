package models.board;

import communication.client.ClientInterface;

import controllers.UserInterface;
import models.event.*;
import models.card.Card;
import game.Game;

import models.event.choice.ChoiceOption;
import models.jsonHandler.JsonCreator;

import models.player.PlayerClient;
import models.statusBoard.StatusId;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * @brief   : Classe premettant de représenter le board pour le client n'indiquant que les informations minimum
 * nécéssaire ainsi que la main du client actuelle.
 * @Date    : 21.10.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class BoardClient {
    private int nbdrCardDeck = 70;
    private int totalBet = 0;
    private int personalId  = -1;
    private int currentPlayerId = -1;

    public int flagState = 0;

    private ArrayList<Card> discard = new ArrayList<>(70);
    private ArrayList<Card> ante = new ArrayList<>(6);
    private ArrayList<Card> clientHand = new ArrayList<>(10);

    private ArrayList<PlayerClient> players = new ArrayList<>(6);

    private UserInterface ui = null;
    private ClientInterface client;

    public BoardClient(){}

    public void setTotalBet(int totalBet) {
        this.totalBet = totalBet;
    }

    public int getTotalBet() {
        return totalBet;
    }

    public void setNbdrCardDeck(int nbdrCardDeck) {
        this.nbdrCardDeck = nbdrCardDeck;
    }

    public int getNbdrCardDeck() {
        return nbdrCardDeck;
    }

    public void setDiscard(ArrayList<Card> discard) {
        this.discard = discard;
    }

    public ArrayList<Card> getDiscard() {
        return discard;
    }

    public void setAnte(ArrayList<Card> ante) {
        this.ante = ante;
    }

    public ArrayList<Card> getAnte() {
        return ante;
    }

    public void setClientHand(ArrayList<Card> clientHand) {
        this.clientHand = clientHand;
    }

    public ArrayList<Card> getClientHand() {
        return clientHand;
    }

    public void setPersonalId(int personalId) {
        this.personalId = personalId;
    }

    public int getPersonalId() {
        return personalId;
    }

    public void setPlayers(ArrayList<PlayerClient> players) {
        this.players = players;
    }

    public ArrayList<PlayerClient> getPlayers() {
        return players;
    }

    public int getCurrentPlayerId() {
        return currentPlayerId;
    }

    public void setCurrentPlayerId(int currentPlayerid) {
        this.currentPlayerId = currentPlayerid;
    }

    public void setFlagState(int flagState){
        this.flagState = flagState;
    }

    public int getFlagState() {
        return flagState;
    }

    public void setui(UserInterface ui) {
        this.ui = ui;
    }

    public UserInterface getui() {
        return ui;
    }

    public ClientInterface getClient() {
        return client;
    }

    public void setClient(ClientInterface client) { this.client = client; }

    /**
     * @brief       : Envoie une demande por rejoindre une partie au serveur
     * @param name  : String du nom du joueur
     */
    public boolean joinGame(String name){
        return client.joinGame(new JsonCreator().joinGame(name));
    }

    /**
     * @brief           : Envoie une demande pour commencer la partie au serveur
     */
    public boolean startGame(){
        return client.startGame(new JsonCreator().startGame(personalId));
    }

    /**
     * @brief           : Notifie au serveur quel carte veut joueur le joueur
     * @param posCad    : int de la position de la carte dans la main du joueur
     */
    public void playCard(int posCad){
        client.playerPlayCard(new JsonCreator().playerPlayCard(personalId, posCad));
    }

    /**
     * @brief       : Notifie le serveur du choix de fin de partie
     * @param choice: boolean du choix
     */
    public void chooseEndGame(boolean choice){
        client.chooseEndGame(new JsonCreator().chooseEndGame(personalId, choice));
    }

    /**
     * @biref           : Fonction permettant d'assigner l'id au joueur
     * @param assignId  : AssignId permettant de mettre à jour l'id du joueur
     */
    public void processEvent(AssignId assignId){
        this.personalId = assignId.getId();
    }

    /**
     * @brief       : Recherche une carte parmis la liste du jeu
     * @param id    : int de l'id de la carte recherché
     * @return      : Card correspondant à l'id
     */
    public Card searchCardById(int id){
        return Game.cardGameList.get(id);
    }

    /**
     * @brief                : Fonction mettant à jour la boardClient
     * @param updateBoard    : UpdateBoardClient permettant de mettre à jour le boardClient
     */
    public void processEvent(UpdateBoardClient updateBoard){
        if(updateBoard.getFlagState() != flagState && flagState != StatusId.WAITING_PLAYER.getId()) {
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        this.nbdrCardDeck = updateBoard.getNbrCardDeck();
        this.totalBet = updateBoard.getTotalBet();
        this.currentPlayerId = updateBoard.getCurrentPlayerId();
        this.flagState = updateBoard.getFlagState();

        if(updateBoard.getAnte() != null) {
            ante = new ArrayList<>(updateBoard.getAnte());
        }

        if(updateBoard.getDiscard() != null) {
            discard = new ArrayList<>(updateBoard.getDiscard());
        }

        if(updateBoard.getClientHand() != null) {
            clientHand = new ArrayList<>(updateBoard.getClientHand());
        }

        players = new ArrayList<>();

        for(PlayerClient p : updateBoard.getPlayers()){
            players.add(new PlayerClient(p));
        }

        if(ui != null)
            ui.updateBoard( this, updateBoard.getMsg());


        if(flagState == StatusId.END_GAME.getId()){
            ui.showEndgame(false);
        }

    }

    /**
     * @brief           : Notifie au joueur de quitter la partie
     * @param quitGame  : QuitGame permettant de dire la fin de partie
     */
    public void processEvent(QuitGame quitGame){
        if(quitGame.isQuit()){
            ui.showEndgame(quitGame.isHardQuit());
        } else {
            if (ui != null) {
                ui.updateBoard(this, "relance la partie");
            }
        }
    }

    /**
     * @brief               : Permet d'indiquer la liste des choix que doit réaliser le joueur
     * @param choiceOption  : ChoiceOption de l'évènement contenant la liste des choix
     */
    public void processEvent(ChoiceOption choiceOption){
        currentPlayerId = choiceOption.getTargetPlayerId();
        ui.showChoices(choiceOption.getChoiceOption());
    }

    /**
     * @brief           : Envoie au serveur l'index du choix qui a été fait
     * @param idChoice  : int de l'id du choix réalisé
     */
    public void sendChoice(int idChoice){
        client.choiceMade(new JsonCreator().choiceMade(personalId, idChoice));
    }

    /**
     * @brief         : Envoie au serveur le choix réalisé par le joueur
     * @param restart : boolean du choix du joueur
     */
    public void sendEndGame(boolean restart) {
        client.chooseEndGame(new JsonCreator().chooseEndGame(personalId, restart));
    }

    /**
     * @brief   : Envoie au serveur l'évènement que le joueur quite la partie
     */
    public void sendToQuit(){
        client.sendToQuit(new JsonCreator().sendToQuit(personalId));
    }

    /**
     * @brief           : sert à notifier le controlleur d'un message
     *                    recu du serveur et à afficher à l'écran
     */
    public void handleServerMessage(String msg) {
        this.ui.displayServerMessage(msg);
    }
}
