package models.event;

import models.card.Card;
import models.player.PlayerClient;
import models.statusBoard.StatusId;

import java.util.ArrayList;

/**
 * @brief   : Hérite de la classe Event et représente l'évènement de mise à jour des BoardClients
 * @Date    : 05.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class UpdateBoardClient extends Event{
    private String msg;
    private int nbrCardDeck;
    private int totalBet;
    private int currentPlayerId;
    private int flagState;

    private ArrayList<Card> ante = new ArrayList<>();
    private ArrayList<Card> discard = new ArrayList<>();
    private ArrayList<Card> clientHand = new ArrayList<>();

    private ArrayList<PlayerClient> players = new ArrayList<>();

    public UpdateBoardClient( String msg, int nbrCardDeck, int totalBet, int currentPlayerId, int flagState, ArrayList<Card> ante, ArrayList<Card> discard, ArrayList<Card> clientHand, ArrayList<PlayerClient> players){
        super();
        this.msg = msg;
        this.nbrCardDeck = nbrCardDeck;
        this.totalBet = totalBet;
        this.currentPlayerId = currentPlayerId;
        if(flagState != StatusId.ANTE.getId()){
            this.ante = ante;
        }
        this.discard = discard;
        this.clientHand = clientHand;
        this.players = players;
        this.flagState = flagState;
    }

    public String getMsg(){
        return msg;
    }

    public int getNbrCardDeck() {
        return nbrCardDeck;
    }

    public int getTotalBet() {
        return totalBet;
    }

    public ArrayList<Card> getAnte() {
        return ante;
    }

    public ArrayList<Card> getDiscard() {
        return discard;
    }

    public ArrayList<Card> getClientHand() {
        return clientHand;
    }

    public ArrayList<PlayerClient> getPlayers() {
        return players;
    }

    public void setNbrCardDeck(int nbrCardDeck) {
        this.nbrCardDeck = nbrCardDeck;
    }

    public void setTotalBet(int totalBet) {
        this.totalBet = totalBet;
    }

    public void setAnte(ArrayList<Card> ante) {
        this.ante = ante;
    }

    public void setDiscard(ArrayList<Card> discard) {
        this.discard = discard;
    }

    public void setClientHand(ArrayList<Card> clientHand) {
        this.clientHand = clientHand;
    }

    public void setPlayers(ArrayList<PlayerClient> players) {
        this.players = players;
    }

    public PlayerClient getPlayer(int pos){
        return players.get(pos);
    }

    public void setCurrentPlayerId(int currentPlayerId) {
        this.currentPlayerId = currentPlayerId;
    }

    public int getCurrentPlayerId() {
        return currentPlayerId;
    }

    public int getFlagState() {
        return flagState;
    }

    public void setFlagState(int flagState) {
        this.flagState = flagState;
    }
}
