package models.event;

/**
 * @brief   : Hérite de la classe Event et représente l'évènement assignant le personnalId à BoardClient
 * @Date    : 05.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class AssignId extends Event{

    private int id;

    public AssignId(int id){
        super();
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
