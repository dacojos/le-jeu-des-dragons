package models.event.choice;

import models.event.Event;

/**
 * @brief   : Évènement gérant l'option choisie par le choix du joueur
 * @Date    : 03.12.2019
 * @Author   : Bouyiatiotis Stéphane
 */
public class ChoiceMade extends Event {
    private int playerId;
    private int idChoose;

    public ChoiceMade(int playerId, int idChoose) {
        this.playerId = playerId;
        this.idChoose = idChoose;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public void setIndexChoose(int indexChoose) {
        this.idChoose = indexChoose;
    }

    public int getIndexChoose() {
        return idChoose;
    }
}
