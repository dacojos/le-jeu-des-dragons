package models.event.choice;

import models.event.Event;

import java.util.ArrayList;

/**
 * @brief   : Évènement gérant les options de choix pour le joueur ciblé
 * @Date    : 03.12.2019
 * @Author   : Bouyiatiotis Stéphane
 */
public class ChoiceOption extends Event {
    private int targetPlayerId;
    private ArrayList<String> choiceOption;

    public ChoiceOption(int targetPlayerId, ArrayList<String> choiceOption){
        this.targetPlayerId = targetPlayerId;
        this.choiceOption = choiceOption;

    }

    public int getTargetPlayerId() {
        return targetPlayerId;
    }

    public void setTargetPlayerId(int targetPLayerId) {
        this.targetPlayerId = targetPLayerId;
    }

    public ArrayList<String> getChoiceOption() {
        return choiceOption;
    }

    public void setChoiceOption(ArrayList<String> choiceOption) {
        this.choiceOption = choiceOption;
    }

    public void addChoiceOption(String option){
        choiceOption.add(option);
    }

    public String selectOption(int index){
        return choiceOption.get(index);
    }
}
