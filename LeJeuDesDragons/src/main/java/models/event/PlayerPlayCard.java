package models.event;

/**
 * @brief   : Hérite de la classe Event et représente l'évènement d'un joueur voulant jouer une carte
 * @Date    : 05.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class PlayerPlayCard extends Event{
    private int idPlayer;
    private int posCard;

    public PlayerPlayCard(int idPlayer, int posCard){
        super();
        this.idPlayer = idPlayer;
        this.posCard = posCard;
    }

    public int getIdPlayer(){
        return idPlayer;
    }

    public int getPosCard() {
        return posCard;
    }

    public void setIdPlayer(int idPlayer) {
        this.idPlayer = idPlayer;
    }

    public void setPosCard(int posCard) {
        this.posCard = posCard;
    }
}
