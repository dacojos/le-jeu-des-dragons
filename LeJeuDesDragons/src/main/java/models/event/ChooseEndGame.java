package models.event;

/**
 * @brief   : Évènement permettant de dire si on doit réaliser un restart ou bien un quit
 * @Date    : 26.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class ChooseEndGame extends Event {
    private boolean restart;
    private int playerId;

    public ChooseEndGame(int playerId, boolean restart){
        super();
        this.restart = restart;
        this.playerId = playerId;
    }

    public boolean isRestart() {
        return restart;
    }

    public void setRestart(boolean restart) {
        this.restart = restart;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }
}
