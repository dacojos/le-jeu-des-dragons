package models.event;

/**
 * @brief   : Hérite de la classe Event et représente l'évènement quand un joueur rejoind une partie
 * @Date    : 05.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class JoinGame extends Event{
    private String name;

    public JoinGame(String name){
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
