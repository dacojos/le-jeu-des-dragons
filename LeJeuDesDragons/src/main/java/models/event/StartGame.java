package models.event;

/**
 * @brief   : Hérite de la classe Event et représente l'évènement quand le joueur hote lance la partie
 * @Date    : 05.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class StartGame extends Event{
    private int idPlayer;

    public StartGame(int idPlayer){
        super();
        this.idPlayer = idPlayer;
    }

    public int getIdPlayer(){
        return idPlayer;
    }

    public void setIdPlayer(int idPlayer) {
        this.idPlayer = idPlayer;
    }
}
