package models.event;

public class SendToQuit {
    private int playerId;

    public SendToQuit(int playerId){
        this.playerId = playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public int getPlayerId() {
        return playerId;
    }
}
