package models.event;

/**
 * @brief   : Évènement indiquant aux joueurs que la partie est fini
 * @Date    : 26.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class QuitGame extends Event{
    private boolean quit;
    private boolean hardQuit;

    public QuitGame(boolean quit, boolean hardQuit){
        super();
        this.quit = quit;
        this.hardQuit = hardQuit;
    }

    public boolean isQuit() {
        return quit;
    }

    public void setQuit(boolean quit) {
        this.quit = quit;
    }

    public boolean isHardQuit() {
        return hardQuit;
    }
}
