package models.card.dragon;

/**
 * @brief   : Enum des métaux pour les dragons de métal
 * @Date     : 16.10.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public enum Metal {
    GOLD,
    SILVER,
    BRONZE,
    BRASS,
    COPPER,
    DIVIN
}
