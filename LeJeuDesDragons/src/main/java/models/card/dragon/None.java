package models.card.dragon;

import models.power.Power;

/**
 * @brief   : Classe enfant de Dragon permettant de créer les dragon sans couleur ou sans métal
 * @Date     : 16.10.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class None extends Dragon{
    private String type;
    private boolean alignement = false;

    public None(int id, String name, int strength, String type) {
        super(id, name, strength);
        this.type = type;
    }

    public None(int id, String name, int strength, String type, boolean alignement){
        this(id, name, strength, type);
        this.alignement = alignement;
    }

    public None(int id, String name, int strength, Power power, String type) {
        super(id, name, strength, power);
        this.type = type;
    }

    public None(int id, String name, int strength, Power power, String type, boolean alignement){
        this(id, name, strength, power, type);
        this.alignement = alignement;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setAlignement(boolean alignement) {
        this.alignement = alignement;
    }

    @Override
    public boolean isGood() {
        return alignement;
    }
}
