package models.card.dragon;

import models.power.Power;

/**
 * @brief   : Classe enfant héritant de Dragon permettant de représenter les dragons métalliques
 * @Date     : 16.10.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Metallic extends Dragon {

    private Metal metal;

    public Metallic(int id, String name, int strength, Metal metal) {
        super(id, name, strength);
        this.metal = metal;
    }
    public Metallic(int id, String name, int strength, Power power, Metal metal) {
        super(id, name, strength, power);
        this.metal = metal;
    }

    public void setMetal(Metal metal) {
        this.metal = metal;
    }

    public Metal getMetal() {
        return metal;
    }

    @Override
    public boolean isGood() {
        return true;
    }
}
