package models.card.dragon;

import models.power.Power;

/**
 * @brief   : Classe enfant héritant de Dragon permettant de représenter les dragons de couleur
 * @Date     : 16.10.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Colored extends Dragon {
    private Color color;

    public Colored(int id, String name, int strength, Color color) {
        super(id, name, strength);
        this.color = color;
    }

    public Colored(int id, String name, int strength, Power power, Color color) {
        super(id, name, strength, power);
        this.color = color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    @Override
    public boolean isBad() {
        return true;
    }
}
