package models.card.dragon;

import models.card.Card;
import models.power.Power;

/**
 * @brief   : Classe enfant de Card permettant de représenter les dragons du jeu
 * @Date     : 16.10.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Dragon extends Card {

    public Dragon(int id, String name, int strength) {
        super(id, name, strength);
    }
    public Dragon(int id, String name, int strength, Power power) {
        super(id, name, strength, power);
    }

    @Override
    public boolean isGood(){
        return false;
    }

    @Override
    public boolean isBad(){
        return false;
    }
}
