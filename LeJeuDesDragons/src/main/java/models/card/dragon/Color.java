package models.card.dragon;

/**
 * @brief   : Enum des couleurs pour les dragons de couleur
 * @Date     : 16.10.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public enum Color {
    RED,
    BLUE,
    BLACK,
    GREEN,
    WHITE,
    DIVIN
}
