package models.card.mortal;

import models.card.Card;
import models.power.Power;

/**
 * @brief   : Classe enfant héritant de card permettant de créer les mortels
 * @Date     : 16.10.2019
 * @Author   : Bouyiatiotis Stéphane
 */
public class Mortal extends Card {
    public static final int PRICE_TO_PAY = 1;


    public Mortal(int id, String name, int strength) {
        super(id, name, strength);
    }
    public Mortal(int id, String name, int strength, Power power) {
        super(id, name, strength, power);
    }

    @Override
    public boolean isMortal() {
        return true;
    }
}
