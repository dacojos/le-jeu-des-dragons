package models.card;


import models.board.Board;
import models.card.dragon.*;
import models.event.choice.ChoiceMade;
import models.player.Player;
import models.power.Power;

/**
 * @brief   : Classe permettant de représenter les cartes du jeu
 * @Date     : 16.10.2019
 * @Author   : Bouyiatiotis Stéphane
 *
 */
public class Card {
    private int idCard;
    private String name;
    private int strength;
    private Power power;

    public static final int MAX_STRENGTH = 13;

    public Card(String name, int strength){
        this.name = name;
        this.strength = strength;
    }

    public Card(int id, String name, int strength){
        this(name, strength);
        this.idCard = id;
    }

    public Card(int id, String name, int strength, Power power){
        this(id, name, strength);
        this.power = power;
    }

    public void setPower(Power power) {
        this.power = power;
    }

    public Power getPower() {
        return power;
    }

    public void setIdCard(int idCard) {
        this.idCard = idCard;
    }

    public int getIdCard() {
        return idCard;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getStrength() {
        return strength;
    }

    public boolean isGood(){
        return false;
    }

    public boolean isBad(){
        return false;
    }

    public boolean isMortal(){
        return false;
    }

    public Metal getMetal(){
        return null;
    }

    public Color getColor(){
        return null;
    }

    /**
     * @brief               : Active le pouvoir de la carte
     * @param board         : Board du plateau de jeu
     * @param currentPlayer : Player du joueur ayant joué la carte
     */
    public void activatePower(Board board, Player currentPlayer){
        if(power != null) {
            power.activatePower(board, currentPlayer);
        }
    }

    /**
     * @brief               : applique le choix réalisé par le joueur concerné
     * @param board         : Board du plateau de jeu
     * @param currentPlayer : Player du joueur ayant activé le pouvoir
     * @param choiceMade    : ChoiceMade contenant le choix que le joueur concerné a fait
     */
    public void applyChoice(Board board, Player currentPlayer, ChoiceMade choiceMade){
        power.applyChoice(board, currentPlayer, choiceMade);
    }

    @Override
    public String toString(){
        return name + " " + strength;
    }

}
