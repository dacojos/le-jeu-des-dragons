package models.jsonHandler;

import models.board.Board;
import models.player.Player;
import models.statusBoard.StatusId;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;

/**
 * @brief   : Permet de générer la string json en fonction de l'action réaliser.
 * @Date    : 11.11.2019
 * @author  : Bouyiatiotis Stéphane
 */
public class JsonCreator {
    private JSONObject json = new JSONObject();

    public JsonCreator(){

    }

    /**
     * @brief       : Crée la json string de l'évènement JoinGame
     * @param name  : String du nom de la personne rejoingnant la partie
     * @return      : String json de l'évènement
     */
    public String joinGame(String name){

        json.put("name", name);

        return json.toString();
    }

    /**
     * @brief           : Crée la json string de l'évènement StartGame
     * @param playerId  : int de l'id du joueur voulant lancer la partie
     * @return          : String json de l'évènement
     */
    public String startGame(int playerId){
        json.put("id", playerId);

        return json.toString();
    }

    /**
     * @brief           : Crée le json string de l'évènement AssignId
     * @param playerId  : int de l'id du nouveau joueur
     * @return          : String json de l'évènement
     */
    public String assignId(int playerId){
        json.put("personalId", playerId);

        return json.toString();
    }

    /**
     * @brief           : Crée le json string de l'évènement UpdateBoardClient
     * @param board     : Board contenant les informations à envoyer
     * @param playerId  : int de l'id du joueur auxquel il faut envoyer l'update
     * @return          : String json de l'évènement
     */
    public String updateBoardClient( String msg, Board board, int playerId){
        JSONArray ante = new JSONArray();
        JSONArray discard = new JSONArray();
        JSONArray clientHand = new JSONArray();

        json.put( "msg", msg);
        json.put("nbrCardDeck", board.getSizeDeck());
        json.put("totalBet", board.getTotalBet());
        if(board.getStatusBoard().getStateId() == StatusId.CHOICE){
            json.put("currentPlayerId", board.getStatusBoard().getCurrentChoicePlayerId());
        } else {
            json.put("currentPlayerId", board.getStatusBoard().getCurrentPlayerId());
        }
        json.put("flagState", board.getStatusBoard().getStateId().getId());

        //Crée la liste de l'ante
        if( board.getStatusBoard().getStateId() != StatusId.ANTE){
            for(int i = 0; i < board.getSizeAnte(); ++i){
                if(board.getAnte().get(i) != null)
                    ante.add(board.getAnte().get(i).getIdCard());
            }
        }
        json.put("ante", ante);


        //Crée la liste de la discard
        for(int i = 0; i < board.getSizeDiscard(); ++i){
            discard.add(board.getDiscard().get(i).getIdCard());
        }
        json.put("discard", discard);


        //Crée la liste des cards de la main du joueur
        Player playerHand = board.getPlayers().get(playerId);
        for(int i = 0; i < playerHand.getSizeHand(); ++i){
            clientHand.add(playerHand.getHand().get(i).getIdCard());
        }
        json.put("clientHand", clientHand);

        JSONArray players = new JSONArray();

        //Crée la liste des joueurs dans le jsonArray
        Player playerClient;
        JSONObject player;
        JSONArray flight;
        for(int i = 0; i < board.getNumberPlayer(); ++i) {
            playerClient = board.getPlayers().get(i);
            player = new JSONObject();

            //Information nécésaire au PlayerClient pour l'affichage
            player.put("coin", playerClient.getCoin());
            player.put("name", playerClient.getName());
            player.put("id", playerClient.getId());
            player.put("handSize", playerClient.getSizeHand());

            //Prend le vol de chaque joueur
            flight = new JSONArray();
            for(int j = 0; j < playerClient.getFlight().size(); ++j){
                flight.add(playerClient.getFlight().get(j).getIdCard());
            }
            player.put("flight", flight);
            players.add(player);
        }

        //Place tous les joueurs
        json.put("players", players);

        return json.toString();
    }

    /**
     * @brief           : Crée la string json de l'évènement PlayerPlayCard
     * @param playerId  : int de l'id du joueur ayant joué la carte
     * @param pos       : int de la position de la carte dans la main du joueur
     * @return          : String json de l'évènement
     */
    public String playerPlayCard(int playerId, int pos){
        json.put("idPlayer", playerId);
        json.put("posCard", pos);

        return json.toString();
    }

    /**
     * @brief           : Crée la string json de l'évènement ChooseEndGame
     * @param playerId  : int de l'id du player votant
     * @param restart   : boolean indiquant si le joueur veut restart
     * @return          : String json de l'évènement
     */
    public String chooseEndGame(int playerId, boolean restart){
        json.put("restart", restart);
        json.put("playerId", playerId);
        return json.toString();
    }

    /**
     * @brief       : Crée la string json de l'évènement QuitGame
     * @param quit  : boolean indiquant si la partie se quitte
     * @return      : String json de l'évènement
     */
    public String quitGame(boolean quit, boolean hardQuit){
        json.put("quit", quit);
        json.put("hardQuit", hardQuit);
        return json.toString();
    }

    /**
     * @brief               : Crée la string json de l'évènement ChoiceOption
     * @param playerId      : int de l'id du joueur concernée
     * @param choiceOption  : ArrayList des choix possibles
     * @return              : String json de l'évènement
     */
    public String choiceOption(int playerId, ArrayList<String> choiceOption){
        JSONArray listChoice = new JSONArray();

        json.put("playerId", playerId);
        for (int i = 0; i < choiceOption.size(); ++i){
            listChoice.add(choiceOption.get(i));
        }
        json.put("choiceOption", listChoice);
        return json.toString();
    }


    /**
     * @brief           : Crée la string json de l'évènement ChoiceMade
     * @param playerId  : int de l'id du joueur
     * @param idChoose  : int de l'index du choic
     * @return          : String json de l'évènement
     */
    public String choiceMade(int playerId, int idChoose){
        json.put("playerId", playerId);
        json.put("idChoose", idChoose);
        return json.toString();
    }

    /**
     * @brief           : Crée la string de l'évènement sendToQuit
     * @param playerId  : int de l'id du joueur quittant la partie
     * @return          : String json de l'évènement
     */
    public String sendToQuit(int playerId){
        json.put("playerId", playerId);
        return json.toString();
    }
}
