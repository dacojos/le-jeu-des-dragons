package models.jsonHandler;

import models.event.*;
import models.card.Card;
import game.Game;
import models.event.choice.ChoiceMade;
import models.event.choice.ChoiceOption;
import models.player.PlayerClient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;

/**
 * @brief   : Permet de lire la string json et de créer l'évènement
 * @Date    : 11.11.2019
 * @author  : Bouyiatiotis Stéphane
 */
public class JsonReader {
    private JSONObject json = new JSONObject();

    public JsonReader(){

    }

    /**
     * @brief   : Parse la string envoyé
     * @param s : String json devant être parsé
     */
    private void createParse(String s){
        JSONParser jsonParser = new JSONParser();

        try {
            json = (JSONObject) jsonParser.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * @brief           : Crée l'évènement JoinGame
     * @param stringJson: String contenant les information et devant être parser pour créer l'évènement
     * @return          : JoinGame de l'évènement
     */
    public JoinGame parseJoinGame(String stringJson){
        createParse(stringJson);
        return new JoinGame((String) json.get("name"));
    }

    /**
     * @brief           : Crée l'évènement StartGame
     * @param stringJson: String contenant les information et devant être parser pour créer l'évènement
     * @return          : StartGame de l'évènement
     */
    public StartGame parseStartGame(String stringJson){
        createParse(stringJson);
        return new StartGame(Integer.parseInt( json.get("id").toString()));
    }

    /**
     * @brief           : Crée l'évènement PlayerPlayCard
     * @param stringJson: String contenant les information et devant être parser pour créer l'évènement
     * @return          : PlayerPlayCard de l'évènement
     */
    public PlayerPlayCard parsePlayerPlayCard(String stringJson){
        createParse(stringJson);
        return new PlayerPlayCard(Integer.parseInt(  json.get("idPlayer").toString()), Integer.parseInt(  json.get("posCard").toString()));
    }

    /**
     * @brief           : Crée l'évènement AssignId
     * @param stringJson: String contenant les information et devant être parser pour créer l'évènement
     * @return          : AssignId de l'évènement
     */
    public AssignId parseAssignId(String stringJson){
        createParse(stringJson);
        return new AssignId(Integer.parseInt(  json.get("personalId").toString()));
    }

    /**
     * @brief           : Crée l'évènement UpdateBoardClient
     * @param stringJson: String contenant les information et devant être parser pour créer l'évènement
     * @return          : UpdateBoardClient de l'évènement
     */
    public UpdateBoardClient parseUpdateBoardClient(String stringJson){
        createParse(stringJson);

        ArrayList<Card> ante = new ArrayList<>();
        ArrayList<Card> discard = new ArrayList<>();
        ArrayList<Card> clientHand = new ArrayList<>();

        ArrayList<PlayerClient> players = new ArrayList<>();

        //Récupère l'ante
        JSONArray jsonArray = (JSONArray) json.get("ante");
        for(int i = 0; i < jsonArray.size(); ++i){
            Integer pos = Integer.parseInt(jsonArray.get(i).toString());
            ante.add(Game.cardGameList.get(pos));
        }

        //Récupère discard
        jsonArray = (JSONArray) json.get("discard");
        for(int i = 0; i < jsonArray.size(); ++i){
            Integer pos = Integer.parseInt(jsonArray.get(i).toString());
            discard.add(Game.cardGameList.get(pos));
        }

        //Récupère clientHand
        jsonArray = (JSONArray) json.get("clientHand");
        for(int i = 0; i < jsonArray.size(); ++i){
            Integer pos = Integer.parseInt(jsonArray.get(i).toString());
            clientHand.add(Game.cardGameList.get(pos));
        }

        //Récupère les joueurs ainsi que leurs informations
        jsonArray = (JSONArray) json.get("players");
        for(int i = 0; i < jsonArray.size(); ++i){
            JSONObject jsonPlayerClient = (JSONObject) jsonArray.get(i);
            ArrayList<Card> flight = new ArrayList<>();

            //Récupère le vol de chaque joueur
            JSONArray jsonArrayFlightClient = (JSONArray) jsonPlayerClient.get("flight");
            for (int j = 0; j < jsonArrayFlightClient.size(); ++j){
                Integer pos = Integer.parseInt(jsonArrayFlightClient.get(j).toString());
                flight.add(Game.cardGameList.get(pos));
            }

            players.add(new PlayerClient(Integer.parseInt(  jsonPlayerClient.get("coin").toString()),
                                        (String) jsonPlayerClient.get("name"),
                                        Integer.parseInt(  jsonPlayerClient.get("id").toString()),
                                        Integer.parseInt(  jsonPlayerClient.get("handSize").toString()),
                                        flight));
        }

        return new UpdateBoardClient(json.get("msg").toString(),
                                     Integer.parseInt(json.get("nbrCardDeck").toString()),
                                     Integer.parseInt(json.get("totalBet").toString()),
                                     Integer.parseInt(json.get("currentPlayerId").toString()),
                                     Integer.parseInt(json.get("flagState").toString()),
                                     ante,
                                     discard,
                                     clientHand,
                                     players);
    }

    /**
     * @brief           : Crée l'évènement ChooseEndGame
     * @param stringJson: String contenant les informations permettant de créer les évènements
     * @return          : ChooseEndGame de l'évènement
     */
    public ChooseEndGame parseChooseEndGame(String stringJson){
        createParse(stringJson);

        return new ChooseEndGame(Integer.parseInt(json.get("playerId").toString()),
                                 Boolean.parseBoolean(json.get("restart").toString()));
    }

    /**
     * @brief           : Crée l'évènement QuitGame
     * @param stringJson: String contenant les informations pour créer l'évènement
     * @return          : QuitGame de l'évènement
     */
    public QuitGame parseQuitGame(String stringJson){
        createParse(stringJson);

        return new QuitGame(Boolean.parseBoolean(json.get("quit").toString()),
                Boolean.parseBoolean(json.get("hardQuit").toString()));
    }

    /**
     * @brief           : Crée l'évènement ChoiceMade
     * @param stringJson: String contenant les informations de l'évènement
     * @return          : ChoiceOption de l'évènement
     */
    public ChoiceOption parseChoiceOption(String stringJson){
        createParse(stringJson);
        ArrayList<String> listChoice = new ArrayList<>();

        JSONArray jsonArray = (JSONArray) json.get("choiceOption");
        for (int i = 0; i < jsonArray.size(); ++i){
            listChoice.add(jsonArray.get(i).toString());
        }

        return new ChoiceOption(Integer.parseInt(json.get("playerId").toString()), listChoice);
    }

    /**
     * @brief           : Crée l'évènement ChoiceMade
     * @param stringJson: String contenant les informations de l'évènement
     * @return          : ChoiceMade de l'évènement
     */
    public ChoiceMade parseChoiceMade(String stringJson){
        createParse(stringJson);

        return new ChoiceMade(Integer.parseInt(json.get("playerId").toString()),
                              Integer.parseInt(json.get("idChoose").toString()));
    }

    /**
     * @brief           : Crée l'évènement sendToQuit
     * @param stringJson: String contenant les informations de l'évènement
     * @return          : SendToQuit de l'évènement
     */
    public SendToQuit parseSendToQuit(String stringJson){
        createParse(stringJson);

        return new SendToQuit(Integer.parseInt(json.get("playerId").toString()));
    }
}
