package game;

import communication.client.DDClient;
import communication.server.DDServer;
import models.board.Board;
import models.board.BoardClient;

import java.util.Scanner;

/**
 * Test class to launch game in command line
 * Useful to test basic network actions
 */
public class GameTest {

    public static void main(String[] args) {

        Board board = new Board();
        BoardClient boardClient = new BoardClient();

        if(args.length == 0) {
            System.out.println("you have to provide 'server' or 'client'");
        } else {

            String arg = args[0];

            if(arg.equals("server")) {

                DDServer ddServer = new DDServer(board);
                ddServer.start();
                DDClient ddClient = new DDClient("127.0.0.1", boardClient);

            } else if(arg.equals("client")) {

                DDClient ddClient = new DDClient("127.0.0.1", boardClient);
                while(true) {
                    Scanner scanner = new Scanner(System.in);
                    String myString = scanner.next();
                    ddClient.joinGame("{\"name\":\"caca\"}");
                }

            } else {
                System.out.println("unknown arg " + arg);
            }
        }
    }
}
