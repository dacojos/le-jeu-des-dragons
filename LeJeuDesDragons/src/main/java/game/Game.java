package game;

import communication.client.DDClient;
import communication.protocol.DDProtocol;
import communication.server.DDServer;

import controllers.Controller;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import models.board.Board;
import models.board.BoardClient;
import models.card.Card;
import models.card.dragon.*;
import models.card.mortal.Mortal;
import models.power.dragon.color.*;
import models.power.dragon.metal.*;
import models.power.dragon.none.Dracolich;
import models.power.mortal.*;
import models.statusBoard.StatusId;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @brief   : Classe permettant de lancer une partie ou d'en rejoindre une, possèdent toutes les cartes du jeu
 * @Date    : 05.11.2019
 * @Author  : Bouyiatiotis Stéphane
 */
public class Game extends Application {
    private Board board = null;
    private BoardClient boardClient = null;

    public static ArrayList<Card> cardGameList;

    static {
        Brass brass = new Brass();
        Silver silver = new Silver();
        White white = new White();
        Blue blue = new Blue();
        Bronze bronze = new Bronze();
        Copper copper = new Copper();
        Black black = new Black();
        Gold gold = new Gold();
        Red red = new Red();
        Green green = new Green();

        cardGameList = new ArrayList<>(70);
        cardGameList.add(new Mortal(0, "L'Archimage", 9, new Archmage()));
        cardGameList.add(new Metallic(1, "Bahamut", 13, new Bahamut(), Metal.DIVIN));
        cardGameList.add(new None(2, "Dracolich", 10, new Dracolich(), "Dragon Mort-Vivant"));
        cardGameList.add(new Metallic(3, "Dragon d'Arain", 1, brass, Metal.BRASS));
        cardGameList.add(new Metallic(4, "Dragon d'Arain", 2, brass, Metal.BRASS));
        cardGameList.add(new Metallic(5, "Dragon d'Arain", 4, brass, Metal.BRASS));
        cardGameList.add(new Metallic(6, "Dragon d'Arain", 5, brass, Metal.BRASS));
        cardGameList.add(new Metallic(7, "Dragon d'Arain", 7, brass, Metal.BRASS));
        cardGameList.add(new Metallic(8, "Dragon d'Arain", 9, brass, Metal.BRASS));
        cardGameList.add(new Metallic(9, "Dragon d'Argent", 2, silver, Metal.SILVER));
        cardGameList.add(new Metallic(10, "Dragon d'Argent", 3, silver, Metal.SILVER));
        cardGameList.add(new Metallic(11, "Dragon d'Argent", 6, silver, Metal.SILVER));
        cardGameList.add(new Metallic(12, "Dragon d'Argent", 8, silver, Metal.SILVER));
        cardGameList.add(new Metallic(13, "Dragon d'Argent", 10, silver, Metal.SILVER));
        cardGameList.add(new Metallic(14, "Dragon d'Argent", 12, silver, Metal.SILVER));
        cardGameList.add(new Colored(15, "Dragon Blanc", 1, white, Color.WHITE));
        cardGameList.add(new Colored(16, "Dragon Blanc", 2, white, Color.WHITE));
        cardGameList.add(new Colored(17, "Dragon Blanc", 3, white, Color.WHITE));
        cardGameList.add(new Colored(18, "Dragon Blanc", 4, white, Color.WHITE));
        cardGameList.add(new Colored(19, "Dragon Blanc", 6, white, Color.WHITE));
        cardGameList.add(new Colored(20, "Dragon Blanc", 8, white, Color.WHITE));
        cardGameList.add(new Colored(21, "Dragon Bleu", 1, blue, Color.BLUE));
        cardGameList.add(new Colored(22, "Dragon Bleu", 2, blue, Color.BLUE));
        cardGameList.add(new Colored(23, "Dragon Bleu", 4, blue, Color.BLUE));
        cardGameList.add(new Colored(24, "Dragon Bleu", 7, blue, Color.BLUE));
        cardGameList.add(new Colored(25, "Dragon Bleu", 9, blue, Color.BLUE));
        cardGameList.add(new Colored(26, "Dragon Bleu", 11, blue, Color.BLUE));
        cardGameList.add(new Metallic(27, "Dragon De Bronze", 1, bronze, Metal.BRONZE));
        cardGameList.add(new Metallic(28, "Dragon De Bronze", 3, bronze, Metal.BRONZE));
        cardGameList.add(new Metallic(29, "Dragon De Bronze", 6, bronze, Metal.BRONZE));
        cardGameList.add(new Metallic(30, "Dragon De Bronze", 7, bronze, Metal.BRONZE));
        cardGameList.add(new Metallic(31, "Dragon De Bronze", 9, bronze, Metal.BRONZE));
        cardGameList.add(new Metallic(32, "Dragon De Bronze", 11, bronze, Metal.BRONZE));
        cardGameList.add(new Metallic(33, "Dragon De Cuivre", 1, copper, Metal.COPPER));
        cardGameList.add(new Metallic(34, "Dragon De Cuivre", 3, copper, Metal.COPPER));
        cardGameList.add(new Metallic(35, "Dragon De Cuivre", 5, copper, Metal.COPPER));
        cardGameList.add(new Metallic(36, "Dragon De Cuivre", 7, copper, Metal.COPPER));
        cardGameList.add(new Metallic(37, "Dragon De Cuivre", 8, copper, Metal.COPPER));
        cardGameList.add(new Metallic(38, "Dragon De Cuivre", 10, copper, Metal.COPPER));
        cardGameList.add(new Colored(39, "Dragon Noir", 1, black, Color.BLACK));
        cardGameList.add(new Colored(40, "Dragon Noir", 2, black, Color.BLACK));
        cardGameList.add(new Colored(41, "Dragon Noir", 3, black, Color.BLACK));
        cardGameList.add(new Colored(42, "Dragon Noir", 5, black, Color.BLACK));
        cardGameList.add(new Colored(43, "Dragon Noir", 7, black, Color.BLACK));
        cardGameList.add(new Colored(44, "Dragon Noir", 9, black, Color.BLACK));
        cardGameList.add(new Metallic(45, "Dragon D'Or", 2, gold, Metal.GOLD));
        cardGameList.add(new Metallic(46, "Dragon D'Or", 4, gold, Metal.GOLD));
        cardGameList.add(new Metallic(47, "Dragon D'Or", 6, gold, Metal.GOLD));
        cardGameList.add(new Metallic(48, "Dragon D'Or", 9, gold, Metal.GOLD));
        cardGameList.add(new Metallic(49, "Dragon D'Or", 11, gold, Metal.GOLD));
        cardGameList.add(new Metallic(50, "Dragon D'Or", 13, gold, Metal.GOLD));
        cardGameList.add(new Colored(51, "Dragon Rouge", 2, red, Color.RED));
        cardGameList.add(new Colored(52, "Dragon Rouge", 3, red, Color.RED));
        cardGameList.add(new Colored(53, "Dragon Rouge", 5, red, Color.RED));
        cardGameList.add(new Colored(54, "Dragon Rouge", 8, red, Color.RED));
        cardGameList.add(new Colored(55, "Dragon Rouge", 10, red, Color.RED));
        cardGameList.add(new Colored(56, "Dragon Rouge", 12, red, Color.RED));
        cardGameList.add(new Colored(57, "Dragon Vert", 1, green, Color.GREEN));
        cardGameList.add(new Colored(58, "Dragon Vert", 2, green, Color.GREEN));
        cardGameList.add(new Colored(59, "Dragon Vert", 4, green, Color.GREEN));
        cardGameList.add(new Colored(60, "Dragon Vert", 6, green, Color.GREEN));
        cardGameList.add(new Colored(61, "Dragon Vert", 8, green, Color.GREEN));
        cardGameList.add(new Colored(62, "Dragon Vert", 10, green, Color.GREEN));
        cardGameList.add(new Mortal(63, "La Druidesse", 6, new Druid()));
        cardGameList.add(new Mortal(64, "L'Idiot",3, new Fool()));
        cardGameList.add(new Mortal(65, "Le Prêtre", 5, new Priest()));
        cardGameList.add(new Mortal(66, "La Princesse", 4, new Princess()));
        cardGameList.add(new Colored(67, "Tiamat", 13, new Tiamat(), Color.DIVIN));
        cardGameList.add(new Mortal(68, "Le Tueur De Dragon", 8, new Dragonslayer()));
        cardGameList.add(new Mortal(69, "La Voleuse", 7, new Thief()));
    }

    /**
     * @brief           : Permet de rejoindre une partie
     * @param ipAdress  : String de l'ip de l'adresse à rejoindre
     * @param name      : String du nom du joueur rejoignant la partie
     * @param ctr       : Controlleur gérant l'interface
     */
    public boolean join(String ipAdress, String name, Controller ctr){

        boardClient = new BoardClient();

        boardClient.setui(ctr);
        boardClient.setClient(new DDClient(ipAdress, boardClient));
        boardClient.getClient().start();
        return boardClient.joinGame(name);

    }

    /**
     * @brief       : Permet de créer une partie et de la rejoindre
     * @param name  : String du nom du joueur créant la partie
     * @param ctr   : Controlleur gérant l'interface
     */
    public void host(String name, Controller ctr){

        board = new Board();
        board.setServer(new DDServer(board));

        boardClient = new BoardClient();

        boardClient.setui(ctr);
        board.getServer().start();

        boardClient.setClient(new DDClient("127.0.0.1", boardClient));
        boardClient.getClient().start();
        boardClient.joinGame(name);

        board.getServer().diffusePrivateIp();
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoardClient(BoardClient boardClient) {}

    public BoardClient getBoardClient() {
        return boardClient;
    }

    public void launchMenu(Stage primaryStage) {
        FXMLLoader fxml = new FXMLLoader(getClass().getResource("/views/menu.fxml"));
        Parent root = null;
        try {
            root = fxml.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        primaryStage.setTitle("Jeu des Dragons");

        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
        Controller ctrl = fxml.getController();
        ctrl.setApp(this);
        ctrl.setPrimaryStage(primaryStage);


        byte[] buffer = new byte[256];
        new Thread(() -> {

            // TODO: listen packets only if game not started, i.e. client board status
            String ip = DDClient.receiveUDPMessage(DDProtocol.MULTICAST_ADDRESS, DDProtocol.MULTICAST_PORT, buffer);
            ctrl.setServerAddr(ip);

        }).start();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        launchMenu(primaryStage);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
