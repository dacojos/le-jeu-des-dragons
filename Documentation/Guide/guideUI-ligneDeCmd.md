# lancement du LeJeuDesDragons-1.0-SNAPSHOT.jar:
le jar prend un paramètre en argument soit \[server,client\]

###exemple de lancement : 

java -jar LeJeuDesDragons-1.0-SNAPSHOT.jar server

java -jar LeJeuDesDragons-1.0-SNAPSHOT.jar client

## liste des commandes pour le server:
commande | explication
--- | ---
assignId \<playerId> | assigne un id au dernier client connecté
update \<playerId> |met à jour le board du client cible
help | affiche les commandes possibles

## liste des commandes pour le client:
commande | explication
--- | ---
join \<adress> \<name> | connecte le client à un server
start | permet au premier client de démarrer la partie
play \<posCard> | joue une carte
ls \<\[border,hand,ante,discard \<playerId>\]> | affiche une partie du board
help | affiche les commandes possibles