#Glossaire

**Anté :** Une carte révélée par un joueur au début de la partie. Chaque joueur choisit une anté à chaque mise.

**Couleur :** Le nom des dragons inclut leur couleur.

**Déclencher Un pouvoir :** suivre les effets d'un pouvoir.

**Défausser :** Mettre une carte d'une main ou d'un vol à la défausse. La main ou le vol est spécifié.

**Dette :** Un joueur a une dette envers la mise centrale ou un joueur lorsque il ne peut pas payé.

**Dragon:** Une carte dragon.

**Force:** Nombre écrit dans la partie supérieure de chaque carte. 

**Force Totale:** La somme des Forces des cartes qui composent le vol.

**Jouer une carte :** Poser une carte face visible de sa main dans son vol à son tour de jeu.

**Leader :** Le joueur qui commence le tour.

**Mise :** Phase du jeu débutant par le choix des anté et se terminant lorsque la mise centrale est gagné. Elle est composé d'au moins 3 tours et se produit normalement plusieurs fois durant la partie.

**Mise centrale:** L'or à gagner durant une mise.

**Main :** Les cartes tenues en main par un joueur.

**Magot :** L'or d'un joueur.

**Mortel :** Une carte qui n'est pas un dragon.

**Nature :** Une carte n'a qu'une nature soit bon, mauvais ou mortel. Elle est spécifié sur la carte.

**Payer :** Mettre de l'or de son magot à la mise centrale ou à un autre magot.

**Pouvoir :** La capacité d'une carte. Le joueur ne peut pas décider si le pouvoir se déclenche ou non.

**Tour :** Un tour est effectué si tous les joueurs ont jouer une carte.

**Vol :** La combinaison de carte face visible joué par un joueur durant la mise. Chaque joueur a son propre vol.

**Couleur de dragons :** Une combinaison de 3 dragons de la même couleur dans un vol.

**Brelan de dragons :** Une combinaison de 3 dragons de la même force dans un vol.
