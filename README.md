# Le Jeu Des Dragons

Lien vers la page de lancement : https://dacojos.gitlab.io/le-jeu-des-dragons-landing/

## Cahier des charges

### Technologies utilisées

Nous avons décidé de réaliser l'application en Java et d'utiliser JavaFX pour l'interface. La version utilisée est la 1.8.0.
L'application sera dévellopée sur Intellij et prévue pour être une application desktop.
La partie réseau sera aussi réalisée en java.

### Présentation du projet

#### Contexte

Dans le cadre du cours PDG nous allons réaliser une application permettant de jouer au jeu de carte "le jeu des dragons" ou encore "Three dragons ante" en anglais.

#### Description du jeu

Le jeu des dragons ou encore Three dragons ante est un jeu de carte tiré de l'univers de Donjon & Dragon. Le jeu de carte se veut aussi canon dans l'univers du jeu.
Le jeux utilise principalement les dragons de couleur ou les dragons de métal qui sont les plus connus dans le jeu.
Nous avons les 5 dragons de couleur : Noir, Rouge, Blanc, Vert et Bleu ainsi que les 5 dragons de métal : Or, Argent, Cuivre, Bronze et Étain.
Il y a aussi 7 non-dragons appellés mortels, comme la princesse, l'archimage, le voleur, la druidesse, le fou, le prêtre et le tueur de dragon.
Pour finir, les 3 dragons uniques comme Tiamat le dieu des dragons de couleur, Bahamut le dieu des dragons de métal ainsi que le Dracoliche le dragon mort-vivant.

#### Fonctionnement

Chaque joueur possède une somme de départ qu'il utilisera au début de chaque mise quand les dragons ante auront été choisis, la quantité de la mise dépendra de
 la force la plus grande parmi les dragons ante. La force des cartes varie de 1 à 13. Chaque joueur joue une carte chacun son tour jusqu'à en avoir 3 sur le terrain.
 Puis à la fin du round, celui qui possède le vol le plus puissant remporte la totalité de la mise.
 Chaque carte possède son propre pouvoir, ainsi un dragon noir aura toujours le même pouvoir peu importe sa force.

 Pour activer le pouvoir d'une carte il faut que la carte jouée soit de force égale ou inférieure à la carte précédente, sauf pour le premier joueur du tour.

Le but du jeu est de finir avec le plus de pièces d'or. La partie se termine quand un joueur atteint 0 pièce d'or ou moins après la fin d'une mise.

#### Objectif

Nous avons pour objectif de créer une application serveur-clients permettant de jouer au jeu des dragons.
Dans un premier temps, nous implémenterons les règles du jeu sans inclure toute la variété des cartes exitantes puis dans un second temps on ajoutera les cartes.

#### Périmètre
#### Description de l'existant

Nous avons pour base la version physique du jeu de cartes "Le jeu Des Dragons". Nous n'utiliserons pas les différentes extensions rajoutées au jeu.

### Expression des besoins

#### Besoins fonctionnels

l'application serveur doit permettre de:
 * gérer les cartes du jeu réparties dans diverses collections (pioche, anté, mains, défausse etc...).
 * gérer les clients.
 * transmettre aux bons clients quelles actions ont été faites avec quelles cartes elles ont été exécutées.
 * appliquer les règles du jeu

l'application client doit permettre de:
 * recevoir l'état actuel de la partie de la part du serveur
 * demander au serveur d'exécuter différentes tâches

#### Documentation
Au terme du projet la documentation sera composée des règles du jeu, d'un manuel utilisateur et d'une documentation technique.

## Répatition des tâches

Responsable UX / UI 	: stephane.bouyiatiotis@heig-vd.ch, id git : @scout407
Responsable DevOps		: joshua.gomesdacosta@heigvd.ch   , id git : @dacojos
Responsable Front-End	: max.caduff@heig-vd.ch			  , id git : @maxcaduff
Responsable Back-End	: simon.jobin@heig-vd.ch		  , id git : @sjaubain
